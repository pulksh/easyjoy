const express = require("express");
const router = require("express").Router();
const activityLogController = require("../controller/ActivityLogController");
const auth = require("../middleware/auth");
const reviewDataValidator = require("../middleware/dataValidator/reviewDataValidator");

router
  .route("/")
  .post(activityLogController.add)
//   .put(activityLogController.edit);

router
  .route("/list/:start/:length")
  .post(auth, reviewDataValidator.listCheck, activityLogController.listAll);

router
  .route("/csvData").post(activityLogController.getCsvData);

module.exports = router;
