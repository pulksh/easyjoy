const express = require("express");
const router = express.Router();
const path = require("path")
const notifController = require("../controller/NotificationController");
const auth = require("../middleware/auth");

router
  .route("/")
  .post(auth, notifController.add)

 router
   .route("/:id")
   .get(auth, notifController.getDetail)
   .delete( auth, notifController.delete)

router
  .route("/list/:start/:length")
  .post(auth, notifController.list);
  
router
.route("/sendmail")
.post(auth, notifController.sendMail);

module.exports = router;
