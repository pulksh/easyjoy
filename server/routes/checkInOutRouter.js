const express = require("express");
const router = require("express").Router();
const path = require("path");
const checkInOutController = require("../controller/checkInOutController");
const auth = require("../middleware/auth");
const reviewDataValidator = require("../middleware/dataValidator/reviewDataValidator");
const checkPermission = require("../middleware/accessValidator/reviewAccessValidator");
const multer = require("multer");

router.route("/").post(checkInOutController.add).put(checkInOutController.edit);
router
  .route("/:id")
  .get(checkInOutController.getDetail)
  .delete(checkInOutController.delete);
router
  .route("/list/:start/:length")
  .post(auth, reviewDataValidator.listCheck, checkInOutController.listAll);
  router
  .route("/csvData")
  .post(checkInOutController.getCsvData);

  // API for send mail to owner
  router.route("/sendmail").post(checkInOutController.sendMail);

module.exports = router;
