const router = require("express").Router();
const pageController = require("../controller/PageController");
const auth = require("../middleware/auth");

router.route("/")
.post(auth, pageController.add)
.put(auth, pageController.edit);

router.route("/:id").get(pageController.getDetail);

router.route('/pg/:id').get(pageController.getDetailByName);


router
  .route("/list/:start/:length")
  .post(auth, pageController.listAll); 
  
module.exports = router;
