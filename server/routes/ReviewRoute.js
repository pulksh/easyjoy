const express = require("express");
const router = require("express").Router();
const path = require("path");
const reviewController = require("../controller/ReviewController");
const auth = require("../middleware/auth");
const reviewDataValidator = require("../middleware/dataValidator/reviewDataValidator");
const checkPermission = require("../middleware/accessValidator/reviewAccessValidator");
//const routeHelper = require("./routeHelper")
const multer = require("multer");

router
  .route("/")
  // .post( reviewDataValidator.addCheck,checkPermission.addAcsCheck,reviewController.add)
  .post(reviewController.add)
  .put(reviewController.edit);
router
  .route("/:id")
  // .get(reviewDataValidator.getDetailCheck,checkPermission.getDetailAcsCheck,reviewController.getDetail)
  .get(reviewController.getDetail)
  .delete(reviewController.delete);
router
  .route("/list/:start/:length")
  .post(reviewDataValidator.listCheck, reviewController.listAll);

//routeHelper.generateStandardRoutes(router, dataValidator, auth, checkPermission, reviewController)
router.route("/csvData").post(reviewController.getCsvData);

module.exports = router;
