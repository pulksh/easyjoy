const router = require("express").Router();
const settingController = require("../controller/SettingController");
const auth = require("../middleware/auth");
const dataValidator = require("../middleware/dataValidator/settingDataValidator");
const checkPermission = require("../middleware/accessValidator/settingAccessValidator");
const multer = require("multer");
const upload = multer({
  dest: process.env.ROOT_PATH + process.env.TEMP_PATH
});

router
  .route("/")
  .post(upload.single("value"), dataValidator.addCheck, auth, checkPermission.addAcsCheck,settingController.add)
  .put(upload.single("value"), dataValidator.editCheck, auth, checkPermission.editAcsCheck,settingController.edit);

router
  .route("/list/:start/:length")
  .post(dataValidator.listCheck,  settingController.listAll);

router
  .route("/:id")
  .delete(dataValidator.deleteCheck, auth, checkPermission.deleteAcsCheck, settingController.delete)
  .get(dataValidator.getDetailCheck, auth, checkPermission.getDetailAcsCheck, settingController.getDetail);

module.exports = router;