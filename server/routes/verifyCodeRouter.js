const express = require("express");
const router = express.Router();
const path = require("path")
const verifyCodeController = require("../controller/verifyCodeController");
const auth = require("../middleware/auth");

const multer = require("multer");
// const upload = multer({
//   dest:path.join(__dirname,"../uploads/temp")
// });

router
  .route("/")
  .post(auth, verifyCodeController.verifyCode)
module.exports = router;
