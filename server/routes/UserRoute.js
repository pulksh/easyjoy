const express = require("express");
const router = express.Router();
const path = require("path")
const userController = require("../controller/UserController");
const auth = require("../middleware/auth");
const loginDataVd = require("../middleware/dataValidator/loginDataValidator");
const checkPermission = require("../middleware/accessValidator/userAccessValidator");
const userDataValidator= require("../middleware/dataValidator/userDataValidator");
const multer = require("multer");

const upload = multer({
  dest: path.join(__dirname, "../uploads/temp"),
});

const fields = [{ name: "agreement" }];

router
  .route("/")
  .post(upload.any(fields), userController.add)
  .put(upload.any(fields), auth, userController.edit);

router.route("/login").post(userController.login);
router.route("/forgotPassword").post(loginDataVd.forgetPassword,userController.forgotPassword);
router.route("/reset/:token").post(userController.resetPassword);
// router
//   .route("/")
//   .post(userDataValidator.add, userController.add)
//   .put( userDataValidator.edit,auth, checkPermission.editAuth,userController.edit);

router
  .route("/activate")
  .post(auth, userController.activate)

router
   .route("/:id")
   .get(userController.get)
   .delete(auth, checkPermission.deleteAuth, userController.delete);

router
  .route("/list/:start/:length")
  .post(auth,checkPermission.listAuth, userController.list);

  router
  .route("/ownerlist/:start/:length")
  .post(auth,checkPermission.ownerListAuth,userController.list);

  // Send Email
  router
  .route("/sendmail/:id")
  .post(auth, userController.sendMail)


module.exports = router;
