const router = require("express").Router();
const locationController = require("../controller/LocationController");
const auth = require("../middleware/auth");
const path = require("path");
const locationDataValidator = require("../middleware/dataValidator/locationDataValidator");
const checkPermission = require("../middleware/accessValidator/locationAccessValidator");
const multer = require("multer");
// const upload = multer({
//   dest: process.env.ROOT_PATH + process.env.TEMP_PATH
// });
const upload = multer({
  dest: path.join(__dirname, "../uploads/temp"),
});

const fields = [{ name: "featured_image" }, { name: "additional_image" }];

router
  .route("/")
  .post(
    upload.any(fields),
     auth,
    // productDataValidator.add,
    // checkPermission.addAuth,
    locationController.add
  )
  .put(
    upload.any(fields),
     auth,
    //  locationDataValidator.edit,
    // checkPermission.editAuth,
    locationController.edit
  );

router
  .route("/list/:start/:length")
  .post(auth,locationController.list);

  router
  .route("/listlocation/:start/:length")
  .post(locationController.list);


  router
  .route("/frontlist/:start/:length")
  .post(auth, locationController.list);

router
  .route("/:id")

  .delete( auth, checkPermission.deleteAuth,locationController.delete)
  .get(locationController.get);

  router
  .route("/addimag/:id")
  .get(auth, locationController.getLocationAdditionalImage)
  .delete(
    auth,
    // checkPermission.deleteAuth,
    // settingController.deleteAdditionalImage
    locationController.deleteAdditionalImage
  );

module.exports = router;
