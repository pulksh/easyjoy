const locationServ = require("../services/LocationService");
const utils = require("../utils/utils");
const multer = require("multer");
const upload = multer({ dest: "uploads/temp" });
const fs = require("fs");

module.exports = {
  add: async function (req, res, next) {
    let image = [];
    if (req.files) {
      for (let i = 0; i < req.files.length; i++) {
        const tmp_path = req.files[i].path;
        if (req.files[i].fieldname === "featured_image") {
          const file_final_name =
            new Date().getTime() + "_" + req.files[i].originalname;
          const final_path =
            process.env.ROOT_PATH +
            process.env.LOCATIONIMAGE_DESTINATION +
            file_final_name;
          final_url =
            process.env.API_URL +
            process.env.LOCATIONIMAGE_DESTINATION +
            file_final_name;
          fs.rename(tmp_path, final_path, (err) => {
            if (err) {
              return "File linking failed";
            }
          });
          req.body.featured_image = final_url;
        } else {
          const file_final_name =
            new Date().getTime() + "_" + req.files[i].originalname;
          const final_path =
            process.env.ROOT_PATH +
            // process.env.LOCATIONIMAGE_DESTINATION +
            process.env.LOCATION_ADDITIONALIMAGE +
            file_final_name;
          final_url =
            process.env.API_URL +
            // process.env.LOCATIONIMAGE_DESTINATION +
            process.env.LOCATION_ADDITIONALIMAGE +
            file_final_name;
          fs.rename(tmp_path, final_path, (err) => {
            if (err) {
              return "File linking failed";
            }
          });
          image.push(final_url);
        }
        req.body.additional_image = image;
      }
    }
    let result = await locationServ.save(req.body, next);
    utils.sendResponse(result, req, res);
  },

  edit: async function (req, res, next) {
    let image = [];
    if (req.files) {
      for (let i = 0; i < req.files.length; i++) {
        const tmp_path = req.files[i].path;
        if (req.files[i].fieldname === "featured_image") {
          const file_final_name =
            new Date().getTime() + "_" + req.files[i].originalname;
          const final_path =
            process.env.ROOT_PATH +
            process.env.LOCATIONIMAGE_DESTINATION +
            file_final_name;
          final_url =
            process.env.API_URL +
            process.env.LOCATIONIMAGE_DESTINATION +
            file_final_name;
          fs.rename(tmp_path, final_path, (err) => {
            if (err) {
              return "File linking failed";
            }
          });
          req.body.featured_image = final_url;
        } else {
          const file_final_name =
            new Date().getTime() + "_" + req.files[i].originalname;
          const final_path =
            process.env.ROOT_PATH +
            // process.env.LOCATIONIMAGE_DESTINATION +
            process.env.LOCATION_ADDITIONALIMAGE +
            file_final_name;
          final_url =
            process.env.API_URL +
            // process.env.LOCATIONIMAGE_DESTINATION +
            process.env.LOCATION_ADDITIONALIMAGE +
            file_final_name;
          fs.rename(tmp_path, final_path, (err) => {
            if (err) {
              return "File linking failed";
            }
          });
          image.push(final_url);
        }
        req.body.additional_image = image;
      }
    }
    let result = await locationServ.save(req.body, next);
    utils.sendResponse(result, req, res);
  },

  delete: async function (req, res, next) {
    let result = await locationServ.delete(req.params.id, next);
    utils.sendResponse(result, req, res);
  },
  getLocationAdditionalImage: async function (req, res, next) {
    let result = await locationServ.getLocationAdditionalImage(
      req.params.id,
      next
    );
    utils.sendResponse(result, req, res);
  },
  deleteAdditionalImage: async function (req, res, next) {
    let result = await locationServ.deleteAdditionalImage(req.params.id, next);
    utils.sendResponse(result, req, res);
  },

  list: async function (req, res, next) {
    let { start, length } = req.params;
    let result = await locationServ.list(
      parseInt(start),
      parseInt(length),
      req.body,
      req.currUser,
      next
    );
    utils.sendResponse(result, req, res);
  },

  filter: async function (req, res, next) {
    let { start, length } = req.params;
    let result = await locationServ.filter(
      parseInt(start),
      parseInt(length),
      req.body,
      req.params.id,
      next
    );
    utils.sendResponse(result, req, res);
  },

  get: async function (req, res, next) {
    let result = await locationServ.get(req.params.id, next);
    utils.sendResponse(result, req, res);
  },
};
