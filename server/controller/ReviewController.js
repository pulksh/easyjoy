const reviewServ = require("../services/ReviewService");
const utils = require("../utils/utils");

module.exports = {
  add: async function(req, res, next) {
    let result = await reviewServ.save(req.body, next);
    utils.sendResponse(result, req, res);
  },

  edit: async function(req, res, next) {
    let result = await reviewServ.save(req.body, next);
    utils.sendResponse(result, req, res);
  },

  delete: async function(req, res, next) {
    let result = await reviewServ.delete(req.params.id, next);
    utils.sendResponse(result, req, res);
  },

  listAll: async function(req, res, next) {
    let { start, length } = req.params;
    let result = await reviewServ.listAll(
      parseInt(start),
      parseInt(length),
      req.body,
      next
    );
    utils.sendResponse(result, req, res);
  },

  getDetail: async function(req, res, next) {
    let result = await reviewServ.getDetail(req.params.id, next);
    utils.sendResponse(result, req, res);
  },
  getCsvData:async function(req, res, next) {
    let result = await reviewServ.getCsvData(req, next);
    utils.sendResponse(result, req, res);
  }
};
