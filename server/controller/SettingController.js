const settingServ = require("../services/SettingService");
const utils = require("../utils/utils");
const fs = require("fs");
module.exports = {
  add: async function(req, res) {
    if (req.file) {
      const tmp_path = req.file.path;
      const file_final_name =
        new Date().getTime() + "_" + req.file.originalname;
      const final_path =
        process.env.ROOT_PATH + process.env.LOGOIMAGE_DESTINATION + file_final_name;
      final_url =
        process.env.API_URL + process.env.LOGOIMAGE_DESTINATION + file_final_name;
      fs.rename(tmp_path, final_path, err => {
        if (err) {
          return "File linking failed";
        }
      });
      req.body.value = final_url;
    }
    let result = await settingServ.save(req.body);
    utils.sendResponse(result, req, res);
  },

  edit: async function(req, res) {
    if (req.file) {
      const tmp_path = req.file.path;
      const file_final_name =
        new Date().getTime() + "_" + req.file.originalname;
      const final_path =
        process.env.ROOT_PATH + process.env.LOGOIMAGE_DESTINATION + file_final_name;
      final_url =
        process.env.API_URL + process.env.LOGOIMAGE_DESTINATION + file_final_name;
      fs.rename(tmp_path, final_path, err => {
        if (err) {
          return "File linking failed";
        }
      });
      req.body.value = final_url;
    }
    let result = await settingServ.save(req.body);
    utils.sendResponse(result, req, res);
  },

  delete: async function(req, res) {
    let result = await settingServ.delete(req.params.id);
    utils.sendResponse(result, req, res);
  },

  listAll: async function(req, res, next) {
    let { start, length } = req.params;
    let result = await settingServ.listAll(
      parseInt(start),
      parseInt(length),
      req.body,
      next
    );
    utils.sendResponse(result, req, res);
  },

  getDetail: async function(req, res) {
    let result = await settingServ.getDetail(req.params.id);
    utils.sendResponse(result, req, res);
  }
};
