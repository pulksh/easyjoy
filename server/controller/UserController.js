const userServ = require("../services/UserService");
//const verificationserv = require("../services/UserVerificationService");
const utils = require("../utils/utils");
const multer = require("multer");
const upload = multer({ dest: "uploads/temp" });
const fs = require("fs");
module.exports = {
  sendOtp: async function (req, res) {
    let mobileNumber = req.body.mobileNumber;
    let name = req.body.name;
    result = await verificationserv.sendOtp(mobileNumber, name);
    utils.sendResponse(result, req, res);
  },
  checkMobileNumber: async function (req, res, next) {
    let mobileNumber = req.body.mobileNumber;
    let name = req.body.name;
    result = await userServ.checkMobileNumber(mobileNumber, next);
    utils.sendResponse(result, req, res);
  },
  validateOtp: async function (req, res) {
    const mobileNumber = req.body.mobileNumber;
    const otp = req.body.otp;
    let result = await verificationserv.validateOtp(otp, mobileNumber);
    utils.sendResponse(result, req, res);
  },
  add: async function (req, res, next) {
    if (req.files && req.files[0]) {
      const tmp_path = req.files[0].path;
      const file_final_name =
      new Date().getTime() + "_" + req.files[0].originalname;
      const final_path =
        process.env.ROOT_PATH + process.env.AGGRIMENT_DESTINATION + file_final_name;
      final_url =
        process.env.API_URL + process.env.AGGRIMENT_DESTINATION + file_final_name;
      fs.rename(tmp_path, final_path, err => {
        if (err) {
          return "File linking failed";
        }
      });
      req.body.agreement = final_url;
    }
    
    let result = await userServ.save(req.body, req.currUser, next);
    utils.sendResponse(result, req, res);
  },

  login: async function (req, res, next) {
    let username = "-1-1-1-1-1"; //some junk string initially
    let password = "-1-1-1-1-1"; //some junk string initially
    let type = "";
    let logintype = "";
    if (req.body.email) {
      username = req.body.email;
      type = "email";
    }
    if (req.body.mobileNumber) {
      username = req.body.mobileNumber;
      type = "otp";
    }
    if (req.body.otp) {
      password = req.body.otp;
    }
    if (req.body.password) {
      password = req.body.password;
    }
    if (req.body.type) {
      logintype = req.body.type;
    }
    let result = await userServ.login(
      username,
      password,
      type,
      logintype,
      next
    );
    utils.sendResponse(result, req, res);
  },

  list: async function (req, res, next) {
    let { start, length } = req.params;
    let result = await userServ.list(
      parseInt(start),
      parseInt(length),
      req.body,
      req.currUser,
      next
    );
    utils.sendResponse(result, req, res, next);
  },
  sellerList: async function (req, res, next) {
    let { start, length } = req.params;
    let result = await userServ.sellerList(
      parseInt(start),
      parseInt(length),
      next
    );
    utils.sendResponse(result, req, res);
  },

  get: async function (req, res) {
    let { id } = req.params;
    let result = await userServ.get(id);
    utils.sendResponse(result, req, res);
  },

  edit: async function (req, res, next) {
    if (req.files && req.files[0]) {
      const tmp_path = req.files[0].path;
      const file_final_name =
      new Date().getTime() + "_" + req.files[0].originalname;
      const final_path =
        process.env.ROOT_PATH + process.env.AGGRIMENT_DESTINATION + file_final_name;
      final_url =
        process.env.API_URL + process.env.AGGRIMENT_DESTINATION + file_final_name;
      fs.rename(tmp_path, final_path, err => {
        if (err) {
          return "File linking failed";
        }
      });
      req.body.agreement = final_url;
    }
    let result = await userServ.save(req.body, req.currUser, next);
    utils.sendResponse(result, req, res);
  },

  delete: async function (req, res, next) {
    let result = await userServ.delete(req.params.id, next);
    utils.sendResponse(result, req, res);
  },
  forgotPassword: async function (req, res, next) {
    let result = await userServ.forgotPassword(req.body, res, next);
    utils.sendResponse(result, req, res);
  },
  resetPassword: async function (req, res) {
    // let result = await userServ.resetPassword(req.body.token,req.body.password, req.body.password);
    let result = await userServ.resetPassword(
      req.params.token,
      req.body.newPassword,
      req.body.verifyPassword
    );
    utils.sendResponse(result, req, res);
  },

  // Send mail to admin
  sendMail: async function (req, res) {
    let result = await userServ.sendMailToAdmin(req.params.id);
    utils.sendResponse(result, req, res);
  },

  activate: async function (req, res) {
    let result = await userServ.activate(req.body);
    utils.sendResponse(result, req, res);
  },

};
