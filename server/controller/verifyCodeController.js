const codeServ = require("../services/CodeVerifyService");
const utils = require("../utils/utils");
const multer = require("multer");
const upload = multer({ dest: "uploads/temp" });
const fs = require("fs");
module.exports = {
    verifyCode: async function(req, res) {
    let code = req.body.code;
    let currentUser =  req.currUser;
    result = await codeServ.verifyCode(code,currentUser);
    utils.sendResponse(result, req, res);
  }
};
