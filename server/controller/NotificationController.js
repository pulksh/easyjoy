const notificationServ = require("../services/NotificationService");
const utils = require("../utils/utils");

module.exports = {
  add: async function (req, res, next) {
    let result = await notificationServ.add(req.body, next);
    utils.sendResponse(result, req, res);
  },

  getDetail: async function (req, res, next) {
    let result = await notificationServ.getDetail(req.params.id, next);
    utils.sendResponse(result, req, res);
  },

  list: async function (req, res, next) {
    let { start, length } = req.params;
    let result = await notificationServ.listAll(
      parseInt(start),
      parseInt(length),
      req.body,
      req.currUser,
      next
    );
    utils.sendResponse(result, req, res);
  },
  delete: async function(req, res, next) {
    let result = await notificationServ.delete(req.params.id, next);
    utils.sendResponse(result, req, res);
  },
  sendMail: async function (req, res) {
    let result = await notificationServ.sendMail(req.params.id);
    utils.sendResponse(result, req, res);
  },
};
