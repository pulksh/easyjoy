const activityLogServ = require("../services/ActivityLogService");
const utils = require("../utils/utils");

module.exports = {
  add: async function(req, res, next) {
    let result = await activityLogServ.add(req.body, next);
    utils.sendResponse(result, req, res);
  },

  listAll: async function(req, res, next) {
    let { start, length } = req.params;
    let result = await activityLogServ.listAll(
      parseInt(start),
      parseInt(length),
      req.body,
      req.currUser,
      next
    );
    utils.sendResponse(result, req, res);
  },

  getCsvData:async function(req, res, next) {
    let result = await activityLogServ.getCsvData(req, next);
    utils.sendResponse(result, req, res);
  }
};
