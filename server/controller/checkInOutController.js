const checkInOutServ = require("../services/checkInOutService");
const utils = require("../utils/utils");

module.exports = {
  add: async function(req, res, next) {
    let result = await checkInOutServ.save(req.body, next);
    utils.sendResponse(result, req, res);
  },

  edit: async function(req, res, next) {
    let result = await checkInOutServ.save(req.body, next);
    utils.sendResponse(result, req, res);
  },

  delete: async function(req, res, next) {
    let result = await checkInOutServ.delete(req.params.id, next);
    utils.sendResponse(result, req, res);
  },

  listAll: async function(req, res, next) {
    let { start, length } = req.params;
    let result = await checkInOutServ.listAll(
      parseInt(start),
      parseInt(length),
      req.body,
      req.currUser,
      next
    );
    utils.sendResponse(result, req, res);
  },

  getDetail: async function(req, res, next) {
    let result = await checkInOutServ.getDetail(req.params.id, next);
    utils.sendResponse(result, req, res);
  },

  getCsvData:async function(req, res, next) {
    let result = await checkInOutServ.getCsvData(req, next);
    utils.sendResponse(result, req, res);
  },

  sendMail: async function(req, res, next) {
    let result = await checkInOutServ.sendMail(req.body, next);
    utils.sendResponse(result, req, res);
  },
};
