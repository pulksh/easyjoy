const pageServ = require("../services/PageService");
const utils = require("../utils/utils");

module.exports = {
  add: async function(req, res, next) {
    let result = await pageServ.save(req.body, next);
    utils.sendResponse(result, req, res);
  },
  
  edit: async function(req, res, next) {
    let result = await pageServ.save(req.body, next);
    utils.sendResponse(result, req, res);
  },

  delete: async function(req, res, next) {
    let result = await pageServ.delete(req.params.id, next);
    utils.sendResponse(result, req, res);
  },

  listAll: async function(req, res, next) {
    let { start, length } = req.params;
    let result = await pageServ.listAll(
      parseInt(start),
      parseInt(length),
      req.body,
      next
    );
    utils.sendResponse(result, req, res);
  },

  getDetail: async function(req, res, next) {
    let result = await pageServ.getDetail(req.params.id, next);
    utils.sendResponse(result, req, res);
  },
  getDetailByName: async function (req, res,next) {
    let result = await pageServ.getDetailByName(req.params.id, next);
    utils.sendResponse(result, req, res);
},
};
