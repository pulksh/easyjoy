const socialServ = require("../services/socialLoginService");
const utils = require("../utils/utils");
const multer = require("multer");
module.exports = {
  add: async function (req, res, next) {
    let result = await socialServ.socialLogin(req.body, req.currUser, next);
    utils.sendResponse(result, req, res);
  },
};
