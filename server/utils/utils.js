const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
require("dotenv").config();
const moment = require("moment");

module.exports = {
  
  emailSend: async function(params){
    try{
      let transporter = await nodemailer.createTransport({
        host: process.env.EMAIL_HOST,
        port: process.env.EMAIL_PORT,
        secure:process.env.EMAIL_SECURE, // true for 465, false for other ports
        auth: {
          user: process.env.EMAIL,
          pass: process.env.EMAIL_PASSWORD
        }
      });
      let info = await transporter.sendMail({
        from: params.from,
        to: params.to,
        subject: params.subject,
        html:
          params.text
      });
    }catch(err){
      console.log("================Mail not Send================");
      console.log(params);
      console.log(err.message);
    }
  },

  jwtEncode: function(paylod) {
    let token = jwt.sign(paylod, process.env.JWT_KEY);
    return token;
  },
  jwtDecode: function(token) {
    let paylodDecoded = jwt.verify(token, process.env.JWT_KEY);
    return paylodDecoded;
  },
  sendResponse: function(result, req, res) {
    res.json(result);
  },
  sendResponseFile: function(result, req, res) {
    if (
      result.err != undefined &&
      result.err != null &&
      result.err.length > 0
    ) {
      res.status(400).send(result.err);
    } else {
      res.download(result);
    }
  },
  checkRole: (user, role) => {
    for (i = 0; i < user.roles.length; i++) {
      if (user.roles[i] == role) {
        return true;
      }
    }
    return false;
  },
  ucfirst: str => {
    return str.charAt(0).toUpperCase() + str.slice(1);
  },
  logIt: (message, type) => {
    //type-> info,error
    if (type == "error") {
      console.error("=====" + new Date().toString() + "=====");
      console.error(message);
    } else if (type == "info") {
      console.log("=====" + new Date().toString() + "=====");
      console.log(message);
    }
  },
  getYearFolderName(dateObj) {
    let dt = dateObj.getFullYear() 
    return dt;
  },
  getMonthFolderName(dateObj) {
    let dt =  dateObj.getMonth()
    return dt+1;
  },
  getDateFolderName(dateObj) {
    let dt =  dateObj.getDate();
    return dt;
  },
  convertDateTimeInto24Hr(dateObj) {
    let time = moment(dateObj).format("HH:mm")
    return time;
  },
  convertDateTimeInDay(dateObj) {
    
    let date = moment(dateObj, "YYYY-MM-DD")
    let day =  date.day()
    return day
  },
   convertDate(date){
    let dateString = date;
    let dateObj = new Date(dateString);
    let momentObj = moment(dateObj);
    let momentString = momentObj.format('YYYY-MM-DD');
    return momentString
 }
};
