let Sequelize = require("sequelize");
let User = require('./User');

const activityLog = sequelize.define("activity_log", {
  user_id: {
    type: Sequelize.INTEGER,
  },
  login_time: {
    type: Sequelize.DATE,
  },
});

activityLog.belongsTo(User, { foreignKey: "user_id" });

module.exports = activityLog;
