let Sequelize = require("sequelize");
let User = require('./User');
let Location = require('./Location');

const Review = sequelize.define("review", {
  review: {
    type: Sequelize.STRING
  },
  rating: {
    type: Sequelize.NUMBER
  },
  review_order_hygiene: {
    type: Sequelize.NUMBER
  },
  review_clean_pot_seat: {
    type: Sequelize.NUMBER
  },
  review_handwash: {
    type: Sequelize.NUMBER
  },
  review_dryseat: {
    type: Sequelize.NUMBER
  },
});

Review.belongsTo(User, { foreignKey: 'user_id' });
Review.belongsTo(Location, { foreignKey: 'location_id' });

module.exports = Review;
