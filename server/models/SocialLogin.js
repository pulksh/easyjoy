let Sequelize = require("sequelize");
let User = require('./User');

const SocialLogin = sequelize.define("social_logins", {
  type: {
    type: Sequelize.STRING
  },
  token: {
    type: Sequelize.NUMBER
  },
});

SocialLogin.belongsTo(User, { foreignKey: 'user_id' });

module.exports = SocialLogin;
