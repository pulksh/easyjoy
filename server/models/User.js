let Sequelize = require("sequelize");

const User = sequelize.define(
  "user",
  {

    title: {
      type: Sequelize.STRING,
     // defaultValue: "mr"
    },
    first_name: {
      type: Sequelize.STRING,
     // allowNull: false
    },
  
    last_name: {
      type: Sequelize.STRING
    },
    email: {
      type: Sequelize.STRING,
      unique: true
    },
    phone: {
      type: Sequelize.INTEGER,
    },
    address_1: {
      type: Sequelize.STRING,
    
    },
    address_2: {
      type: Sequelize.STRING,
    },
    address_3: {
      type: Sequelize.STRING,
    },
    city: {
      type: Sequelize.STRING,
    },
    state: {
      type: Sequelize.STRING,
    },
    role: {
      type: Sequelize.NUMBER // 1- Admin 2- User 3- Owner
    },
     password: {
       type: Sequelize.STRING
     },    
     reset_password_token: {
      type: Sequelize.STRING,
      allowNull: true
    },
    account_verified_token: {
      type: Sequelize.STRING,
      allowNull: true
    },
    reset_password_expires: {
      type: Sequelize.DATE,
      allowNull: true
    },
    agreement: {
      type: Sequelize.STRING,
    },
    is_active: {
      type: Sequelize.BOOLEAN
    },
  },
  { defaultScope: { attributes: { exclude: ["password"] } } }
);
module.exports = User;
