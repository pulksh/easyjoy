let Sequelize = require("sequelize")

const Pages = sequelize.define(
    "page",{
    name:{
        type: Sequelize.STRING
    },
    title:{
        type: Sequelize.STRING
    },
    content:{
        type: Sequelize.STRING
    },
    meta_keyword : {
        type : Sequelize.STRING
    },
    meta_title : {
        type : Sequelize.STRING
    },
    meta_desc : {
        type : Sequelize.STRING
    } 
  
});
module.exports = Pages;