let Sequelize = require("sequelize");
const Location = require("./Location");

const LocationAdditionalImage = sequelize.define("location_additional_image", {
additional_image:{
    type: Sequelize.STRING
}
});
LocationAdditionalImage.belongsTo(Location);
Location.hasMany(LocationAdditionalImage);

module.exports = LocationAdditionalImage;