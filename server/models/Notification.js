const Location = require("./Location");
const User = require("./User");
let Sequelize = require("sequelize");
const Notification = sequelize.define("notification", {
  // user_id: {
  //   type: Sequelize.INTEGER,
  // },
  // location_id: {
  //   type: Sequelize.INTEGER,
  // },
  message: {
    type: Sequelize.STRING,
  },
  status: {
    type: Sequelize.STRING,
    default: "Pending",
  },
  sendmail: {
    type: Sequelize.INTEGER,
    default: 0
  }
});

Notification.belongsTo(User, { foreignKey: 'user_id' });
Notification.belongsTo(Location, { foreignKey: 'location_id' });

module.exports = Notification;