let Sequelize = require("sequelize");
let User = require('./User');
let Location = require('./Location');

const checkInOut = sequelize.define("check_in_out", {
  checkin_time: {
    type: Sequelize.DATE
  },
  checkout_time: {
    type: Sequelize.DATE
  },
  code: {
    type: Sequelize.STRING
  },
  verified: {
    type: Sequelize.BOOLEAN,
    defaultValue: 0
  },
  verification_time:  {
    type: Sequelize.DATE
  },
});

checkInOut.belongsTo(User, { foreignKey: 'user_id' });
checkInOut.belongsTo(Location, { foreignKey: 'location_id' });

module.exports = checkInOut;
