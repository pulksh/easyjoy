let Sequelize = require("sequelize");

const Setting = sequelize.define("setting", {
  content_type:{
    type: Sequelize.NUMBER
  },
  name: {
    type: Sequelize.STRING
  },
  value: {
    type: Sequelize.STRING
  },
  is_editable:{
    type: Sequelize.BOOLEAN
  }
});
module.exports = Setting;
