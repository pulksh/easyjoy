let Sequelize = require("sequelize");
const Location = sequelize.define("location", {
  name: {
    type: Sequelize.STRING,
  },
  user_id: {
    type: Sequelize.INTEGER,
  },
  description: {
    type: Sequelize.STRING,
  },
  rating: {
    type: Sequelize.INTEGER,
  },
  morning_ot: {
    type: Sequelize.TIME
  },
  morning_ct: {
    type: Sequelize.TIME
  },
  evening_ot: {
    type: Sequelize.TIME
  },
  evening_ct: {
    type: Sequelize.TIME
  },
  close_day: {
    type: Sequelize.INTEGER
  },
  geo_coordinates: {
    type: Sequelize.GEOMETRY("POINT", 4326),
  },
  url: {
    type: Sequelize.STRING,
  },
  is_active: {
    type: Sequelize.BOOLEAN,
    allowNull: true,
  },
  show_home_page: {
    type: Sequelize.BOOLEAN,
  },
  featured_image: {
    type: Sequelize.STRING,
  },
  meta_title: {
    type: Sequelize.STRING
  },
  meta_keyword: {
    type: Sequelize.STRING
  },
  meta_desc: {
    type: Sequelize.STRING
  },
});

module.exports = Location;
const Review = require("./Review");
Location.hasMany(Review, {
  foreignKey: "location_id",
});
