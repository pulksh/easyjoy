const User = require("../models/User");
const jwt = require("jsonwebtoken");
const Sequelize = require("sequelize");
const bcrypt = require("bcrypt");
const utils = require("../utils/utils");
const crypto = require("crypto");
const Op = Sequelize.Op;
const moment = require("moment");
const fs = require("fs");
const SocialLogin = require("../models/SocialLogin");
const ActivityLog = require("../models/ActivityLog");

module.exports = {
  save: async function (user, currUser, next) {
    const saltRounds = 10;
    let result = null;
    try {
      if (user.password) {
        let salt = bcrypt.genSaltSync(saltRounds);
        let hash = bcrypt.hashSync(user.password, salt);
        user.password = hash;
      }
      if (user.id) {
        let userAgreementFile = await User.findOne({
          where: { id: user.id },
        });
        
        if (userAgreementFile.agreement && user.agreement && user.agreement !== userAgreementFile.agreement) {
          let fileName = userAgreementFile.agreement.split("/");
          fileName = fileName[fileName.length - 1];
          const path =
            process.env.ROOT_PATH +
            process.env.AGGRIMENT_DESTINATION +
            fileName;
          fs.unlink(path, (err) => {
            if (err) {
              console.error("File is not overide");
            }
          });
        }

        Object.keys(user).forEach((el) => {
          user[el] = (user[el] == 'null' ? null : user[el]);
        });
        result = await User.update(
          {
            ...user,
          },
          { where: { id: user.id } }
        );
        if (result.includes(1)) {
          return { message: "Updated Successfully" };
        } else {
          return { error: "Something went wrong try again" };
        }
      } else {
        let usr;
        usr = await User.findOne({
          where: { email: user.email },
        });
        if (usr && usr.email === user.email) {
          return { error: "Email already registerd" };
        }
        result = await User.create(user);

        if (result && result.id) {
          
          let params = {
            from: process.env.EMAIL,
            // to: result.email,
            to: "easyjoy009@gmail.com",
            subject: "Registration Successful",
            text: `hey,\n\n
              This is confirmation that your registration is successful on  ${moment().format(
                "DD-MM-YYYY"
              )}. Now you can login with your registered email and password.
              \n\nRegards,
              \nEasyjoy Team`,
          };
          utils.emailSend(params);
        } else if (result === null) {
          throw Error("Email id does not exists, please check");
        }
        
        return { result: result, message: "Successfully Registered" };
      }
    } catch (err) {
      return { error: err.message };
    }
  },

  checkMobileNumber: async function (mobileNumber, next) {
    try {
      user = await User.findOne({
        where: { phone: mobileNumber },
        raw: true,
      });
      return user;
    } catch (err) {
      next(err);
    }
  },

  delete: async function (id, next) {
    let result = null;
    try {
      result = await User.destroy({ where: { id: id } });
      await SocialLogin.destroy({ where: { user_id: id } });
      return { message: " user deleted successfully" };
    } catch (err) {
      next(err);
    }
  },

  list: async function (start, length, searchQry, currentUser, next) {
    let result = null;
    try {
      let whereClause = {};
      Object.keys(searchQry).forEach((el) => {
        whereClause[el] = { [Op.like]: "%" + searchQry[el] + "%" };
      });
      result = await User.findAndCountAll({
        where: whereClause,
        offset: start,
        limit: length,
        order: [["createdAt", "DESC"]],
      });
      return result;
    } catch (err) {
      next(err);
    }
  },

  get: async function (id) {
    let result = null;
    try {
      result = await User.findOne({ where: { id } });
      if (!result) {
        return (result = {
          error: "User Not Found",
        });
      }
      return result;
    } catch (err) {
      return { error: err.message };
    }
  },

  login: async function (username, password, type, logintype, next) {
    let token = null;
    try {
      let user = null;
      if (type == "email") {
        user = await User.findOne({
          where: { email: username },
          attributes: { include: ["password"] },
        });

        if (!user) {
          return { error: "Email or password is incorrect." };
        }
        if (logintype == "backend") {
          if (user.role != 1 && user.role != 3) {
            throw Error("Permission Denied.");
          }
        }
        let isMatched = await bcrypt.compare(password, user.password);
        if (!isMatched) {
          return { error: "Email or password is incorrect." };
        }
        user.password = undefined;
      } else {
        let verf = await UserVerifServ.validateOtp(password, username);
        if (!verf.valid) {
          return { error: "Mobile number or OTP is incorrect." };
        }
        user = await User.findOne({
          where: { phone: username },
        });
        if (!user) {
          return { error: "Mobile number or OTP is incorrect2." };
        }
      }
      token = await jwt.sign(
        { email: user.email, id: user.id },
        process.env.JWT_KEY
      );
      await ActivityLog.create({user_id: user.id, login_time: new Date()});
      return {
        result: user,
        token,
        message: "Login successful",
      };
    } catch (err) {
      return { error: err.message };
      // next(err);
    }
  },

  resetPassword: async function (token, newPassword, verifyPassword) {
    let result = null;
    try {
      result = await User.findOne({
        where: { reset_password_token: token },
        raw: true,
      }); //,reset_password_expires:{$gt: Date}});
      if (result) {
        if (newPassword === verifyPassword) {
          let newPasswordEnc = bcrypt.hashSync(newPassword, 10);
          await User.update(
            {
              reset_password_token: null,
              reset_password_expires: null,
              password: newPasswordEnc,
            },
            { where: { id: result.id } }
          );
          let params = {
            from: process.env.EMAIL,
            // to: result.email,
            to: "easyjoy009@gmail.com",
            subject: "Account Verified",
            subject: "Password reset confirmation",
            text:
              "This is a confirmation that the password for your account " +
              result.email +
              " has just been changed.\n",
          };
          utils.emailSend(params);
          return {
            result: true,
            message: "Password changed successfully",
          };
        } else {
          throw Error("Your password does't match.");
        }
      } else {
        throw Error(
          "Sorry your link is expired, generate new link through forget password."
        );
      }
    } catch (err) {
      return { error: err.message };
    }
  },

  forgotPassword: async function (data, flag) {
    let result;
    try {
      result = await User.findOne({ where: { email: data.email }, raw: true });
      if (result && result.id) {
        token = crypto.randomBytes(20).toString("hex");
        result.reset_password_token = token;
        result.reset_password_expires = Date.now() + 86400000;

        await User.update(
          {
            allowed_access: result.allowed_access,
            reset_password_token: result.reset_password_token,
            reset_password_expires: result.reset_password_expires,
          },
          { where: { id: result.id } }
        );

        if (data.flag && data.flag == "admin_fogot_password") {
          url = process.env.ADMIN_BASE_URL;
        } else {
          url = process.env.FRONTEND_BASE_URL;
        }
        let params = {
          from: process.env.EMAIL,
          // to: data.email,
          to: "easyjoy009@gmail.com",
          subject: "Password help has arrived!",
          text: `\n<br/>You are receiving this because you (or someone else) have requested the reset of the password for your account.
            \n<br/><br/>Please click on the following link, or paste this into your browser to complete the process:
            \n<br/><br/>${url}/reset/${token}
            \n<br/><br/>If you did not request this, please ignore this email and your password will remain unchanged.
            \n<br/><br/><br/>Kind Regards, 
            \n<br/><br/>Easyjoy Team`,
        };
        utils.emailSend(params);
      } else if (result === null) {
        throw Error("Email id does not exists, please check");
      }
      return {
        result: true,
        message: "Mail has been sent, please check your inbox or spam folder",
      };
    } catch (err) {
      return { error: err.message };
    }
  },

  // Send mail to admin
  sendMailToAdmin: async function (ownerId) {
    let result = null;
    try {
      result = await User.findOne({ where: { id: ownerId } });

      if (result && result.id) {
        token = crypto.randomBytes(20).toString("hex");
        result.account_verified_token = token;
        
        await User.update(
          {
            is_active: 0,
            account_verified_token: result.account_verified_token,
          },
          { where: { id: result.id } }
        );
      }
      let params = {
        from: process.env.EMAIL,
        // to: result.email,
        to: "easyjoy009@gmail.com",
        subject: "Notification for activating your account",
        text:
          `Hi ${result.first_name} ${result.last_name},`+
          `<br/><br/>Please click on below mentioned link to activate your account associated with Easyjoy. Before Accepting please look at our <a href="${process.env.FRONTEND_BASE_URL}/pg/terms_of_use" style="color:#17a2b8">Terms of Use</a> policy.`+
          `<br/><br/><a href="${process.env.FRONTEND_BASE_URL}/activate/${token}" style="color:#FFF;background-color:#007bff;height:20px;line-height:20px;padding:5px;border-radius:4px;">Accept It</a>` +
          `<br/><br/>Regards` +
          `<br/>Easyjoy Team`,
      };
      utils.emailSend(params);

      return {
        result: true,
        message: "Account verification mail send successfully",
      };
    } catch (err) {
      return { error: err.message };
    }
  },

  activate: async function (tokenObj) {
    let result = null;
    try {
      result = await User.findOne({ where: { account_verified_token: tokenObj.token } });

      if(result && result.id){
        await User.update(
          {
            account_verified_token:null,
            is_active: 1,
          },
          { where: { id: result.id } }
        );
        return {status: "success"}
      }
      else{
        return {status: "failed"}
      }
    }
    catch(err){
      return {status: "failed"}
    }
  },
};
