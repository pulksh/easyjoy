const CheckInOut = require("../models/checkInOut");
const Location = require("../models/Location");
const jwt = require("jsonwebtoken");
const Sequelize = require("sequelize");
const nodemailer = require("nodemailer");
const bcrypt = require("bcrypt");
const fs = require("fs");
const utils = require("../utils/utils");
const crypto = require("crypto");
const Op = Sequelize.Op;
module.exports = {
  verifyCode: async function (vrfyCode, currentUser) {
    let result = null;
    try {
      let locList = await Location.findAndCountAll({
        where: { user_id: currentUser.id },
        raw: true,
      });
      let locIds= []
      for(i=0; i<locList.rows.length; i++){
        locIds.push(locList.rows[i].id)
      }
           
      result = await CheckInOut.findOne({
        where: {
          location_id: {
            [Op.in]: locIds
          },
          code: vrfyCode
        },
        raw: true,
      });
      if (result) {
        if(result.verified == 1){
          return { error: "Code already verified" };
          throw Error("Code already verified");
        } else {
          await CheckInOut.update(
            {
              verified: 1,
              verification_time: new Date()
            },
            { where: { code: result.code } }
          );
        }
        return {
          result: true,
          message: "Code is verified successfully",
        };
      }
    } catch (err) {
      return { error: err.message };
    }
  },
};
