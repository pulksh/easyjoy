const checkInOut = require("../models/checkInOut");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
let User = require("../models/User");
let Location = require("../models/Location");
const moment = require("moment");
module.exports = {
  save: async function (check_in_out, next) {
    let result = null;

    try {
      if (check_in_out.id) {
        result = await checkInOut.update(
          {
            ...check_in_out,
          },
          { where: { id: check_in_out.id } }
        );
        if (result.includes(1)) {
          result = "Updated Successfully";
        } else {
          result = "Something went wrong try again";
        }
      } else {
        let randomNumber = Math.floor(100000 + Math.random() * 900000);
        check_in_out.code = randomNumber;
        result = await checkInOut.create(check_in_out);
      }
    } catch (err) {
      next(err);
    }
    return result;
  },
  delete: async function (id, next) {
    let result = null;
    try {
      result = await checkInOut.destroy({ where: { id: id } });
    } catch (err) {
      next(err);
    }
    return result;
  },
  listAll: async function (start, length, searchQry, currentUser, next) {
    try {
      let whereClause = {};
      Object.keys(searchQry).forEach((el) => {
        if (el == "location") {
          whereClause["location_id"] = searchQry[el];
        } else {
          whereClause[el] = { [Op.like]: "%" + searchQry[el] + "%" };
        }
      });

      if (currentUser) {
        if (currentUser.role == "3") {
          let locRes = await Location.findAndCountAll({
            where: { user_id: { [Op.eq]: currentUser.id } },
            order: [["createdAt", "DESC"]],
          });
          let locArr = locRes.rows.map((loc) => {
            return loc.id;
          });
          whereClause.location_id = { [Op.in]: locArr };
        }
      }

      result = await checkInOut.findAndCountAll({
        where: whereClause,
        include: [{ model: User }, { model: Location }],
        offset: start,
        limit: length,
        order: [["createdAt", "DESC"]],
      });
      return result;
    } catch (err) {
      next(err);
    }

    return result;
  },
  getDetail: async function (id, next) {
    let result = null;
    try {
      result = await checkInOut.findOne({ where: { id } });
      return result;
    } catch (err) {
      next(err);
    }
  },
  //Get csv data
  getCsvData: async function () {
    try {
      let whereClause = {};
      // Object.keys(searchQry).forEach((el) => {
      //   if (el == "location") {
      //     whereClause["location_id"] = searchQry[el];
      //   } else {
      //     whereClause[el] = { [Op.like]: "%" + searchQry[el] + "%" };
      //   }
      // });
      let data = await checkInOut.findAndCountAll({
        where: whereClause,
        include: [{ model: User }, { model: Location }],
        order: [["createdAt", "DESC"]],
      });

      let finalresult = [];
      for (let i = 0; i < data.rows.length; i++) {
        data1 = {
          "User Name":
            data.rows[i].user && data.rows[i].user.first_name
              ? data.rows[i].user.first_name
              : "",
          Email:
            data.rows[i].user && data.rows[i].user.email
              ? data.rows[i].user.email
              : "",
          Location:
            data.rows[i].location && data.rows[i].location.name
              ? data.rows[i].location.name
              : "",
          "Checkin Time": data.rows[i].checkin_time
            ? moment(data.rows[i].checkin_time).format("YYYY-MM-DD hh:mm A")
            : "",
          "Checkout Time": data.rows[i].checkout_time
            ? moment(data.rows[i].checkout_time).format("YYYY-MM-DD hh:mm A")
            : "",
          "Is Verified": data.rows[i].verified ? "YES" : "NO",
          "Verification Time": data.rows[i].verification_time
            ? moment(data.rows[i].verification_time).format(
                "YYYY-MM-DD hh:mm A"
              )
            : "",
        };
        finalresult.push(data1);
      }
      result = {
        data: finalresult,
      };
      return result;
    } catch (err) {
      return {error: err.message }
    }
  },

  // Send mail to owner
  sendMail: async function (mailBody, next) {
    let result = null;
    try {
      let locResp = await Location.findOne({ where: { id: mailBody.location_id } });
      if(locResp) {
        let userResp = await User.findOne({ where: { id: locResp.user_id } });
        if(userResp.email){
          let params = {
            from: process.env.EMAIL,
            // to: userResp.email,
            to: "easyjoy009@gmail.com",
            subject: "Notification to owner",
            text: `hey,\n\n
              Someone want to use your restaurant
              \n\nRegards,
              \nEasyjoy Team`,
          };
          utils.emailSend(params);
        }
      }
    } catch (err) {
      return {error: err.message }
    }
    return result;
  },
};
