const SocialLogin = require("../models/SocialLogin");
const User = require("../models/User");
const ActivityLog = require("../models/ActivityLog");
const jwt = require("jsonwebtoken");

module.exports = {
  socialLogin: async function (user) {
    let token = null;
    try {
        let socialUsr;
      if (user.token) {
        socialUsr = await SocialLogin.findOne({
          where: { token: user.token },
        });

        if (!socialUsr) {
          let usrObj = {
            title: user.title,
            first_name: user.first_name,
            email: user.email,
            phone: "",
            role: user.role,
            password: "",
          };
          // have to add logic to check email id first if if email exists then dont create new account and update old one
          socialUsr = await User.create(usrObj);
          if (socialUsr) {
            let socUser = {
              type: user.type,
              token: user.token,
              user_id: socialUsr.id,
            };
            await SocialLogin.create(socUser);
          }
        } else {
          socialUsr = await User.findOne({
            where: { id: socialUsr.user_id },
            attributes: { include: ["password"] },
          });
        }
      }

      token = await jwt.sign(
        { email: socialUsr.email, id: socialUsr.id },
        process.env.JWT_KEY
      );

      await ActivityLog.create({user_id: socialUsr.id, login_time: new Date()});
      return {
        result: socialUsr,
        token,
        message: "Login successful",
      };
    } catch (err) {
        return { error: err.message };
    }
  },
};
