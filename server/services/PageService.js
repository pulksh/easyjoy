const Pages = require("../models/Pages");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
module.exports = {
  save: async function(pages, next) {
    let result = null;

    try {
      if (pages.id) {
        result = result = await Pages.update(
          {
            ...pages
          },
          { where: { id: pages.id } }
        );
        if (result.includes(1)) {
          result = "Updated Successfully";
        } else {
          result = "Something went wrong try again";
        }
      } else {
        result = await Pages.create(pages);
      }
    } catch (err) {
      next(err);
    }
    return result;
  },
  delete: async function(id, next) {
    let result = null;
    try {
      result = await Pages.destroy({ where: { id: id } });
    } catch (err) {
      next(err);
    }
    return result;
  },
  listAll: async function(start, length, searchQry, next) {
    let result = null;
    try {
      let whereClause = {};
      Object.keys(searchQry).forEach(el => {
        whereClause[el] = { [Op.like]: "%" + searchQry[el] + "%" };
      });
      result = await Pages.findAndCountAll({
        where: whereClause,
        offset: start,
        limit: length,
        order: [["createdAt", "DESC"]]
      });
     
      return result;
    } catch (err) {
      next(err);
    }

    return result;
  },
  getDetail: async function(id, next) {
    let result = null;
    try {
      result = await Pages.findOne({ where: { id: id } });
      return result;
    } catch (err) {
      next(err);
    }
  },
  getDetailByName:  async function(name){
    let result = {data:null,err:null};
    try{
        result.data = await Pages.findOne({ where : {name: name }});
      }
      catch(err){
        result.err =  [err];
      }
    return result;
},
};