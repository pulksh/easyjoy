const Notification = require("../models/Notification");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
let User = require("../models/User");
let Location = require("../models/Location");
const moment = require("moment");
const utils = require("../utils/utils");
var cron = require('node-cron');

cron.schedule('*/1 * * * *', async function(){
      let result = null;
      try {
        result = await  Notification.findAll({ where: {createdAt: {
          [Op.lt]: new Date(Date.now() - (60 * 60 * 1000)),
        }, sendmail: 0 } });

        result.map(data => {
           Notification.update(
                {
                 sendmail : 1,
                },
                { where: { user_id : data.user_id } }
              );
        })       
      }
      catch(err){
        return {status: "failed"}
      }
});

function getLocalTime(date, time, zone)
{
  if(time == '')
  {
      time = "00:00:00";
  }
  var input = date+" "+time;
  var fmt = "YYYY-MM-DD H:mm:ss"; 
  var m = moment.tz(input, fmt, zone);
  m.utc();
  var s = m.format(fmt); 
  s = s.split(" "); 
  return s;
} 


module.exports = {
  add: async function (notification) {
    let result = null;
    try {
      let locResp = await Location.findOne({
        where: { id: notification.location_id },
      });
      let user = await User.findOne({ where: { id: notification.user_id } });

      if (locResp) {
        let userResp = await User.findOne({ where: { id: locResp.user_id } });
        if (userResp.email) {
          let params = {
            from: process.env.EMAIL,
            // to: userResp.email,
            to: "easyjoy009@gmail.com",
            subject: "Notification to owner",
            text: `hey,\n\n
              ${user.first_name} want to use your restaurant at your location ${locResp.name}
              \n\nRegards,
              \nEasyjoy Team`,
          };
          utils.emailSend(params);

          let notifObj = {
            user_id: notification.user_id,
            location_id: notification.location_id,
            message: `Hey! ${user.first_name} want to use your restaurant at your location ${locResp.name}`,
            status: "check in",
          };
          result = await Notification.create(notifObj);
        }
      }
    } catch (err) {
      return { error: err.message };
    }
    return result;
  },

  listAll: async function (start, length, searchQry, currentUser) {
    try {
      // let whereClause = {};
      if (currentUser.role == "3") { 
        let locList = await Location.findAndCountAll({
          where: { user_id: currentUser.id },
          raw: true,
        });
        // Getting list of location ids 
        let locIds = [];
        for (i = 0; i < locList.rows.length; i++) {
          locIds.push(locList.rows[i].id);
        }
        result = await Notification.findAndCountAll({
          where: {
            location_id: {
              [Op.in]: locIds,
            },
          },
          include: [{ model: User }, { model: Location }],
          offset: start,
          limit: length,
          order: [["createdAt", "DESC"]],
        });
        return result;
      } else {
        result = await Notification.findAndCountAll({
          include: [{ model: User }, { model: Location }],
          offset: start,
          limit: length,
          order: [["createdAt", "DESC"]],
        });
        return result;
      }
    } catch (err) {
      return { error: err.message };
    }
  },

  delete: async function (id) {
    let result = null;
    try {
      result = await Notification.destroy({ where: { id: id } });
      return { message: "Notification deleted successfully" };
    } catch (err) {
      return { error: err.message };
    }
  },

  // Send Mail 
  sendMail: async function () {
    let result = null;
    try {
      notifResp = await Notification.findAndCountAll({
        include: [{ model: User }, { model: Location }],
        where: {
          sendmail: 0,
          createdAt: {
            [Op.lte]: moment()
              .subtract(60, "minute")
              .format("YYYY-MM-DD HH:MM"),
          },
        },
      });
      for (let i = 0; i < notifResp.rows.length; i++) {
        let params = {
          from: process.env.EMAIL,
          // to: notifResp.rows[i]["user"].email,
          to: "easyjoy009@gmail.com",
          subject: "Please rate your experience with Easyjoy.",
          text:
            `Hi ${notifResp.rows[i]["user"].first_name},` +
            `\nThanks for using Easyjoy platform. We request you to please rate your experience on our platform by using below link.` +
            `\n<a href="${process.env.FRONTEND_BASE_URL}/review/${notifResp.rows[i]["location"].id}" style="color:#FFF;background-color:#007bff;height:20px;line-height:20px;padding:5px;border-radius:4px;">Click here</a>` +
            `\n\nRegards` +
            `\nEasyjoy Team`,
        };
        
        utils.emailSend(params);

        result = await Notification.update(
          {
            sendmail: 1,
          },
          { where: { id: notifResp.rows[i].id } }
        );
      }

      return {
        result: true,
        message: "Mail send to user successfully",
      };
    } catch (err) {
      return { error: err.message };
    }
  },
};
