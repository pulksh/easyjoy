const Location = require("../models/Location");
const Review = require("../models/Review");
const LocationAdditionalImage = require("../models/LocationAdditionalImage");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const fs = require("fs");
module.exports = {
  save: async function (location, next) {
    let result = null;
    if (
      location.is_display_frontend === "" ||
      location.is_display_frontend === undefined
    ) {
      location.is_display_frontend = false;
    }
    if (location.is_active === "" || location.is_active === undefined) {
      location.is_active = false;
    }
    try {
      if (location.id) {
        let locationImage = await Location.findOne({
          where: { id: location.id },
        });
        if (
          location.featured_image &&
          location.featured_image !== locationImage.featured_image
        ) {
          let fileName = locationImage.featured_image.split("/");
          fileName = fileName[fileName.length - 1];
          const path =
            process.env.ROOT_PATH +
            process.env.LOCATIONIMAGE_DESTINATION +
            fileName;
          fs.unlink(path, (err) => {
            if (err) {
              console.error("File is not overide");
            }
          });
        }

        const point = {
          type: "Point",
          coordinates: [location.longitude, location.latitude],
        };
        location.geo_coordinates = point;

        result = await Location.update(
          {
            ...location,
          },
          { where: { id: location.id } }
        );
        let deleteAdditionalImgArr = null;
        let locationImgArr = await LocationAdditionalImage.findAll({
          where: { locationId: location.id },
          raw: true,
        });
        if (locationImgArr && locationImgArr.length) {
          deleteAdditionalImgArr = locationImgArr.map(async (e) => {
            return await LocationAdditionalImage.destroy({
              where: { id: e.id },
              raw: true,
            });
          });
        }
        if (location.additional_image) {
          let additionalImage = [];
          for (let i = 0; i < location.additional_image.length; i++) {
            additionalImage.push({
              locationId: location.id,
              additional_image: location.additional_image[i],
            });
          }
          await LocationAdditionalImage.bulkCreate(additionalImage);
        }
        if (result.includes(1)) {
          result = "Updated Successfully";
        } else {
          result = "Something went wrong try again";
        }
      } else {
        const point = {
          type: "Point",
          coordinates: [location.longitude, location.latitude],
        };
        location.geo_coordinates = point;
        result = await Location.create(location);
        if (location.additional_image) {
          let additionalImage = [];
          for (let i = 0; i < location.additional_image.length; i++) {
            additionalImage.push({
              locationId: result.id,
              additional_image: location.additional_image[i],
            });
          }
          await LocationAdditionalImage.bulkCreate(additionalImage);
        }
      }
    } catch (err) {
      return { error: err.message };
    }
    return result;
  },

  delete: async function (id) {
    let result = null;
    try {
      result = await Location.destroy({ where: { id: id } });
      return { message: " Location deleted successfully" };
    } catch (err) {
      return { error: err.message };
    }
  },

  list: async function (start, length, searchQry, currentUser) {
    let result = null;
    let result1 = null;
    try {
      let whereClause = {};
      if (currentUser) {
        if (currentUser.role == "3") {
          whereClause.user_id = currentUser.id;
        }
      }
      Object.keys(searchQry).forEach((el) => {
        if (el == "show_home_page") {
          whereClause["show_home_page"] = searchQry[el];
        } else if (el == "is_active") {
          whereClause["is_active"] = searchQry[el];
        } else if (el == "lat") {
          whereClause[Op.and] = [
            Sequelize.literal(
              `ST_Distance_Sphere(geo_coordinates,ST_GeomFromText('POINT(${
                searchQry.lng + " "
              } ${searchQry.lat})')) <= ${
                searchQry["distance"] ? searchQry["distance"] : 1000000
              }`
            ),
          ];
        } else if (el !== "lng" && el !== "distance") {
          whereClause[el] = { [Op.like]: "%" + searchQry[el] + "%" };
        }
      });

      const location = sequelize.literal(
        `ST_GeomFromText('POINT(${searchQry.lng + " "} ${searchQry.lat})')`
      );

      if (searchQry["lat"]) {
        result = await Location.findAndCountAll({
          where: whereClause,
          offset: start,
          limit: length,
          attributes: {
            include: [
              [
                sequelize.fn(
                  "ST_Distance_Sphere",
                  sequelize.literal("geo_coordinates"),
                  location
                ),
                "distance",
              ],
            ],
          },

          include: [
            {
              model: Review,
              attributes: ["id"],
            },
          ],
          order: [sequelize.literal("`distance` ASC")],
        });

        result1 = await Location.findAndCountAll({
          where: whereClause,
          offset: start,
          limit: length,
          order: [["createdAt", "DESC"]],
        });
      } else {
        result = await Location.findAndCountAll({
          where: whereClause,
          offset: start,
          limit: length,
          include: [
            {
              model: Review,
              attributes: ["id"],
            },
          ],
          order: [["createdAt", "DESC"]],
        });
        // Added same query again because of sequlize bug when used associaction inside include options
        // While using only include query it gives wrong count bcz of left outer join between tables
        result1 = await Location.findAndCountAll({
          where: whereClause,
          offset: start,
          limit: length,
          order: [["createdAt", "DESC"]],
        });
      }
      result.count = result1.count;
      return result;
    } catch (err) {
      return { error: err.message };
    }
  },
  get: async function (id) {
    let result = null;
    try {
      result = await Location.findOne({
        where: { id },
        include: [
          {
            model: Review,
            attributes: ["id"],
          },
        ],
      });

      if (!result) {
        return (result = {
          error: "Location Not Found",
        });
      }
      result.dataValues.latitude = result.geo_coordinates.coordinates[1];
      result.dataValues.longitude = result.geo_coordinates.coordinates[0];

      return result;
    } catch (err) {
      return { error: err.message };
    }
  },
  getLocationAdditionalImage: async function (id, next) {
    let result = null;
    try {
      result = await LocationAdditionalImage.findAll({
        where: { locationId: id },
      });
      return result;
    } catch (err) {
      next(err);
    }
  },
  deleteAdditionalImage: async function (id, next) {
    let result = null;
    try {
      let chkImagePath = await LocationAdditionalImage.findOne({
        where: { id: id },
      });
      result = await LocationAdditionalImage.destroy({ where: { id: id } });
      let fileName = chkImagePath.additional_image.split("/");
      fileName = fileName[fileName.length - 1];
      const path = "./uploads/locationAdditionalImage/" + fileName;
      fs.unlink(path, (err) => {
        if (err) {
          err = "File is not overide";
        } else {
          result;
        }
      });
    } catch (err) {
      next(err);
    }
    return result;
  },
};
