const Setting = require("../models/Setting");
const Sequelize = require("sequelize");
const fs = require("fs");
const Op = Sequelize.Op;
module.exports = {
  save: async function(setting) {
    let result = null;

    try {
      if (setting.id) {
        let settingImage = await Setting.findOne({ where: { id : setting.id } });
        if (settingImage.value && settingImage.content_type === 1 && setting.value.split("_")[1] !== settingImage.value.split("_")[1]) {
          let fileName = settingImage.value.split("/");
          fileName = fileName[fileName.length - 1];
          const path = "./uploads/logoImage/" + fileName;
         
          if(fs.existsSync(path))
            {
              fs.unlink(path, (err) => {
                if (err) {
                  throw new Error("File is not overide");
                }
              })
           }
        }

        result = result = await Setting.update(
          {
            ...setting
          },
          { where: { id: setting.id } }
        );
        if (result.includes(1)) {
          result = "Updated Successfully";
        } else {
          result = "Something went wrong try again";
        }
      } else {
        result = await Setting.create(setting);
      }
    } catch (err) {
        return{error: err.message};
    }
    return result;
  },
  delete: async function(id) {
    let result = null;
    try {
      result = await Setting.destroy({ where: { id: id } });
    } catch (err) {
      return{error: err.message};
    }
    return result;
  },
  listAll: async function(start, length, searchQry) {
    try {
      let whereClause = {};
      Object.keys(searchQry).forEach(el => {
        whereClause[el] = { [Op.like]: "%" + searchQry[el] + "%" };
      });
      result = await Setting.findAndCountAll({
        where: whereClause,
        offset: start,
        limit: length,
        order: [["createdAt", "DESC"]]
      });
      return result;
    } catch (err) {
      return{error: err.message};
    }

    return result;
  },
  getDetail: async function(id) {
    let result = null;
    try {
      result = await Setting.findOne({ where: { id } });
      return result;
    } catch (err) {
      return{error: err.message};
    }
  }
};
