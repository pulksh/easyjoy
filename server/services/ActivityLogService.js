const ActivityLog = require("../models/ActivityLog");
const moment = require("moment");
const Sequelize = require("sequelize");
let User = require("../models/User");
const Op = Sequelize.Op;

module.exports = {
  add: async function (activity, next) {
    let result = null;
    try {
      result = await ActivityLog.create(activity);
    } catch (err) {
      next(err);
    }
    return result;
  },

  listAll: async function (start, length, searchQry, next) {
    let result = null;
    try {
      let whereClause = {};
      Object.keys(searchQry).forEach((el) => {
        whereClause[el] = { [Op.like]: "%" + searchQry[el] + "%" };
      });
      result = await ActivityLog.findAndCountAll({
        where: whereClause,
        include: [{ model: User }],
        offset: start,
        limit: length,
        order: [["createdAt", "DESC"]],
      });

      return result;
    } catch (err) {
      next(err);
    }

    return result;
  },

  
  getCsvData: async function () {
    try {
      let whereClause = {};
      let data = await ActivityLog.findAndCountAll({
        where: whereClause,
        include: [{ model: User }],
        order: [["createdAt", "DESC"]],
        // raw:true
      });
      let finalresult = [];
      for (let i = 0; i < data.rows.length; i++) {
        console.log(data.rows[i].user,'data.rows[i].user')
        data1 = {
          "User Name":
            data.rows[i].user && data.rows[i].user.first_name
              ? data.rows[i].user.first_name
              : "",
          "Email":
            data.rows[i].user && data.rows[i].user.email
              ? data.rows[i].user.email
              : "",
          "Phone":
            data.rows[i].user && data.rows[i].user.phone
              ? data.rows[i].user.phone
              : "",
          "Login Time": data.rows[i].login_time
            ? moment(data.rows[i].login_time).format("YYYY-MM-DD hh:mm")
            : "",
        };
        console.log(data1,'data1data1')
        finalresult.push(data1);
      }
      result = {
        data: finalresult,
      };
      return result;
    } catch (err) {
      return {err: err.message }
    }
  },
};
