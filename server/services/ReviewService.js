const Review = require("../models/Review");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
let User = require('../models/User');
let Location = require('../models/Location');

module.exports = {
  save: async function(review, next) {
    let result = null;

    try {
      if (review.id) {
        result = await Review.update(
          {
            ...review
          },
          { where: { id: review.id } }
        );
        if (result.includes(1)) {
          await module.exports.updateLocationRating(review.location_id);
          result = "Updated Successfully";
        } else {
          result = "Something went wrong try again";
        }
      } else {
        result = await Review.create(review);
    
        await module.exports.updateLocationRating(review.location_id);
      }
    } catch (err) {
      next(err);
    }
    return result;
  },

  updateLocationRating: async function (location_id) {
     // attributes: [
        //   [Sequelize.fn("AVG", Sequelize.col("location.rating")), "avgRating"],
        // ],
        // group by :locId

        let Rating = await Review.findAndCountAll({
          where:{location_id:location_id},
          attributes: ['location_id', 
            [sequelize.fn('AVG', sequelize.col('rating')),"avgRating"]],
          group: ['location_id']
        })
      
        let locres = await Location.update(
          {
             rating: Rating.rows[0].dataValues.avgRating
          },
          { where: { id: location_id } }
        ); 
      
  }, 
  delete: async function(id, next) {
    let result = null;
    try {
      result = await Review.destroy({ where: { id: id } });
    } catch (err) {
      next(err);
    }
    return result;
  },
  listAll: async function(start, length, searchQry, next) {
    try {
      let whereClause = {};
      Object.keys(searchQry).forEach(el => {
        if(el == 'location'){
          whereClause['location_id'] = searchQry[el];
        }
        else {
          whereClause[el] = { [Op.like]: "%" + searchQry[el] + "%" };
        }
      });
      result = await Review.findAndCountAll({
        where: whereClause,
        include: [
          { model: User },
          { model: Location }
        ],
        offset: start,
        limit: length,
        order: [["createdAt", "DESC"]]
      });
      return result;
    } catch (err) {
      next(err);
    }

    return result;
  },
  getDetail: async function(id, next) {
    let result = null;
    try {
      result = await Review.findOne({ where: { id } });
      return result;
    } catch (err) {
      next(err);
    }
  },

  getCsvData: async function () {
    try {
      let whereClause = {};
      let data = await Review.findAndCountAll({
        where: whereClause,
        include: [{ model: User }, { model: Location }],
        order: [["createdAt", "DESC"]],
        // raw:true
      });
      let finalresult = [];
      for (let i = 0; i < data.rows.length; i++) {
        data1 = {
          "User Name": data.rows[i].user && data.rows[i].user.first_name ? data.rows[i].user.first_name : "",
          "Email": data.rows[i].user && data.rows[i].user.email ? data.rows[i].user.email : "",
          "Location": data.rows[i].location && data.rows[i].location.name ? data.rows[i].location.name : "",
          "Review": data.rows[i].review ? data.rows[i].review : "",
          "Rating": data.rows[i].rating ? data.rows[i].rating : "",
          "Rating for odour hygiene": data.rows[i].review_order_hygiene ? data.rows[i].review_order_hygiene : "",
          "Rating for Clean Pot/ Seat": data.rows[i].review_clean_pot_seat ? data.rows[i].review_clean_pot_seat : "",
          "Rating for Handwash & Water": data.rows[i].review_handwash ? data.rows[i].review_handwash : "",
          "Rating for Dry Seat/ Floor": data.rows[i].review_dryseat ? data.rows[i].review_dryseat : "",
        };
        finalresult.push(data1);
      }
      result = {
        data: finalresult,
      };
      return result;
    } catch (err) {
      return {err: err.message }
    }
  },
};
