const util = require("../../utils/utils");
const Section = require("../../models/Location");
module.exports = {
  addAuth: async function(req, res, next) {
    if (req.currUser.role == 1) {
      next();
    } else {
      res.status(403);
      res.send({ error: "Permission Denied!" });
    }
  },

  editAuth: async function(req, res, next) {
    if (req.currUser.role == 1) {
      next();
    } else {
      res.status(403);
      res.send({ error: "Permission Denied!" });
    }
  },

  deleteAuth: async function(req, res,next) {
    if (req.currUser.role == 1) {
      next();
    } else {
      res.status(403);
      res.send({ error: "Permission Denied!" });
    }
  },
}