module.exports = { addAcsCheck, editAcsCheck, listAcsCheck, deleteAcsCheck, getDetailAcsCheck }

function addAcsCheck(req, res, next) {
  if (req.currUser.role == 1) {
    next();
  } else {
    res.status(403);
    res.send({ error: "Permission Denied!" });
  }
}

function editAcsCheck(req, res, next) {
  if (req.currUser.role == 1) {
    next();
  } else {
    res.status(403);
    res.send({ error: "Permission Denied!" });
  }
}

function getDetailAcsCheck(req, reset_password_expires, next) {
  if (req.currUser.role == 1 ||
      req.currUser.role == 2) {
    next();
  } else {
    res.status(403);
    res.send({ error: "Permission Denied!" });
  }
}

function listAcsCheck(req, res, next) {
  if (req.currUser.role == 1 || req.currUser.role == 2
  ) {
    next();
  } else {
    res.status(403);
    res.send({ error: "Permission Denied!" });
  }
}

function deleteAcsCheck(req, res, next) {
  if (req.currUser.role == 1) {
    next();
  } else {
    res.status(403);
    res.send({ error: "Permission Denied!" });
  }
}