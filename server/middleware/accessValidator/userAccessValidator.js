const util = require("../../utils/utils");

module.exports = {
  editAuth: function(req, res, next) {
    if (req.currUser.role == 1 || req.currUser.role == 2) {
      next();
    } else {
      res.status(403);
      res.send({ error: "Permission Denied!" });
    }
  },

  getAuth: function(req, res, next) {
    if (req.currUser.role == 1 || req.currUser.role == 2) {
      next();
    }else{
      res.status(403);
      res.send({ error: "Permission Denied!" });
    }
  },

  listAuth: function(req, res, next) {
    if (req.currUser && req.currUser.role == 1) {
      next();
    } else {
      req.body.is_active = true
      next();
    }
  },

  ownerListAuth: function(req, res, next) {
    if (req.currUser && req.currUser.role == 1) {
      req.currUser.role == 3
      next();
    } else {
      req.body.is_active = true
      next();
    }
  },

  deleteAuth: function(req, res, next) {
    if (req.currUser.role == 1) {
      next();
    }else{
      res.status(403);
      res.send({ error: "Permission Denied!" });
    }
  }
};
