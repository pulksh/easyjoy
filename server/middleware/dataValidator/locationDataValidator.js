const Yup = require("yup");
module.exports = {
  add: async (req, res, next) => {
    let schema = Yup.object().shape({
      name: Yup.string().required("Name is a required field"),
      rating: Yup.number().required("Rating is a required field"),
      description: Yup.string(),
      latitude: Yup.number().required("Latitude should be number"),
      longitude: Yup.number().required("Longitude should be number"),
    });
    
    schema.validate(req.body, { abortEarly: false })
    .then(
      (response)=>{
       next();
      }
    )
    .catch((err)=> {
        next(err);
    });
  },
  edit: async (req, res, next) => {
    let schema = Yup.object().shape({
      name: Yup.string().required("Name is a required field"),
      rating: Yup.number().required("Rating is a required field"),
      description: Yup.string(),
      latitude: Yup.number().required("Latitude should be number"),
      longitude: Yup.number().required("Longitude should be number"),
    });
    schema.validate(req.body, { abortEarly: false })
    .then(
      (response)=>{
       next();
      }
    )
    .catch((err)=> {
        next(err);
    });
  }
};
