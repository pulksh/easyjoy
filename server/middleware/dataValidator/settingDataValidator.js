const Yup = require("yup");

module.exports = { addCheck , editCheck, listCheck, deleteCheck, getDetailCheck };

async function addCheck(req, res, next) {
  let schema = Yup.object().shape({
    name: Yup.string().required("Name is a required field"),
    value: Yup.string(),
    content_type: Yup.number().typeError("Content Type should be a number").required("Content Type is a required field"),
    is_editable: Yup.boolean()
  });
  
  schema.validate(req.body, { abortEarly: false })
  .then(
    (response)=>{
     next();
    }
  )
  .catch((err)=> {
    return{error: err.message};
  });
}

async function editCheck(req, res, next) {
  let schema = Yup.object().shape({
    id:Yup.number().typeError("Setting Id should be a number").required("Setting Id is a required field"),
    name: Yup.string().required("Name is a required field"),
    value: Yup.string(),
    content_type: Yup.number().typeError("Content Type should be a number").required("Content Type is a required field"),
    is_editable: Yup.boolean()
  });
  
  schema.validate(req.body, { abortEarly: false })
  .then(
    (response)=>{
     next();
    }
  )
  .catch((err)=> {
    return{error: err.message};
  });
}

async function listCheck(req, res, next) {
  let schema = Yup.object().shape({
      start: Yup.number().typeError("'start' parameter should be number").required("'start' parameter in url is missing"),
      length: Yup.number().typeError("'length' parameter should be number").required("'length' parameter in url is missing"),
  });

  let obj = {... req.params }

  schema.validate(obj, { abortEarly: false })
  .then(
    (response)=>{
     next();
    }
  )
  .catch((err)=> {
    return{error: err.message};
  });
}

async function deleteCheck(req, res, next) {
  let schema = Yup.object().shape({
    id: Yup.number().typeError("'id' parameter should be number").required("'id' parameter in url is missing")
  });

  let obj = {... req.params }

  schema.validate(obj, { abortEarly: false })
  .then(
    (response)=>{
    next();
    }
  )
  .catch((err)=> {
    return{error: err.message};
  });
}

async function getDetailCheck(req, res, next) {
  let schema = Yup.object().shape({
    id: Yup.number().typeError("'id' parameter should be number").required("'id' parameter in url is missing")
  });

  let obj = {... req.params }

  schema.validate(obj, { abortEarly: false })
  .then(
    (response)=>{
    next();
    }
  )
  .catch((err)=> {
    return{error: err.message};
  });
}