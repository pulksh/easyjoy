const Yup = require("yup");
module.exports = { addCheck, editCheck, listCheck, deleteCheck, getDetailCheck };

async function addCheck(req, res, next) {
  let schema = Yup.object().shape({
    user_id: Yup.number().required("User id is a required field"),
    location_id: Yup.number().required("Location id is a required field")
  });

  schema.validate(req.body, { abortEarly: false })
    .then(
      (response) => {
        next();
      }
    )
    .catch((err) => {
      next(err);
    });
}

async function editCheck(req, res, next) {
  let schema = Yup.object().shape({
    user_id: Yup.number().required("User id is a required field"),
    location_id: Yup.number().required("Location id is a required field")
  });

  schema.validate(req.body, { abortEarly: false })
    .then(
      (response) => {
        next();
      }
    )
    .catch((err) => {
      next(err);
    });
}

async function listCheck(req, res, next) {
  let schema = Yup.object().shape({
    start: Yup.number().typeError("'start' parameter should be number").required("'start' parameter in url is missing"),
    length: Yup.number().typeError("'length' parameter should be number").required("'length' parameter in url is missing")
  });

  let obj = { ...req.params }

  schema.validate(obj, { abortEarly: false })
    .then(
      (response) => {
        next();
      }
    )
    .catch((err) => {
      next(err);
    });
}

async function deleteCheck(req, res, next) {
  let schema = Yup.object().shape({
    id: Yup.number().typeError("'id' parameter should be number").required("'id' parameter in url is missing")
  });

  let obj = { ...req.params }

  schema.validate(obj, { abortEarly: false })
    .then(
      (response) => {
        next();
      }
    )
    .catch((err) => {
      next(err);
    });q
}

async function getDetailCheck(req, res, next) {
  let schema = Yup.object().shape({
    id: Yup.number().typeError("'id' parameter should be number").required("'id' parameter in url is missing")
  });

  let obj = { ...req.params }

  schema.validate(obj, { abortEarly: false })
    .then(
      (response) => {
        next();
      }
    )
    .catch((err) => {
      next(err);
    });
}