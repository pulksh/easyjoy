const Yup = require("yup");
module.exports = { loginCheck, forgetPassword, resetPassword };

async function loginCheck(req, res, next) {
  let schema = Yup.object().shape({
    email: Yup.string().required("Email is a required field").email(),
    password: Yup.string().required("Password is a required field"),
  });

  schema
    .validate(req.body, { abortEarly: false })
    .then((response) => {
      next();
    })
    .catch((err) => {
      next(err.errors);
    });
}

async function forgetPassword(req, res, next) {
  let schema = Yup.object().shape({
    email: Yup.string().required("Email is a required field").email("Invalid Email"),
  });

  schema
    .validate(req.body, { abortEarly: false })
    .then((response) => {
      next();
    })
    .catch((err) => {
      next(err.errors);
    });
}

async function resetPassword(req, res, next) {
  let schema = Yup.object().shape({
    newPassword: Yup.string().required("Password is a required field"),
    verifyPassword: Yup.string().oneOf([Yup.ref("newPassword"), null], "password must match"),
    token: Yup.string().required("Invalid token"),
  });
  const obj = { ...req.params, ...req.body };
  schema
    .validate(obj, { abortEarly: false })
    .then((response) => {
      next();
    })
    .catch((err) => {
      next(err.errors);
    });
}
