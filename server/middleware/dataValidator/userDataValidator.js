const Yup = require("yup");
module.exports = {
  add: async (req, res, next) => {
     const phoneRegex = RegExp(
         /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/
       );
    let schema = Yup.object().shape({
        title: Yup.string(),
        first_name: Yup.string().required("First Name is a required field")
        .min(2, "Too Short").max(100, "Too Long"),
        // last_name: Yup.string().max(100, "Too Long"),
        email: Yup.string().email("Invalid Email"),
        password:Yup.string(),
        phone: Yup.string().matches(phoneRegex, "Invalid phone number").required("Phone is a required field"),
        role: Yup.number()
    });
    
    schema.validate(req.body, { abortEarly: false })
    .then(
      (response)=>{
       next();
      }
    )
    .catch((err)=> {
        next(err);
    });
  },
  edit: async (req, res, next) => {
     const phoneRegex = RegExp(
         /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/
       );
    let schema = Yup.object().shape({
        id:Yup.number().typeError("User Id should be a number").required("User Id is a required field"),
        title: Yup.string(),
        first_name: Yup.string().max(100, "Too Long"),
        email: Yup.string().required("Email is a required field").email("Invalid Email"),
        phone: Yup.string().matches(phoneRegex, "Invalid phone number"),
        role: Yup.number()
    });
    
    schema.validate(req.body, { abortEarly: false })
    .then(
      (response)=>{
       next();
      }
    )
    .catch((err)=> {
        next(err);
    });
  }
};