const express = require("express");
const http = require("http");
//const https = require("https");
const bodyParser = require("body-parser");

require("dotenv").config();
const cors = require("cors");
var app = express();
app.use(cors());
global.sequelize = require("./db/dbconnection");

app.use(bodyParser.json({ limit: "50mb", extended: true }));

app.use("/uploads", express.static("uploads"));

const user = require("./routes/UserRoute");
const setting = require("./routes/SettingRoute");
const location = require("./routes/LocationRoute");
const review = require("./routes/ReviewRoute");
const checkinout = require("./routes/checkInOutRouter");
const verifycode = require("./routes/verifyCodeRouter");
const sociallogin = require("./routes/socialLoginRoutes");
const Page = require("./routes/PageRoutes");
const Notification = require("./routes/NotificationRoute");
const ActivityLog = require("./routes/ActivityLogRoute");

const port = process.env.PORT;

let server = http.createServer(app);

app.use("/user", user);
app.use("/location", location);
app.use("/setting", setting);
app.use("/review", review);
app.use("/checkinout", checkinout);
app.use("/verifycode", verifycode);
app.use("/sociallogin", sociallogin)
app.use('/staticpage', Page);
app.use('/notification', Notification);
app.use('/activitylog', ActivityLog);

app.use(function (err, req, res, next) {
  // next is required parameter with err parameter
  if (err.errors) {
    res.status(500).json({ error: err.message, errors: err.errors });
  } else {
    res.status(500).json({ error: err.message });
  }
});

app.use(function (err, req, res, next) {
  // next is required parameter with err parameter
  if (err.errors) {
    res.status(401).json({ error: err.message, errors: err.errors });
  } else {
    res.status(401).json({ error: err.message });
  }
});

server.listen(port, () => {
  console.log(`Server is starting at ${port}`);
});
