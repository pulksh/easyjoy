import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import { Formik, ErrorMessage } from "formik";
import { Link, Redirect } from "react-router-dom";
import * as Yup from "yup";
import UserService from "../../services/UserService";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
export default class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.token = props.match.params.token;
    this.state = {
      password: "",
      confirm_password: "",
    };
    this.userv = new UserService();
    this.schema = Yup.object({
      password: Yup.string().required("Password is a required field"),
      confirm_password: Yup.string()
        .oneOf([Yup.ref("password"), null], "Password must match")
        .required("Confirm Password is a required field"),
    });
  }

  async submitUserForm(values, actions) {
    this.userv
      .resetPassword(values.password, values.confirm_password, this.token)
      .then((result) => {
        if(result.error){
          toast.error(result.error);
        } else {
          toast.success(result.message);
          setTimeout(()=>{
            this.setState({ loading: false, redirect: true });
          }, 3000)
        }
      })
      .catch((err) => {
        toast.error(err.error);
        this.setState({ loading: false, redirect: false });
      });
  }

  render() {
       if (this.state.redirect === true) {
          return <Redirect to="/" />;
       }
    return (
      <>
        <Formik
          validationSchema={this.schema}
          initialValues={this.state}
          enableReinitialize={true}
          onSubmit={this.submitUserForm.bind(this)}
          render={({
            values,
            errors,
            status,
            touched,
            handleBlur,
            handleChange,
            handleSubmit,
            isSubmitting,
            setFieldValue,
          }) => (
            <div className="main-wrapper">
               <Form onSubmit={handleSubmit}>
                <div className="page-content change-password-col">
                  <div className="list no-hairlines custom-form">
                    <div className="back-btn">
                      <Link to={"index.html"} className="back link">
                        <img src="assets/img/left-arrow-big-black.svg" alt="" />
                      </Link>
                    </div>
                    <div className="register-icon">
                      <img src="assets/img/register-top-img.png" alt="" />
                    </div>
                    <div className="logo">
                      <Link to={"index.html"}>
                        <img src="assets/img/logo.svg" alt="" />
                      </Link>
                    </div>
                    <ul className="change-list">
                      <li className="item-content item-input">
                        <div className="item-col">
                          <div className="item-input-wrap">
                            <input
                              type="password"
                              placeholder="Enter password"
                              value={values.password}
                              name="password"
                              onChange={handleChange}
                            />
                            <div className="item-input-icon">
                              <img src="assets/img/lock-icon.svg" alt="" />
                            </div>
                          </div>
                          <ErrorMessage name="password">
                            {(msg) => <div className="err_below">{msg}</div>}
                          </ErrorMessage>
                        </div>
                      </li>
                      <li className="item-content item-input">
                        <div className="item-col">
                          <div className="item-input-wrap">
                            <input
                              type="password"
                              placeholder="Enter confirm password"
                              value={values.confirm_password}
                              name="confirm_password"
                              onChange={handleChange}
                            />
                            <div className="item-input-icon">
                              <img src="assets/img/lock-icon.svg" alt="" />
                            </div>
                          </div>
                          <ErrorMessage name="confirm_password">
                            {(msg) => <div className="err_below">{msg}</div>}
                          </ErrorMessage>
                        </div>
                      </li>
                      <li className="item-content item-input">
                        <div className="item-col">
                          <div className="item-input-wrap submit-btn-col">
                            <button type="submit" className="btn btn-submit">
                              Change password
                            </button>
                          </div>
                        </div>
                      </li>
                    </ul>
                    <span className="login-back">
                      Go back to login ?{" "}
                      <Link to={{ pathname: "/" }}>Login Now!</Link>
                    </span>
                  </div>
                </div>
              </Form>
              <ToastContainer />
            </div>
          )}
        />
      </>
    );
  }
}
