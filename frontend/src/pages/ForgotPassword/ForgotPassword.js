import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import { Formik, ErrorMessage } from "formik";
import { Link, Redirect } from "react-router-dom";
import * as Yup from "yup";
import UserService from "../../services/UserService";
import { ToastContainer, toast } from "react-toastify";
export default class ForgotPassword extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
    };
    this.userServ = new UserService();
    this.schema = Yup.object({
      email: Yup.string()
        .required("Email is a required field")
        .email("Invalid email"),
    });
  }

  async submitEmail(values, actions) {
    try {
      let response = await this.userServ.forget_password({
        email: values.email,
      });

      if(response.error){
        toast.error(response.error)
      } else {
        toast.success(response.message);
        setTimeout(()=>{
          this.setState({ loading: false, redirect: true });
        }, 3000)
        // this.props.history.push("/login");
      }
      
    } catch (err) {
      console.log(err.message);
    }
  }

  render() {
    if (this.state.redirect === true) {
      return <Redirect to="/" />;
    }
    return (
      <>
        <Formik
          validationSchema={this.schema}
          initialValues={this.state}
          enableReinitialize={true}
          onSubmit={this.submitEmail.bind(this)}
          render={({
            values,
            errors,
            status,
            touched,
            handleBlur,
            handleChange,
            handleSubmit,
            isSubmitting,
            setFieldValue,
          }) => (
            <div className="main-wrapper">
              <Form onSubmit={handleSubmit}>
                <div className="page-content change-password-col">
                  <div className="list no-hairlines custom-form">
                    <div className="back-btn">
                      <Link to={"/"} className="back link">
                        <img
                          src="/assets/img/left-arrow-big-black.svg"
                          alt=""
                        />
                      </Link>
                    </div>
                    {/* <div className="register-icon">
                      <img src="/assets/img/register-top-img.png" alt="" />
                    </div> */}
                    <div className="logo" style={{paddingTop:'130px'}}>
                      {/* <Link to={"index.html"}>
                        <img src="/assets/img/logo.svg" alt="" />
                      </Link> */}
                       <h1 style={{fontWeight:'900'}}>Forgot Password</h1>
                                              
                    </div>
                    <ul className="change-list">
                      <li className="item-content item-input">
                        <div className="item-col">
                          <div className="item-input-wrap">
                            {/* <input type="text" placeholder="Email" /> */}
                            <input
                              type="text"
                              placeholder="Enter email"
                              value={values.email}
                              name="email"
                              onChange={handleChange}
                              onBlur={handleBlur}
                              isValid={touched.email && !errors.email}
                            />
                            <div className="item-input-icon">
                              <img src="/assets/img/email.svg" alt="" />
                            </div>
                          </div>
                          <ErrorMessage name="email">
                            {(msg) => <div className="err_below">{msg}</div>}
                          </ErrorMessage>
                        </div>
                      </li>
                      <li className="item-content item-input">
                        <div className="item-col">
                          <div className="item-input-wrap submit-btn-col">
                            <button type="submit" className="btn btn-submit">
                              Reset password
                            </button>
                          </div>
                        </div>
                      </li>
                    </ul>
                    <span className="login-back">
                      Go back to login ?{" "}
                      <Link to={{ pathname: "/" }}>Login Now!</Link>
                    </span>
                  </div>
                </div>
              </Form>
              <ToastContainer />
            </div>
          )}
        />
      </>
    );
  }
}
