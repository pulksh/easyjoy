import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import { Formik, ErrorMessage } from "formik";
import { Link, Redirect } from "react-router-dom";
import * as Yup from "yup";
import UserService from "../../services/UserService";
import { ToastContainer, toast } from "react-toastify";
import SettingService from "../../services/SettingService";
export default class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      redirect: false,
      errorMsg: "",
      settingList: [],
    };
    this.search = { start: 0, perPage: 10, searchTxt: "", searchField: "" };

    this.state.user = {
      title: "Mr",
      first_name: "",
      last_name: "",
      email: "",
      phone: "",
      role: "",
      password: "",
      confirm_password: "",
    };
    const phoneNumberRegex = RegExp(
      /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/
    );

    this.userServ = new UserService();
    this.settingServ = new SettingService();

    this.schema = Yup.object({
      title: Yup.string(),
      first_name: Yup.string()
        .required("First Name is a required field")
        .min(2, "Too Short!")
        .max(100, "Too Long!"),
      email: Yup.string()
        .required("Email is a required field")
        .email("Invalid Email"),
      phone: Yup.string()
        .matches(phoneNumberRegex, "Invalid phone number")
        .required("Phone Number is a required field"),
      password: Yup.string().required("Password is a required field"),
      confirm_password: Yup.string()
        .oneOf([Yup.ref("password"), null], "Password must match")
        .required("Confirm Password is a required field"),
    });
  }

  componentDidMount() {
    this.getSettingList();
  }

  getSettingList() {
    let setting = {};

    this.settingServ.listService(setting, 0, 100).then(
      (response) => {
        this.setState({
          settingList: response.rows,
          totalCount: response.count,
        });
      },
      (error) => {
        this.setState({ settingList: [], totalcount: 0 });
      }
    );
  }

  async submitUserForm(values, actions) {
    //actions.setSubmitting(false);
    if (values.role === null || values.role === "") {
      values.role = 2;
    }
    delete values.confirm_password;
    let tx = values;
    this.setState({
      user: tx,
    });

    try {
      let response = await this.userServ.addService(this.state?.user);
      
      if (response.message) {
        // setTimeout(() => {
        //   this.setState({ redirect: true });
        // }, 2000);
        // this.setState({ errorMsg: "" });
        toast.success(response.message);
        try {
          let userResp = await this.userServ.login(
            values.email, values.password
          );
          if (userResp && userResp.id) {
            toast.success(userResp.message);
            this.setState({ redirect: true });
          } else {
            toast.error(userResp.error);
          }
        } catch (err) {
          toast.error(err.message);
          this.setState({ redirect: false, error: err.message });
        }
      } else {
        toast.error(response.error);
      }
    } catch (err) {
      toast.error(err.message);
      this.setState({ redirect: false, error: err.message });
    }
  }

  render() {
    if (this.state.redirect === true) {
      return <Redirect to="/home" />;
    }
    return (
      <>
        <Formik
          validationSchema={this.schema}
          initialValues={this.state.user}
          enableReinitialize={true}
          onSubmit={this.submitUserForm.bind(this)}
          render={({
            values,
            errors,
            status,
            touched,
            handleBlur,
            handleChange,
            handleSubmit,
            isSubmitting,
            setFieldValue,
          }) => (
            <div className="main-wrapper">
              <Form onSubmit={handleSubmit}>
                <div className="page-content change-password-col register-col">
                  <div className="list no-hairlines custom-form">
                    <div className="back-btn">
                      <Link to={"/"} className="back link">
                        <img
                          src="/assets/img/left-arrow-big-black.svg"
                          alt=""
                        />
                      </Link>
                    </div>
                    {/* <div className="register-icon">
                      <img src="/assets/img/register-top-img.png" alt="" />
                    </div> */}
                    <div className="logo" style={{ paddingTop: "75px" }}>
                      {/* <Link to={"index.html"}>
                        <img src="/assets/img/logo.svg" alt="doccure" />
                      </Link> */}
                      <h1 style={{ fontWeight: "900" }}>Register</h1>
                      <div class="logo">
                        {this.state?.settingList.map((sett) => {
                          return sett.name === "logo" ? (
                            <Link to="">
                              <img src={sett.value} alt="doccure" />
                            </Link>
                          ) : null;
                        })}
                      </div>
                      <br />
                    </div>
                    <div className="register-inner-col">
                      <div className="top-title">
                        <div>{/* <h3>Doctor Register</h3> */}</div>
                        <div>
                          {/* <Link to={"register.html"}>Not a Doctor?</Link> */}
                        </div>
                      </div>
                      <ul className="change-list">
                        <li className="item-content item-input">
                          <div className="item-col">
                            <div className="item-input-wrap">
                              <input
                                type="text"
                                placeholder="Enter Name"
                                value={values.first_name}
                                name="first_name"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                isValid={
                                  touched.first_name && !errors.first_name
                                }
                              />
                              <div className="item-input-icon">
                                <img src="/assets/img/user-icon.svg" alt="" />
                              </div>
                            </div>
                            <ErrorMessage name="first_name">
                              {(msg) => <div className="err_below">{msg}</div>}
                            </ErrorMessage>
                          </div>
                        </li>
                        <li className="item-content item-input">
                          <div className="item-col">
                            <div className="item-input-wrap">
                              <input
                                type="text"
                                placeholder="Enter email"
                                value={values.email}
                                name="email"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                isValid={touched.email && !errors.email}
                              />
                              <div className="item-input-icon">
                                <img src="/assets/img/email.svg" alt="" />
                              </div>
                            </div>
                            <ErrorMessage name="email">
                              {(msg) => <div className="err_below">{msg}</div>}
                            </ErrorMessage>
                            {this.state.errorMsg ===
                            `Email already registerd` ? (
                              <div className="errormsg">
                                {this.state.errorMsg}
                              </div>
                            ) : (
                              " "
                            )}
                          </div>
                        </li>
                        <li className="item-content item-input">
                          <div className="item-col">
                            <div className="item-input-wrap">
                              <input
                                type="text"
                                placeholder="Enter Phone number"
                                value={values.phone}
                                name="phone"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                isValid={touched.phone && !errors.phone}
                              />
                              <div className="item-input-icon">
                                <img src="/assets/img/smartphone.svg" alt="" />
                              </div>
                            </div>
                            <ErrorMessage name="phone">
                              {(msg) => <div className="err_below">{msg}</div>}
                            </ErrorMessage>
                          </div>
                        </li>
                        <li className="item-content item-input">
                          <div className="item-col">
                            <div className="item-input-wrap">
                              <input
                                type="password"
                                placeholder="Enter password"
                                value={values.password}
                                name="password"
                                onChange={handleChange}
                              />
                              <div className="item-input-icon">
                                <img src="/assets/img/lock-icon.svg" alt="" />
                              </div>
                            </div>
                            <ErrorMessage name="password">
                              {(msg) => <div className="err_below">{msg}</div>}
                            </ErrorMessage>
                          </div>
                        </li>
                        <li className="item-content item-input">
                          <div className="item-col">
                            <div className="item-input-wrap">
                              <input
                                type="password"
                                placeholder="Enter confirm password"
                                value={values.confirm_password}
                                name="confirm_password"
                                onChange={handleChange}
                              />
                              <div className="item-input-icon">
                                <img src="/assets/img/lock-icon.svg" alt="" />
                              </div>
                            </div>
                            <ErrorMessage name="confirm_password">
                              {(msg) => <div className="err_below">{msg}</div>}
                            </ErrorMessage>
                          </div>
                        </li>
                        <li className="item-content item-input">
                          <div className="item-input-wrap submit-btn-col">
                            <button type="submit" className="btn btn-submit">
                              Register
                            </button>
                            <p className="pt-4 text-center">
                              By clicking "Register"; I agree to EASYJOY
                              <Link
                                // to="/terms-condition"
                                to="/pg/terms_of_use"
                              >
                                {" "}
                                Terms of Use{" "}
                              </Link>
                            </p>
                          </div>
                        </li>
                      </ul>
                      <span className="login-back">
                        Already have an account ?{" "}
                        <Link to={{ pathname: "/" }}>Login Now!</Link>
                      </span>
                    </div>
                  </div>
                </div>
              </Form>
              <ToastContainer />
            </div>
          )}
        />
      </>
    );
  }
}
