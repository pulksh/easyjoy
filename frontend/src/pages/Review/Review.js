import React, { Component } from "react";
import HeadeMenus from "../../shared/HeaderMenus/HeaderMenus";
import ReviewService from "../../services/ReviewService";
import LocationService from "../../services/LocationService";
import CardComponent from "../../shared/CardComponent/CardComponent";
import ReviewCard from "../../shared/CardComponent/ReviewCard";
import { Helmet } from 'react-helmet';

export default class Review extends Component {
  constructor(props) {
    super(props);
    this.lid = this.props.match.params.id;
    this.state = {
      reviewList: [],
      location: null,
      show: false,
      location_id: this.lid,
      rating: 0,
      review: ""
    };
    this.reviewServ = new ReviewService();
    this.locationServ = new LocationService();
  }
  componentDidMount() {
    this.getReviewDetails();
  }

  async getReviewDetails() {
    try {
      let filter = {
        location_id: this.lid,
      };
      let responseLoc = await this.locationServ.getLocById(this.lid);
      let responseRev = await this.reviewServ.listReview(0, 1000,filter);
      this.setState({ 
          location: responseLoc,
          reviewList: responseRev.rows ? responseRev.rows : [] }
      );  
    } catch (err) {
      console.log(err);
    }
  }
  
  render() {
    console.log(this.state.responseLoc,'Location list',parseInt(this.lid),'ids')
    return (
      <>
        <Helmet>
          <title>{this.state.location?.meta_title}</title>
          <meta name="keywords" content={this.state.location?.meta_keyword} />
          <meta name="description" content={this.state.location?.meta_desc} />
        </Helmet>
        <div className="main-wrapper">
        <div className="side-menu" id="sidebar-menu"></div>
        <HeadeMenus title="Location Detail" />
          <div className="page-content pt-0">
            {/* <!-- Doctor Profile --> */}
            <div className="doctor-profile-tab">
              <div className="container">
                <div className="tab-pane active" id="OverView">
                  <div className="panel panel-default">
                    <div
                      id="collapseOne"
                      className="panel-collapse collapse in"
                    >
                      <div className="panel-body">
                        <CardComponent locationData={this.state.location} showFullDesc={true} getReviewDetails={this.getReviewDetails.bind(this)}/>
                        <br />
                        <br/>
                        {this.state?.reviewList?.map((dataTitle) => {
                          return (
                            <ReviewCard
                              name={dataTitle?.user?.first_name}
                              rating={dataTitle?.rating}
                              review={dataTitle?.review}
                            />
                          );
                        })}
                      </div>
                    </div>
                  </div>
                </div>
                {/* </div> */}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
