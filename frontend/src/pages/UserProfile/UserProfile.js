import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import { Formik, ErrorMessage } from "formik";
import { Link, Redirect } from "react-router-dom";
import * as Yup from "yup";
import UserService from "../../services/UserService";
import HeadeMenus from "../../shared/HeaderMenus/HeaderMenus";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
export default class UserProfile extends Component {
  constructor(props) {
    super(props);
    this.user_id = this.props.match.params.id;
    this.state = {
      user: null,
      redirect: false,
      errorMsg: "",
    };
    this.state.user = {
      title: "Mr",
      first_name: "",
      last_name: "",
      email: "",
      phone: "",
      role: "",
      password: "",
    };

    this.userServ = new UserService();

    if (this.user_id) {
      this.userServ.getUser(this.user_id).then(
        (response) => {
          this.setState({ user: response });
        },
        (error) => {
          alert("Opps! Something went wrong not able to fetch User details.");
        }
      );
    }
    const phoneNumberRegex = RegExp(
      /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/
    );

    this.schema = Yup.object({
      // title: Yup.string(),
      first_name: Yup.string()
        .required("User name is a required field")
        .max(100, "Too Long"),
      email: Yup.string()
        .required("Email is a required field")
        .email("Invalid Email"),
      phone: Yup.string()
        .matches(phoneNumberRegex, "Invalid phone number")
        .required("Phone Number is a required field"),
    });
  }

  async submitUserForm(values, actions) {
    actions.setSubmitting(false);
    if (values.role === null || values.role === "") {
      values.role = 2;
    }

    let tx = values;
    this.setState({
      user: tx,
    });

    if (this.user_id) {
      this.userServ.editService(this.state?.user).then(
        (response) => {
          if (response.message) {
            setTimeout(() => {
              this.setState({ redirect: true });
            }, 3000);
            this.setState({ errorMsg: "" });
            toast.success(response.message);
          } else {
            toast.error(response.error);
          }
        },
        (err) => {
          this.setState({ redirect: false });
          toast.error(err.error);
        }
      );
    }
  }

  render() {
    if (this.state.redirect === true) {
      return <Redirect to="/" />;
    }
    return (
      <>
        <Formik
          validationSchema={this.schema}
          initialValues={this.state.user}
          enableReinitialize={true}
          onSubmit={this.submitUserForm.bind(this)}
          render={({
            values,
            errors,
            status,
            touched,
            handleBlur,
            handleChange,
            handleSubmit,
            isSubmitting,
            setFieldValue,
          }) => (
            <div className="main-wrapper">
              <HeadeMenus title="User profile" />
              <div className="page-content profile-settings">
                <Form onSubmit={handleSubmit}>
                  <div className="container">
                    <div className="tab-content">
                      <div className="tab-pane active" id="basic-info">
                        <div className="panel panel-default">
                          <div
                            id="collapseOne"
                            className="panel-collapse collapse in"
                          >
                            <div className="panel-body">
                              <div className="setting-widget">
                                <div className="list no-hairlines-md">
                                  <div className="widget-title">
                                    <h3>User Information</h3>
                                  </div>
                                  <hr />
                                  {/* <div className="file-upload">
                                    <Link
                                      //   to={"/user-profile"}
                                      className="file-upload-img"
                                    >
                                      <img
                                        src="/assets/img/doctors/doctor-thumb-02.jpg"
                                        className="img-fluid img-circle"
                                        width="85"
                                        alt="User Image"
                                      />
                                      <span className="cam-icon">
                                        <img
                                          src="/assets/img/placeholder-small.svg"
                                          alt=""
                                        />
                                      </span>
                                    </Link>
                                  </div>
                                   */}
                                  {/* <form> */}
                                  <ul>
                                    <li className="item-content item-input">
                                      <div className="item-col">
                                        <div className="item-title item-label">
                                          Title <span>*</span>
                                        </div>
                                        <div className="item-input-wrap input-dropdown-wrap">
                                          <select>
                                            <option value="Mr">Mr</option>
                                            <option value="Dr">Dr</option>
                                            <option value="Mrs">Mrs</option>
                                            <option value="Miss">Miss</option>
                                            <option value="Ms">Ms</option>
                                          </select>
                                        </div>
                                      </div>
                                    </li>
                                    <li className="item-content item-input">
                                      <div className="item-col">
                                        <div className="item-title item-label">
                                          E-mail <span>*</span>
                                        </div>
                                        <div className="item-input-wrap">
                                          {/* <input type="email" /> */}
                                          <input
                                            type="text"
                                            placeholder="Enter email"
                                            value={values.email}
                                            name="email"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            isValid={
                                              touched.email && !errors.email
                                            }
                                          />
                                          <span className="input-clear-button"></span>
                                          <ErrorMessage name="email">
                                            {(msg) => (
                                              <div className="err_below">
                                                {msg}
                                              </div>
                                            )}
                                          </ErrorMessage>
                                        </div>
                                      </div>
                                    </li>
                                    <li className="item-content item-input">
                                      <div className="item-col">
                                        <div className="item-title item-label">
                                          User Name <span>*</span>
                                        </div>
                                        <div className="item-input-wrap">
                                          {/* <input type="text" /> */}
                                          <input
                                            type="text"
                                            placeholder="Enter first_name"
                                            value={values.first_name}
                                            name="first_name"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            isValid={
                                              touched.first_name &&
                                              !errors.first_name
                                            }
                                          />
                                          <span className="input-clear-button"></span>
                                          <ErrorMessage name="first_name">
                                            {(msg) => (
                                              <div className="err_below">
                                                {msg}
                                              </div>
                                            )}
                                          </ErrorMessage>
                                        </div>
                                      </div>
                                    </li>
                                    <li className="item-content item-input">
                                      <div className="item-col">
                                        <div className="item-title item-label">
                                          Phone Number <span>*</span>
                                        </div>
                                        <div className="item-input-wrap">
                                          {/* <input type="tel" /> */}
                                          <Form.Control
                                            type="text"
                                            value={values.phone}
                                            name="phone"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            isValid={
                                              touched.phone && !errors.phone
                                            }
                                          />
                                          <ErrorMessage name="phone">
                                            {(msg) => (
                                              <div className="err_below">
                                                {msg}
                                              </div>
                                            )}
                                          </ErrorMessage>
                                          {this.state.errorMsg ===
                                          `Mobile Number already registerd` ? (
                                            <div className="errormsg">
                                              {this.state.errorMsg}
                                            </div>
                                          ) : (
                                            " "
                                          )}
                                        </div>
                                      </div>
                                    </li>
                                    <li className="item-content item-input">
                                      <div className="item-col">
                                        <div className="item-title item-label">
                                          Password <span>*</span>
                                        </div>
                                        <div className="item-input-wrap">
                                          {/* <input type="password" /> */}
                                          <span className="input-clear-button"></span>
                                          <Form.Control
                                            type="password"
                                            name="password"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            isValid={
                                              touched.password &&
                                              !errors.password
                                            }
                                          />
                                          <ErrorMessage name="password">
                                            {(msg) => (
                                              <div className="err_below">
                                                {msg}
                                              </div>
                                            )}
                                          </ErrorMessage>
                                          {`"password" is not allowed to be empty` ===
                                          this.state.errorMsg ? (
                                            <div className="errormsg">
                                              {"Password is a required field"}
                                            </div>
                                          ) : (
                                            " "
                                          )}
                                        </div>
                                      </div>
                                    </li>
                                    <li className="bottom-button">
                                      <button className="btn" type="submit">
                                        Submit
                                      </button>
                                    </li>
                                  </ul>
                                  {/* </form> */}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </Form>
                <ToastContainer />
              </div>
            </div>
          )}
        />
      </>
    );
  }
}
