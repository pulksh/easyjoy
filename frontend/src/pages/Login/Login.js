import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import { Formik, ErrorMessage } from "formik";
import { Link, Redirect } from "react-router-dom";
import * as Yup from "yup";
import UserService from "../../services/UserService";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import GoogleLogin from "react-google-login";
// import FacebookLogin from "react-facebook-login";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import SocialLoginService from "../../services/socialLoginService";
import SettingService from "../../services/SettingService";

// import Facebook from '../../components/facebook';
// import Google from "../../components/google";
export default class Login extends Component {
  constructor(props) {
    super(props);
    this.childRef = React.createRef();
    this.state = {
      redirect: false,
      isFacebook: false,
      isGoogle: false,
      settingList: [],
    };
    this.state.login = {
      email: "",
      password: "",
    };
    this.userServ = new UserService();
    this.settingServ = new SettingService();

    this.socialLoginServ = new SocialLoginService();
    this.schema = Yup.object({
      email: Yup.string()
        .required("Email is a required field")
        .email("Invalid email"),
      password: Yup.string().required("Password is a required field"),
    });
  }

  componentDidMount() {
    this.getSettingList();
  }

  getSettingList() {
    let setting = {};

    this.settingServ.listService(setting, 0, 100).then(
      (response) => {
        this.setState({
          settingList: response.rows,
          totalCount: response.count,
        });
      },
      (error) => {
        this.setState({ settingList: [], totalcount: 0 });
      }
    );
    if (window.user?.token) {
      this.setState({ redirect: true });
    }
  }

  async submitLogin(values, actions) {
    try {
      let response = await this.userServ.login(values.email, values.password);
      if (response && response.id) {
        toast.success(response.message);
        this.setState({ redirect: true });
      } else {
        toast.error(response.error);
      }
    } catch (err) {
      toast.error(err.message);
      this.setState({ redirect: false, error: err.message });
    }
  }

  // Google login
  responseGoogle = (response) => {
    let obj = {
      title: "Mr",
      first_name: response.profileObj.name,
      email: response.profileObj.email,
      phone: "",
      role: 2,
      password: "",
      type: "google",
      token: response.profileObj.googleId,
    };
    this.getLogin(obj);
  };

  // Facebook login
  responseFacebook = (response) => {
    let obj = {
      title: "Mr",
      first_name: response.name,
      email: response.email,
      phone: "",
      role: 2,
      password: "",
      type: "facebook",
      token: response.userID,
    };
    this.getLogin(obj);
  };
  componentClicked = () => {};

  getLogin = (obj) => {
    this.socialLoginServ
      .sociallogin(obj)
      .then((response) => {
        if (response.error) {
          console.log(response.error, "error");
        } else {
          navigator.geolocation.getCurrentPosition(
            () => {
              this.setState({ redirect: true });
            },
            () => {
              this.setState({ redirect: true });
            }
          );
        }
      })
      .catch((err) => {
        this.setState({ redirect: false });
      });
  };

  render() {
    if (this.state.redirect === true) {
      return <Redirect to="/" />;
    }
    return (
      <>
        <Formik
          validationSchema={this.schema}
          initialValues={this.state.login}
          enableReinitialize={true}
          onSubmit={this.submitLogin.bind(this)}
          render={({
            values,
            errors,
            status,
            touched,
            handleBlur,
            handleChange,
            handleSubmit,
            isSubmitting,
            setFieldValue,
          }) => (
            <div className="main-wrapper">
              <Form onSubmit={handleSubmit}>
                <div className="page-content change-password-col register-col">
                  <div className="list no-hairlines custom-form">
                    <div className="back-btn">
                      <Link to={"/"} className="back link">
                        <img
                          src="/assets/img/left-arrow-big-black.svg"
                          alt=""
                        />
                      </Link>
                    </div>

                    {/* <div>
                      <Link to={"/"} className="back link">
                        <img
                          src="/assets/img/left-arrow-big-black.svg"
                          alt=""
                        />
                      </Link>
                    </div> */}

                    <div className="logo" style={{ paddingTop: "50px" }}>
                      {/* <img src="/assets/img/logo.svg" alt="doccure" /> */}
                      <h4>Welcome to EasyJoy!</h4>
                      <h4>Now, your search for a clean restroom is over.</h4>
                      <h4>
                        Log in to find restroom options near you and use one to
                        your delight.
                      </h4>
                      <h1 style={{ fontWeight: "900" }}>Login</h1>
                      <div class="logo">
                        {this.state?.settingList.map((sett) => {
                          return sett.name === "logo" ? (
                            <Link to="">
                              <img src={sett.value} alt="doccure" />
                            </Link>
                          ) : null;
                        })}
                      </div>
                      <br />
                    </div>
                    <div className="register-inner-col">
                      {/* <div className="top-title">
                        <div>
                          <h3>Login</h3>
                        </div>
                      </div> */}
                      <ul className="change-list">
                        <li className="item-content item-input">
                          <div className="item-col">
                            <div className="item-input-wrap">
                              {/* <input type="text" placeholder="Email or username" /> */}
                              <input
                                type="text"
                                placeholder="Enter email"
                                value={values.email}
                                name="email"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                isValid={touched.email && !errors.email}
                              />
                              <div className="item-input-icon">
                                <img src="/assets/img/email.svg" alt="" />
                              </div>
                            </div>
                            <ErrorMessage name="email">
                              {(msg) => <div className="err_below">{msg}</div>}
                            </ErrorMessage>
                          </div>
                        </li>
                        <li className="item-content item-input">
                          <div className="item-col">
                            <div className="item-input-wrap">
                              {/* <input type="password" placeholder="Password" /> */}
                              <input
                                type="password"
                                placeholder="Password"
                                value={values.password}
                                name="password"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                isValid={touched.password && !errors.password}
                              />
                              <div className="item-input-icon">
                                <img src="/assets/img/lock-icon.svg" alt="" />
                              </div>
                            </div>
                            <ErrorMessage name="password">
                              {(msg) => <div className="err_below">{msg}</div>}
                            </ErrorMessage>
                          </div>
                        </li>
                        <li className="col-50 save-password"></li>
                        <li className="col-50 forgot-password">
                          <Link to={{ pathname: "/forgot-password" }}>
                            Forgot Password ?
                          </Link>
                        </li>
                        <li className="item-content item-input">
                          <div className="item-input-wrap submit-btn-col">
                            <button type="submit" className="btn btn-submit">
                              Login Now
                            </button>
                          </div>
                        </li>
                      </ul>
                      <span className="login-back">
                        Don't have an login ?{" "}
                        <Link to={{ pathname: "/registration" }}>
                          Signup Now!
                        </Link>
                      </span>
                      <div className="bottom-social-btn">
                        <ul>
                          <li>
                            <FacebookLogin
                              class="fb-login-button"
                              // appId="819975142021188"
                              appId="465723617945866"
                              onClick={this.componentClicked}
                              callback={this.responseFacebook}
                              isMobile={false}
                              render={(renderProps) => (
                                // <button onClick={renderProps.onClick}>This is my custom FB button</button>
                                <Link
                                  onClick={renderProps.onClick}
                                  className="facebook"
                                >
                                  <span>
                                    <img
                                      src="/assets/img/facebook-letter.svg"
                                      alt="facebook"
                                    />
                                  </span>
                                  Facebook
                                </Link>
                              )}
                            />
                          </li>
                          <li>
                            <GoogleLogin
                              clientId="94523924094-trgqaio2uqj8vuo77p7diiok8o55ouok.apps.googleusercontent.com"
                              render={(renderProps) => (
                                <Link
                                  onClick={renderProps.onClick}
                                  className="google"
                                >
                                  <span>
                                    <img
                                      src="/assets/img/google-plus-letter.svg"
                                      alt="googleplus"
                                    />
                                  </span>
                                  Google
                                </Link>
                              )}
                              onSuccess={this.responseGoogle}
                              onFailure={this.responseGoogle}
                              cookiePolicy={"single_host_origin"}
                            />
                          </li>
                        </ul>
                      </div>
                      <div className="bottom-social-btn">
                        <p className="pt-4 text-center">
                          By clicking on Login / Facebook / Google; I agree to
                          EASYJOY
                          <Link to="/pg/terms_of_use"> Terms of Use </Link>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </Form>
              <ToastContainer />
            </div>
          )}
        />
      </>
    );
  }
}
