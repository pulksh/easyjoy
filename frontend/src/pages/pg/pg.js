import React from 'react';
import PgService from "../../services/pgService";
import { Helmet } from 'react-helmet';
import HeadeMenus from '../../shared/HeaderMenus/HeaderMenus';

export default  class Pg extends React.Component{

    constructor(props) {
        super(props);  
        this.state = {page:{name:"",title:"",content:""}};

        this.PgServ = new PgService();
    }
    componentDidMount() {
        this.getPage(this.props.match.params.id);
    }
    componentWillReceiveProps(nextProps) {
        this.getPage(nextProps.match.params.id);
    }

    getPage(name){
        window.scrollTo(0, 0);
        this.PgServ.getPage(name)
            .then(
                (response)=>{
                    this.setState({page: response.data})
                },
                (error)=>{
                    this.setState({page: {name:"",title:"",content:""}});
                }
            );
    }
    render() {
        let page = this.state.page;
        return (
        <>
        <Helmet>
          <title>{page.meta_title}</title>
          <meta name="keywords" content = {page.meta_keyword} />
          <meta name = "description" content = {page.meta_desc} />
        </Helmet> 
        <HeadeMenus />
        <div className="about-area default-padding">
            <div className="container pl-1 pr-1 mt-4">
                <div className="row">
                    <div className="col-md-12 text-justify">
                        <h1 className={"pl-2 pr-2" + (this.state.page.name === 'about_us' ? ' text-center' : '')} style={{ fontWeight: "900" }}>{page.title}</h1>
                        <div className={"pl-2 pr-2" + (this.state.page.name === 'about_us' ? ' text-center' : '')} dangerouslySetInnerHTML={{__html: page.content}}></div>
                    </div>
                </div>
            </div>
        </div>
        </> 
        )
    }
}