import React from 'react';
import UserService from "../../services/UserService";
import { Helmet } from 'react-helmet';
import HeadeMenus from '../../shared/HeaderMenus/HeaderMenus';

export default  class Pg extends React.Component{

    constructor(props) {
        super(props);  
        this.state = {status: "pending"};
        this.userServ = new UserService();
    }
    
    activateUser(){
        window.scrollTo(0, 0);
        this.token = this.props.match.params.token;
        console.log(this.token,"this.token")
        this.userServ.activate(this.token)
            .then(
                (response)=>{
                    this.setState({status: response.status})
                },
                (error)=>{
                    this.setState({status: "failed"});
                }
            );
    }

    componentDidMount(){
        this.activateUser()
    }

    render() {
        return (
        <>
        <Helmet>
          <title>Activation</title>
          <meta name="keywords" content="ActiaveUser,Activation" />
          <meta name="description" content="User Activation Page" />
        </Helmet> 
        <HeadeMenus />
        <div className="about-area default-padding">
            <div className="container ml-4 pl-4 mt-4 pt-4">
                <div className="row">
                        <div className="col-md-11 text-justify">
                        <h1 className="pl-3" style={{ fontWeight: "900" }}>Account Activation</h1>
                        {this.state.status==="pending" && <div className="pl-3"> Please wait while we activate your account</div>}
                        {this.state.status==="success" && <div className="pl-3"><strong className="text-success"> Congratulation, your account is  active now. </strong></div>}
                        {this.state.status==="failed" && <div className="pl-3"><strong className="text-info"> Unfortunately we are not able to activate your account please contact admin. </strong></div>}
                    </div>
                </div>
            </div>
        </div>
        </> 
        )
    }
}