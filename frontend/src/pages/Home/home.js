import React, { useState, useEffect } from "react";
import LocationService from "../../services/LocationService";
import { Link, useHistory } from "react-router-dom";
import HeadeMenus from "../../shared/HeaderMenus/HeaderMenus";
import CardComponent from "../../shared/CardComponent/CardComponent";
import { Helmet } from "react-helmet";
import PgService from "../../services/pgService";

export default function Home() {
  const history = useHistory();
  const [data, setData] = useState([1, 2, 3, 4, 5]);
  const [location, setLocation] = useState({
    coordinates: { lat: "", lng: "" },
  });
  const [area, setArea] = useState("100");
  const [pageMetaData, setPageMetaData] = useState({});
  const LocationServ = new LocationService();

  const getPage = () => {
    window.scrollTo(0, 0);
    let pagesServ = new PgService();
    pagesServ.getPage("home").then((response) => {
      setPageMetaData(response.data);
    });
  };

  const searchLocation = () => {
    if (!("geolocation" in navigator)) {
      onError({
        message: "Geolocation not supported",
      });
    }

    navigator.geolocation.getCurrentPosition(onSuccess, onError);
  };

  const onSuccess = (location) => {
    setLocation({
      coordinates: {
        lat: location.coords.latitude,
        lng: location.coords.longitude,
      },
    });

    history.push(
      `/search-location/${area}/${location.coords.latitude}/${location.coords.longitude}`
    );
  };

  const onError = (error) => {
    setLocation({
      error,
    });
  };

  const getDefaultLocation = () => {
    if (!("geolocation" in navigator)) {
      onError({
        message: "Geolocation not supported",
      });
    }

    navigator.geolocation.getCurrentPosition(onSuccessDefault, onErrorDefault);
  };

  const onSuccessDefault = async (location) => {
    let obj = {
      is_active: true,
      distance: 3000000,
      lat: location.coords.latitude,
      lng: location.coords.longitude,
    };
    let result = await LocationServ.getLocation(0, 1000, obj);
    setData(result.rows);
  };

  const onErrorDefault = (error) => {
    alert("Geolocation is not enabled. Please enable to use this feature");
  };

  useEffect(() => {
    getDefaultLocation();
    getPage();
  }, []);

  return (
    <>
      <div className="main-wrapper ">
        <Helmet>
          <title>{pageMetaData.meta_title}</title>
          <meta name="keywords" content={pageMetaData.meta_keyword} />
          <meta name="description" content={pageMetaData.meta_desc} />
        </Helmet>
        <HeadeMenus title="" />
        <div className="home">
          <div className="page-content header-bg">
            <div className="top-search">
              <div className="container navbar-inner">
                <div className="search-area">
                  <form action="search">
                    <div className="list inset">
                      <div className="distanceMainCls">
                        <div
                          className=""
                          value={area}
                          onChange={(e) => setArea(e.target.value)}
                        >
                          <select className="distanceCls">
                            <option value="2000"> Within 2KM</option>
                            <option value="5000"> Within 5KM</option>
                            <option value="20000"> Within 20KM</option>
                          </select>
                        </div>
                      </div>
                      <div className="distanceMainCls">
                        <Link
                          onClick={searchLocation}
                          className="button button-large button-primary btn customSearchBtn gapDis"
                        >
                          <b>Search Now</b>
                        </Link>
                      </div>

                      <div className="clearBothCls"></div>

                      <div className="dynamicFont">
                        <Link
                          className=""
                          to={"/pg/how_to_use"}
                          style={{ color: "yellow" }}
                        >
                          <div className="freeCls">
                            <center>
                              <span>Free of charge</span>
                            </center>
                          </div>
                        </Link>

                        <div className="pipeCls">
                          <center>
                            <span>|</span>
                          </center>
                        </div>
                        <Link
                          className=""
                          to={"/pg/how_to_use"}
                          style={{ color: "yellow" }}
                        >
                          <div className="topCls">
                            <center>
                              <span>Top-class hygiene & safety</span>
                            </center>
                          </div>
                        </Link>

                        <div className="pipeCls">
                          <center>
                            <span>|</span>
                          </center>
                        </div>
                        <Link
                          className=""
                          to={"/pg/how_to_use"}
                          style={{ color: "yellow" }}
                        >
                          <div className="seamCls">
                            <center>
                              <span>Seamless experience</span>
                            </center>
                          </div>
                        </Link>
                      </div>

                      {/* <ul>
                        <li className="d-flex">
                          <div className="item-col">
                            <b style={{ fontSize: "14px", textAlign: "center", display: "block" }}>
                              <p>Clean restrooms at your fingertips.</p>
                              <p>Use the dropdown to search options near you.</p>
                            </b>
                          </div>
                        </li>
                        <li className="d-flex">
                          <div className="item-icon">
                            <i className="search-icon fas fa-building"></i>
                          </div>
                          <div className="item-col" value={area} onChange={(e) => setArea(e.target.value)}>
                           
                            <select>
                              
                              
                              <option value="2000"> With in 2KM</option>
                              
                              <option value="5000"> With in 5KM</option>
                              
                              <option value="20000"> With in 20KM</option>
                              S
                            </select>
                          </div>
                        </li>
                      </ul> */}

                      {/* <Link
                        // to={`/search-location/${area}/${location.coordinates.lat}/${location.coordinates.lng}`}
                        onClick={searchLocation}
                        className="button button-large button-primary btn"
                      >
                        Search Now
                      </Link> */}
                    </div>
                  </form>
                  <div>
                    <img
                      src="/assets/img/imageIcon1.jpg"
                      style={{ marginTop: "10px", width: "100%" }}
                    ></img>
                  </div>
                </div>
              </div>
            </div>
            <div className="focused segments mainHeading">
              <div className="container">
                <div className="focused-content">
                  <div className="section-title sectionDiv-title">
                    <h3>
                      {data && data.length > 0
                        ? "Partner Locations"
                        : "No Locations Found."}
                      {/* <Link to={"search-doctor.html"} className="see-all-link">View All</Link> */}
                    </h3>
                  </div>
                  <div className="focused segments mt-0 pb-0 search-doctor">
                    <div className="focused-content">
                      <div className="swiper-container">
                        {data &&
                          data.map((dataTitle) => {
                            return (
                              <CardComponent
                                locationData={dataTitle}
                                getReviewDetails={getDefaultLocation}
                              />
                            );
                          })}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
