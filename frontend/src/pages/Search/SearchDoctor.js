import React, { useState, useEffect } from "react";
import { Link, image, useParams } from "react-router-dom";
import HeadeMenus from "../../shared/HeaderMenus/HeaderMenus";
import CardComponent from "../../shared/CardComponent/CardComponent";
import LocationService from "../../services/LocationService";

export default function SearchDoctor({match}) {
  const [data, setData] = useState([1, 2, 3, 4, 5]);
  const [dataCount, setDataCount] = useState(0);
  const LocationServ = new LocationService();
  const {distance, lat,lng} = useParams();
  async function listLocation() {
    let result = await LocationServ.getLocation(0,1000,{is_active:true,distance,lat,lng});
    setData(result.rows);
    setDataCount(result.count);
  }
  useEffect(() => {
    listLocation();
  }, []);
  return (
    <>
      <div className="main-wrapper">
        <div className="side-menu" id="sidebar-menu"></div>
        <HeadeMenus />
        <div className="page-content pt-0">
          <div className="container">
            {/* <!-- Search tag --> */}
            <div className="search-tag">
              {/* <h6>{dataCount} matches found. </h6> */}
              <h6>{data && data.length > 0 ? data.length : 0} matches found. </h6>
            </div>
            {/* <!-- /Search tag --> */}

            {/* <!-- focused --> */}
            <div className="focused segments mt-0 pb-0 search-doctor">
              <div className="focused-content">
                <div className="swiper-container">
                  {data?.map((dataTitle) => {
                    return <CardComponent locationData={dataTitle}  getReviewDetails={listLocation}/>
                  })}
                </div>
              </div>
            </div>

            {/* <!-- Filter --> */}
            <div className="doctor-filter">
              <img src="assets/image/filter.svg" alt="" />
            </div>
            {/*  <!-- /Filter --> */}

            {/* <!-- Rating popup --> */}
            <div
              className="modal fade"
              id="doctor-filter-modal"
              tabindex="-1"
              role="dialog"
              aria-hidden="true"
            >
              <div
                className="modal-dialog modal-dialog-centered"
                role="document"
              >
                <div className="modal-content">
                  <div className="modal-header">
                    <Link
                      className="link popup-close close"
                      to={"#"}
                      data-dismiss="modal"
                      aria-label="Close"
                    >
                      <i className="fas fa-times"></i>
                    </Link>
                  </div>
                  <div className="modal-body">
                    <div className="popup custom-filter-popup">
                      <div className="inner-popup-block">
                        <div className="filter-col">
                          <form method="post">
                            <input
                              type="text"
                              className="filter-box"
                              placeholder="Filter"
                            />
                            <input
                              type="submit"
                              name=""
                              className="clear-btn"
                              value="Clear"
                            />
                          </form>
                        </div>
                        <div className="block-title">Date</div>
                        <div className="list no-hairlines-md">
                          <ul>
                            <li>
                              <input
                                type="text"
                                name="date"
                                id="date"
                                data-select="datepicker"
                              />
                              <span className="calendar-icon">
                                <img
                                  src="assets/image/icon-metro-calendar-big.svg"
                                  alt=""
                                />
                              </span>
                            </li>
                          </ul>
                        </div>
                        <div className="block-title">Gender</div>
                        <div className="list custom-list-two">
                          <ul>
                            <li>
                              <div className="item-radio item-content">
                                <div className="custom-control custom-radio custom-control-inline">
                                  <input
                                    type="radio"
                                    className="custom-control-input"
                                    name="customRadio"
                                    id="customRadio1"
                                  />
                                  <label
                                    className="custom-control-label"
                                    for="customRadio1"
                                  >
                                    <span>Male</span>
                                  </label>
                                  <span className="gender-icon">
                                    <img
                                      src="assets/image/maledoc.svg"
                                      alt=""
                                    />
                                  </span>
                                </div>
                              </div>
                            </li>
                            <li>
                              <div className="item-radio item-content">
                                <div className="custom-control custom-radio custom-control-inline">
                                  <input
                                    type="radio"
                                    className="custom-control-input"
                                    name="customRadio"
                                    id="customRadio2"
                                  />
                                  <label
                                    className="custom-control-label"
                                    for="customRadio2"
                                  >
                                    <span>Female</span>
                                  </label>
                                  <span className="gender-icon">
                                    <img
                                      src="assets/image/doctofemr.svg"
                                      alt=""
                                    />
                                  </span>
                                </div>
                              </div>
                            </li>
                          </ul>
                        </div>
                        <div className="block-title">Select Specialist</div>
                        <div className="list custom-list-two custom-specialist">
                          <ul>
                            <li className="">
                              <div className="item-content item-checkbox">
                                <div className="custom-control custom-checkbox">
                                  <input
                                    type="checkbox"
                                    className="custom-control-input"
                                    id="Urology"
                                  />
                                  <label
                                    className="custom-control-label"
                                    for="Urology"
                                  >
                                    Urology
                                  </label>
                                </div>
                              </div>
                            </li>
                            <li className="">
                              <div className="item-content item-checkbox">
                                <div className="custom-control custom-checkbox">
                                  <input
                                    type="checkbox"
                                    className="custom-control-input"
                                    id="Cardiologist"
                                  />
                                  <label
                                    className="custom-control-label"
                                    for="Cardiologist"
                                  >
                                    Cardiologist
                                  </label>
                                </div>
                              </div>
                            </li>
                            <li className="">
                              <div className="item-content item-checkbox">
                                <div className="custom-control custom-checkbox">
                                  <input
                                    type="checkbox"
                                    className="custom-control-input"
                                    id="Dentist"
                                  />
                                  <label
                                    className="custom-control-label"
                                    for="Dentist"
                                  >
                                    Dentist
                                  </label>
                                </div>
                              </div>
                            </li>
                            <li className="">
                              <div className="item-content item-checkbox">
                                <div className="custom-control custom-checkbox">
                                  <input
                                    type="checkbox"
                                    className="custom-control-input"
                                    id="Neurology"
                                  />
                                  <label
                                    className="custom-control-label"
                                    for="Neurology"
                                  >
                                    Neurology
                                  </label>
                                </div>
                              </div>
                            </li>
                            <li className="">
                              <div className="item-content item-checkbox">
                                <div className="custom-control custom-checkbox">
                                  <input
                                    type="checkbox"
                                    className="custom-control-input"
                                    id="Orthologist"
                                  />
                                  <label
                                    className="custom-control-label"
                                    for="Orthologist"
                                  >
                                    Orthologist
                                  </label>
                                </div>
                              </div>
                            </li>
                            <li className="">
                              <div className="item-content item-checkbox">
                                <div className="custom-control custom-checkbox">
                                  <input
                                    type="checkbox"
                                    className="custom-control-input"
                                    id="Gynaecology"
                                  />
                                  <label
                                    className="custom-control-label"
                                    for="Gynaecology"
                                  >
                                    Gynaecology
                                  </label>
                                </div>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div
                        className="apply-filter-btn"
                        data-dismiss="modal"
                        aria-label="Close"
                      >
                        <Link to={"#"} className="btn">
                          Apply Filter
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
