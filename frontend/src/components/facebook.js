import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import FacebookLogin from "react-facebook-login";
import SocialLoginService from "../services/socialLoginService";
export default class facebook extends Component {
  constructor(props) {
    super(props);

    this.state = {
      redirect: false,
      isLoggIn: false,
      userId: "",
      name: "",
      email: "",
      picture: "",
    };

    this.socialLoginServ = new SocialLoginService();
  }
  componentClicked = () => {
    console.log("facebook clock");
  };
  responseFacebook = (response) => {
    let obj = {
      title: "Mr",
      first_name: response.name,
      email: response.email,
      phone: "",
      role: 2,
      password: "",
      type: "facebook",
      token: "",
    };
    this.socialLoginServ
      .sociallogin(obj)
      .then((response) => {
        if (response.error) {
          console.log(response.error, "error");
        } else {
          toast.success(response.message);
          this.setState({ redirect: true });
        }
      })
      .catch((err) => {
        // toast.error(err.message);
        this.setState({ redirect: false });
      });
  };
  render() {
    if (this.state.redirect === true) {
      return <Redirect to="/" />;
    }
    let fbContent;
    if (this.state.isLoggIn) {
      fbContent = null;
    } else {
      fbContent = (
        <FacebookLogin
          appId="465723617945866"
          autoLoad={true}
          fields="name,email,picture"
          onClick={this.componentClicked}
          callback={this.responseFacebook}
        />
      );
    }
    return <div>{fbContent}</div>;
  }
}
