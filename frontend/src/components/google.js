import React, { Component } from "react";
import GoogleLogin from "react-google-login";
import { Link, Redirect } from "react-router-dom";
import UserService from "../services/UserService";
import SocialLoginService from "../services/socialLoginService";

export default class Google extends Component {
  constructor(props) {
    super(props);

    this.state = {};

    this.userServ = new UserService();
    this.socialLoginServ = new SocialLoginService();
  }
  click = () => {};
  responseGoogle = (response) => {
    console.log(response, "Google auths");
    // profileObj
    let obj = {
      title: "Mr",
      first_name: response.profileObj.name,
      email: response.profileObj.email,
      phone: "",
      role: 2,
      password: "",
      type: "google",
      token: response.profileObj.googleId,
    };

    this.socialLoginServ
      .sociallogin(obj)
      .then((response) => {
        console.log(response, "respo google");
        if (response.error) {
          // toast.error(response.error);
          console.log(response.error, "error");

        } else {
          // toast.success("User Registered successfully");
          toast.success(response.message);
          this.setState({ redirect: true });
        }
      })
      .catch((err) => {
        // toast.error(err.message);
        this.setState({ redirect: false });
      });
  };

  render() {
    if (this.state.redirect === true) {
      return <Redirect to="/" />;
    }
    return (
      <div>
        <GoogleLogin
          clientId="94523924094-trgqaio2uqj8vuo77p7diiok8o55ouok.apps.googleusercontent.com"
          buttonText="Login with Google"
          onSuccess={this.responseGoogle}
          onFailure={this.responseGoogle}
          cookiePolicy={"single_host_origin"}
        />
      </div>
    );
  }
}
