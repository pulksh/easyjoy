import util from "../utils/util";

export default class UserService {
  sociallogin(payload) {
    return util
      .sendApiRequest("/sociallogin/soclogin", "POST", true, payload)
      .then((response) => {
        if (!response.error) {
          localStorage.setItem(
            "user",
            JSON.stringify({ token: response.token, ...response.result })
          );
          window.user = { token: response.token, ...response.result };
          return window.user;
        } else {
          return response;
        }
      })
      .catch((e) => {
        throw e;
      });
  }
}
