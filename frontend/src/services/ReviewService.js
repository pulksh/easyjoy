import util from "../utils/util"

export default  class ReviewService{

    async getReview(data) {
       try{
           return await util.sendApiRequest("/review/"+data,"GET",true)
        }
        catch(err){
            throw err 
    }       
}

    async addReview(data) {
        
        try{
            return await util.sendApiRequest("/review/","POST",true,data)
        }
        catch(err){
            throw err 
        }
       
    }

    async editReview(data) {
        try{
            return await util.sendApiRequest("/review/","PUT",true,data)
        }
        catch(err){
            throw err 
        }
       
    }

    async deleteReview(id) {
        try{
            return await util.sendApiRequest('/review/' + id,"DELETE",true)
        }
        catch(err){
            throw err 
        }
       
    }
   async listReview(start,length,filter) {
        try{
            start = start ? start : 0
            length = length ? length : 100
            return await util.sendApiRequest("/review/list/"+start+"/"+length, "POST",true, filter)
        }
        catch(err){
            throw err
        }
       
     }
}