import util from "../utils/util";

export default class CheckinoutService {
  async addCheckinout(data) {
    try {
      return await util.sendApiRequest("/checkinout/", "POST", true, data);
    } catch (err) {
      throw err;
    }
  }

  async editCheckinout(data) {
    try {
      return await util.sendApiRequest("/checkinout/", "PUT", true, data);
    } catch (err) {
      throw err;
    }
  }

  async sendMail(data) {
    try {
      return await util.sendApiRequest("/checkinout/sendmail", "POST", true, data);
    } catch (err) {
      throw err;
    }
  }
}
