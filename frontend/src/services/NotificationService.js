import util from "../utils/util";

export default class NotificationService {
  async addNotification(data) {
    try {
      return await util.sendApiRequest("/notification/", "POST", true, data);
    } catch (err) {
      throw err;
    }
  }
}