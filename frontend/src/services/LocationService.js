import util from "../utils/util"

export default  class LocationService{

    async getLocById(lid) {
       try{
           return await util.sendApiRequest("/location/"+lid,"GET")
        }
        catch(err){
            throw err 
    }
   }

    async addLocation(data) {
        try{
            return await util.sendApiRequest("/location/","POST",true,data)
        }
        catch(err){
            throw err 
        }
       
    }

    async editLocation(data) {
        try{
            return await util.sendApiRequest("/location/","PUT",true,data)
        }
        catch(err){
            throw err 
        }
       
    }

    async deleteLocation(id) {
        try{
            return await util.sendApiRequest('/location/' + id,"DELETE",true)
        }
        catch(err){
            throw err 
        }
       
    }
   async listLocation(start,length,filter) {
        try{
        start = start ? start : 0
        length = length ? length : 100
        return await util.sendApiRequest("/location/list/"+start+"/"+length, "POST",true,filter)
        }
        catch(err){
            throw err
        }
       
     }

     async getLocation(start,length,filter) {
        try{
        start = start ? start : 0
        length = length ? length : 100
        return await util.sendApiRequest("/location/listlocation/"+start+"/"+length, "POST",true,filter)
        }
        catch(err){
            throw err
        }
       
     }
}