import util from "../utils/util";

export default class SettingService {
  getService(id) {
    return util.sendApiRequest("/setting/" + id, "GET", true).then(
      response => {
        return response;
      },
      error => {
        throw new Error(error);
      }
    );
  }

  editService(service) {
    return util.sendApiRequest("/setting", "PUT", true, service).then(
      response => {
        return response;
      },
      error => {
        throw new Error(error);
      }
    );
  }

  listService(data, start, perpage) {
    const setting = Object.keys(data).reduce((object, key) => {
      if (data[key] !== "") {
        object[key] = data[key];
      }
      return object;
    }, {});
    return util
      .sendApiRequest(
        "/setting/list/" + start + "/" + perpage,
        "POST",
        true,
        setting
      )
      .then(
        response => {
          return response;
        },
        error => {
          throw new Error(error);
        }
      );
  }

  addService(service) {
    return util.sendApiRequest("/setting", "POST", true, service).then(
      response => {
        return response;
      },
      error => {
        throw new Error(error);
      }
    );
  }
  deleteService(id) {
    return util.sendApiRequest("/setting/" + id, "DELETE", true).then(
      response => {
        return response;
      },
      error => {
        throw new Error(error);
      }
    );
  }
  addBlockCountryList(body) {
    return util
      .sendApiRequest("/allowedcountry", "POST", true, body)
      .then(
        response => {
          return response;
        },
        error => {
          throw new Error(error);
        }
      );
  }

  listBlockedCountry(data,start, perpage) {
    console.log(start,'start')
    console.log(perpage,'perpage')
    
    const product = Object.keys(data).reduce((object, key) => {
      if (data[key] !== "") {
        object[key] = data[key];
      }
      return object;
    }, {});

    return util
      .sendApiRequest("/allowedcountry/list/" + start + "/" + perpage, "POST", true,product)
      .then(
        response => {
          return response;
        },
        error => {
          throw new Error(error);
        }
      );
  }
  
  deleteBlockedCountry(id) {
    return util
      .sendApiRequest("/allowedcountry/" + id, "DELETE", true)
      .then(
        response => {
          return response;
        },
        error => {
          throw new Error(error);
        }
      );
  }
  deleteAllBlockedCountry(body) {
    return util
    .sendApiRequest("/allowedcountry/removeallblockedcountry/" , "POST", true,body)
    .then(
      (response) => {
        return response;
      },
      (error) => {
        throw new Error(error);
      }
    );
}
addAllBlockCountryList(body) {
  return util
    .sendApiRequest("/allowedcountry/addallblockedcountry", "POST", true, body)
    .then(
      (response) => {
        return response;
      },
      (error) => {
        throw new Error(error);
      }
    );
}
}
