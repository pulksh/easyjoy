import React, { useState } from "react";
import { Link, Redirect } from "react-router-dom";
import ReviewService from "../../services/ReviewService";
import CheckinoutService from "../../services/CheckinoutService";
import NotificationService from "../../services/NotificationService";
import { Modal } from "react-modal-overlay";
import "react-modal-overlay/dist/index.css";
import RatingComponent from "../RatingComponent/RatingComponent";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
function CardComponent(props) {
  const dataTitle = props.locationData;
  const showFullDesc = props.showFullDesc ? props.showFullDesc : false;
  const [show, setShow] = useState(false);
  const [rating, setRating] = useState(0);
  const [ratingHygiene, setRatingHygiene] = useState(0);
  const [ratingCleanPotSeat, setSatingCleanPotSeat] = useState(0);
  const [ratingHandwash, setRatingHandwash] = useState(0);
  const [ratingDryseat, setRatingDryseat] = useState(0);
  const [review, setReview] = useState("");
  const [color, setColour] = useState("Check In");
  const [colorChange, setColourChange] = useState(false);
  const [checkin, setCheckin] = useState("");
  const [redirect, setRedirect] = useState(false);
  const [mapView, setMapView] = useState(false);
  const checkinoutServ = new CheckinoutService();
  const notificationServ = new NotificationService();

  function handleRating(ratingCount) {
    setRating(ratingCount);
  }

  function handleReview(event) {
    setReview(event.target.value);
  }

  async function handleCheckInOut(dataTitle) {
    if (window.user && window.user.id) {
      if (color === "Check In") {
        let objIn = {
          user_id: window.user.id,
          location_id: dataTitle.id,
          checkin_time: new Date(),
          // checkin_time: new Date().toUTCString(),
        };

        let response = await checkinoutServ.addCheckinout(objIn);
        if (response) {
          setCheckin(response);
          setColourChange(!colorChange);
          setColour("Check Out");
        }
      } else {
        setShow(true);
      }
    } else {
      if (window.confirm("Requesting to please login first before check in.")) {
        setRedirect(true);
      }
    }
  }

  const updateRating = (ratCount, name) => {
    if (name === "location_rating") {
      setRating(ratCount);
    } else if (name === "hygiene") {
      setRatingHygiene(ratCount);
    } else if (name === "pot_seat") {
      setSatingCleanPotSeat(ratCount);
    } else if (name === "handwash") {
      setRatingHandwash(ratCount);
    } else if (name === "dry_floor") {
      setRatingDryseat(ratCount);
    }
  };

  async function addReview(e) {
    e.preventDefault();
    let user_review = document.getElementById("reviewId").value;
    let userReview = review || user_review;
    const usrReview = validateReview(userReview);
    if (usrReview === false) {
      return;
    } else {
      const reviewServ = new ReviewService();
      let obj = {
        user_id: window.user.id,
        location_id: dataTitle.id,
        review_order_hygiene: ratingHygiene,
        review_clean_pot_seat: ratingCleanPotSeat,
        review_handwash: ratingHandwash,
        review_dryseat: ratingDryseat,
        rating: rating,
        review: review,
      };
      let reviewRes = await reviewServ.addReview(obj);
      if (reviewRes) {
        let objOut = {
          id: checkin.id,
          user_id: checkin.user_id,
          location_id: checkin.location_id,
          checkin_time: checkin.checkin_time,
          checkout_time: new Date(),
        };

        //  Commenetd due to not check-in check-out as per client discussion

        // let response = await checkinoutServ.editCheckinout(objOut);
        // if (response) {
        //   setColourChange(!colorChange);
        //   setColour("Check In");
        // }
      }
      setShow(false);
      if (props.getReviewDetails) {
        props.getReviewDetails();
      }
    }
  }

  const validateReview = (userReview) => {
    if (userReview === null || userReview === "") {
      const errMsg = "Please write your review✍";
      toast.error(errMsg);
      return false;
    }
  };

  function handleShow(e) {
    if (window.user && window.user.id) {
      setShow(true);
    } else {
      if (
        window.confirm("Requesting to please login first before your review.")
      ) {
        setRedirect(true);
      }
    }
  }

  const sendNotification = async () => {
    let Obj = {
      user_id: window.user && window.user.id,
      location_id: dataTitle && dataTitle.id,
    };
    if (window.user && window.user.id) {
      let response = await notificationServ.addNotification(Obj);
      if (response) {
        if (dataTitle.url) {
          window.open(dataTitle.url);
        } else {
          alert(
            "Thanks for your interest. Unfortunately we are not able to show you this location over google maps. So please check for another location."
          );
        }
      }
    } else {
      alert("Please Login first.");
      setRedirect(true);
    }
  };

  if (!dataTitle) {
    return "";
  }

  const time = new Date();
  let currentTime =
    (time.getHours() < 10 ? "0" + time.getHours() : time.getHours()) +
    ":" +
    (time.getMinutes() < 10 ? "0" + time.getMinutes() : time.getMinutes()) +
    ":" +
    (time.getMinutes() < 10 ? "0" + time.getSeconds() : time.getSeconds());
  let today = time.getDay();

  if (redirect === true) {
    return <Redirect to="/login" />;
  }
  return (
    <div
      style={{ padding: "0px" }}
      className={`doctor-widget ${
        (currentTime >= dataTitle.morning_ot &&
          currentTime <= dataTitle.morning_ct) ||
        (currentTime >= dataTitle.evening_ot &&
          currentTime <= dataTitle.evening_ct) ||
        dataTitle.close_day !== today
          ? ""
          : " disabled_loc"
      }`}
    >
      <div className="partnerCls">
        <div className="partnerSubUpDiv">
          <div className="partnerInnerSub">
            <div>
              <h2 className="doc-name">
                <Link
                  to={(
                    "/review/" +
                    dataTitle.id +
                    "/" +
                    dataTitle.name
                  ).replace(/\s/g, "")}
                >
                  {dataTitle.name}
                </Link>
              </h2>
            </div>

            <div className="docStyle">
              {!showFullDesc && (
                <p className="doc-speciality" style={{ color: "#9a9898" }}>
                  {(dataTitle.description && dataTitle.description.length > 72
                    ? dataTitle.description.substring(0, 72) + "..."
                    : dataTitle.description
                  )?.replace(/(<([^>]+)>)/gi, "")}
                </p>
              )}

              {showFullDesc && (
                <div
                  dangerouslySetInnerHTML={{ __html: dataTitle.description }}
                ></div>
              )}
              {showFullDesc && (
                <div className="doc-info">
                  <div className="rating">
                    {[1, 2, 3, 4, 5].map((el) => {
                      return dataTitle.rating && el <= dataTitle.rating ? (
                        <i className="fas fa-star filled"></i>
                      ) : (
                        <i className="fas fa-star "></i>
                      );
                    })}
                  </div>
                  <div className="doc-location"></div>
                </div>
              )}
            </div>
          </div>

          <div className="disStyleCls">
            <div
              className="clini-infos text-right"
              style={{ fontSize: "12px", color: "#777", lineHeight: "9px" }}
            >
              <b>
                {dataTitle.distance
                  ? parseInt(dataTitle.distance / 1000) > 1
                    ? parseFloat(dataTitle.distance / 1000).toFixed(1) + " km"
                    : parseInt(dataTitle.distance) + " m"
                  : ""}
              </b>
            </div>
          </div>
        </div>

        <div style={{ width: "100%", margin: "3px" }}>
          <div
            style={{
              float: "left",
              height: "46px",
              width: "25%",
              marginBottom: "5px",
              borderTop: "solid 1px #beb8b8",
              display: "flex",
              "align-items": "center",
            }}
          >
            {!showFullDesc && (
              <div className="doc-info" style={{ margin: "5px" }}>
                <div className="rating">
                  {[1, 2, 3, 4, 5].map((el) => {
                    return dataTitle.rating && el <= dataTitle.rating ? (
                      <i className="fas fa-star filled"></i>
                    ) : (
                      <i className="fas fa-star "></i>
                    );
                  })}
                </div>
                <div className="doc-location"></div>
              </div>
            )}
          </div>

          <div
            style={{
              float: "left",
              width: "35%",
              marginBottom: "5px",
              borderTop: "solid 1px #beb8b8",
            }}
          >
            <div className="clinic-btn" style={{ margin: "5px" }}>
              <a
                className="button button-large button-primary btn customSearchBtn"
                onClick={sendNotification}
                style={{ cursor: "pointer", height: "32px", paddingTop: "5px" }}
              >
                Use It
              </a>
            </div>
          </div>

          <div
            style={{
              float: "left",
              width: "37%",
              marginBottom: "5px",
              borderTop: "solid 1px #beb8b8",
            }}
          >
            <button
              className="doc-exp"
              style={{ margin: "5px" }}
              onClick={handleShow}
            >
              Rate It
            </button>
          </div>

          <div style={{ clear: "both" }}></div>
        </div>
      </div>

      <div style={{ clear: "both" }}></div>

      {/* shankar */}
      {/* <div className="doc-info-left">
       
        <div className="doctor-img">
          <Link to={("/review/" + dataTitle.id + "/" + dataTitle.name).replace(/\s/g, "")}>
            {dataTitle.featured_image ? (
              <img src={dataTitle.featured_image} className="img-fluid" alt="User Featured" />
            ) : (
              <img src="/assets/img/doctors/doctor-thumb-01.jpg" className="img-fluid" alt="User featured default" />
            )}
          </Link>
        </div>

        <div className="doc-info-cont">
          <h1 className="doc-name">
            <Link to={("/review/" + dataTitle.id + "/" + dataTitle.name).replace(/\s/g, "")}>{dataTitle.name}</Link>
          </h1>
          {showFullDesc && (
            <div className="doc-info">
              <div className="rating">
                {[1, 2, 3, 4, 5].map((el) => {
                  return dataTitle.rating && el <= dataTitle.rating ? (
                    <i className="fas fa-star filled"></i>
                  ) : (
                    <i className="fas fa-star "></i>
                  );
                })}
              </div>
              <div className="doc-location"></div>
            </div>
          )}
          {!showFullDesc && (
            <p className="doc-speciality">
              {(dataTitle.description && dataTitle.description.length > 100
                ? dataTitle.description.substring(0, 100) + "..."
                : dataTitle.description
              )?.replace(/(<([^>]+)>)/gi, "")}
            </p>
          )}
          {!showFullDesc && (
            <div className="doc-info">
              <div className="rating">
                {[1, 2, 3, 4, 5].map((el) => {
                  return dataTitle.rating && el <= dataTitle.rating ? (
                    <i className="fas fa-star filled"></i>
                  ) : (
                    <i className="fas fa-star "></i>
                  );
                })}
              </div>
              <div className="doc-location"></div>
            </div>
          )}
        </div>
      </div>
      {showFullDesc && (
        <div
          style={{ borderBottom: "1px dashed #c5c5c5" }}
          dangerouslySetInnerHTML={{ __html: dataTitle.description }}
        ></div>
      )} 
      <div className="doc-info-right">
        <div className="row">
          <div className="clini-infos col-sm-6">
            <ul>
              <li>
                <Link to={`/review/${dataTitle.id}/${dataTitle.name}`}>Reviews: {dataTitle?.reviews?.length}</Link>
              </li>
            </ul>
            <div style={{ position: "relative" }}>
              <span
                className="text-right"
                style={{
                  position: "absolute",
                  top: "-18px",
                  right: "0px",
                  width: "380px",
                  fontWeight: "800",
                  fontSize: "15px",
                }}
              >
                {color === "Check Out" ? `Your code is: ${checkin.code}` : ""}
              </span>
            </div>
          </div>
          <div className="clini-infos col-sm-6 text-right" style={{fontSize: '12px', color:'#777', lineHeight:'33px'}}>
                {dataTitle.distance ? (parseInt(dataTitle.distance/1000) > 1 ? parseInt(dataTitle.distance/1000) + ' km' : parseInt(dataTitle.distance) + ' m') : ""}
          </div>
        </div>
        <div className="clinic-booking">
          
          <div className="clinic-btn">
            <a className="apt-btn" onClick={sendNotification} style={{ cursor: "pointer" }}>
              Use It
            </a>
          </div>
        </div>
        <div className="clinic-booking">
          <div className="clinic-btn mt-3">
            <button className="doc-exp" onClick={handleShow}>
              Rate your experience
            </button>
          </div>
        </div>
      </div>*/}

      <Modal
        className="rating_popup"
        show={show}
        closeModal={() => {
          setShow(false);
        }}
      >
        <h4>Add your review</h4>
        <hr />
        <div className="p-2" style={{ display: "flex" }}>
          <h6>Odour hygiene : </h6>
          <RatingComponent
            ratingStart={updateRating}
            name="hygiene"
            marginLeft="48px"
          />
        </div>
        <div className="p-2" style={{ display: "flex" }}>
          <h6>Clean Pot Seat : </h6>
          <RatingComponent
            ratingStart={updateRating}
            name="pot_seat"
            marginLeft="44px"
          />
        </div>
        <div className="p-2" style={{ display: "flex" }}>
          <h6>Handwash & Water : </h6>
          <RatingComponent
            ratingStart={updateRating}
            name="handwash"
            marginLeft="13px"
          />
        </div>
        <div className="p-2" style={{ display: "flex" }}>
          <h6>Dry Seat/ Floor : </h6>
          <RatingComponent
            ratingStart={updateRating}
            name="dry_floor"
            marginLeft="44px"
          />
        </div>
        <div className="p-2" style={{ display: "flex" }}>
          <h6> Overall Rating : </h6>
          <RatingComponent
            ratingStart={updateRating}
            name="location_rating"
            marginLeft="50px"
          />
        </div>
        <textarea
          id="reviewId"
          className="add_comment"
          onChange={handleReview}
          placeholder="Write your review"
        ></textarea>
        <div className="send_btn_sec">
          <button onClick={addReview}>Submit Review</button>
        </div>
      </Modal>
      <ToastContainer />
    </div>
  );
}

export default CardComponent;
