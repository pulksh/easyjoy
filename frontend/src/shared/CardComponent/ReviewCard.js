import React from "react";

export default function Cards(props) {
    const {name, rating, review} = props
  return (
    <div className="tab-widget">
      <div className="left-icon-col">
        <span>
          <img src="/assets/img/icon-awesome-user.svg" alt="i" />
        </span>
      </div>

      <div className="right-content-col">
        <div className="doctor-widget">
          <h6 style={{textTransform:"capitalize"}}>
            {name}
          </h6>
          <div className="rating">
            {[1, 2, 3, 4, 5].map((el) => {
              return (rating && el <= rating) ? (
                <i className="fas fa-star filled"></i>
              ) : (
                <i className="fas fa-star"></i>
              );
            })}
          </div>
          
          <p className="pt-3">{review}</p>
        </div>
      </div>
    </div>
  );
}
