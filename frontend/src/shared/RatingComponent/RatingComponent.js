import React, { useState } from "react";

function RatingComponent(props) {
  const [starRating, setstarRating] = useState(0);

  async function handleRating(ratingCount, name) {
    await setstarRating(ratingCount);

    props.ratingStart(ratingCount, name);
  }
  return (
    <ul className="rating_star_list" style={{ marginLeft: props.marginLeft }}>
      {[1, 2, 3, 4, 5].map((ratings) => {
        return (
          <li>
            <i
              className={
                starRating >= ratings ? "fas fa-star filled" : "fas fa-star"
              }
              onClick={() => handleRating(ratings, props.name)}
            ></i>
          </li>
        );
      })}
    </ul>
  );
}

export default RatingComponent;
