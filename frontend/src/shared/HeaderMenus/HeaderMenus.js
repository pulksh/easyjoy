import React, { useState, useEffect } from "react";
import { Link, Redirect } from "react-router-dom";
import SettingService from "../../services/SettingService";

export default function HeadeMenus(props) {
  const [showMenu, setShowMenu] = useState(false);
  const [redirect, setRedirect] = useState(false);
  const [showMenuDropdown, setShowMenuDropdown] = useState(false);
  const [logo, setLogo] = useState("");
  const [settings, setSettings] = useState([]);

  useEffect(() => {
    const settingServ = new SettingService();

    let obj = {};
    settingServ.listService(obj, 0, 100).then(
      (response) => {
        console.log(response, "settings");
        response &&
          response.rows.map((sett) => {
            if (sett.name === "logo") {
              setLogo({ logo: sett.value });
            }
            if (sett.name === "site_title") {
              document.title = sett.value;
            }
            if (sett.name === "favicon_icon") {
              document.getElementById("favicon").href = sett.value;
            }
          });
        setSettings({ settings: response });
      },
      (error) => {
        alert("Opps! Something went wrong not able to fetch data.");
      }
    );
  }, []);

  const closeMenu = () => {
    setShowMenu(false);
  };
  const openMenu = () => {
    setShowMenu(true);
  };
  const toggleDropdown = () => {
    setShowMenuDropdown(!showMenuDropdown);
  };
  const logout = () => {
    window.user = null;
    localStorage.clear();

    // this fix is implemented to resolve facebook login issue of second time.
    //setRedirect(true);
    window.location.href = window.location.origin;
  };

  if (redirect === true) {
    <Redirect to="/home" />;
  }
  return (
    <>
      <div
        className={showMenu ? "side-menu  show-menu" : "side-menu"}
        id="sidebar-menu"
      >
        <div
          className="close-btn"
          style={{ "z-index": "999" }}
          onClick={closeMenu}
        >
          <span className="material-icons">close</span>
        </div>
        <ul>
          <li>
            {/* <Link className="" to={"/home"}> */}
            <Link className="" to={"/"}>
              <span className="material-icons">dashboard</span>
              Home
            </Link>
          </li>
          <li>
            {window.user && window.user.id && (
              <Link
                className=""
                to={`/user-profile/${window.user && window.user.id}`}
              >
                <span className="material-icons">person</span>
                My Profile
              </Link>
            )}
          </li>
          <li>
            <Link className="" to={"/pg/about_us"}>
              <div
                style={{
                  background: "#1b5a90",
                  "border-radius": "20px",
                  fontSize: "5px",
                  height: "30px",
                  width: "30px",
                  "margin-bottom": "8px",
                }}
              >
                <i
                  style={{
                    color: "white",
                    fontSize: "14px",
                    "margin-left": "6px",
                    "margin-right": "6px",
                  }}
                  className="fas fa-address-card"
                ></i>
              </div>{" "}
              <div style={{ "margin-bottom": "6px" }}>
                &nbsp;&nbsp;&nbsp; About Us & About You
              </div>
            </Link>
          </li>
          {/* <li>
            <Link className="" to={"/pg/how_to_use"}>
              <span className="material-icons">howtouse</span>
              How to use
            </Link>
          </li> */}
          <li>
            <Link className="" to={"/pg/how_to_use"}>
              <div className="howToUseCls">
                <i
                  style={{
                    color: "white",
                    fontSize: "16px",
                    marginLeft: "6px",
                    marginRight: "6px",
                  }}
                  className="fas fa-question-circle"
                ></i>
              </div>{" "}
              <div style={{ "margin-bottom": "6px" }}>
                &nbsp;&nbsp;&nbsp; How to use{" "}
              </div>
            </Link>
          </li>
        </ul>
        {window.user && window.user.id && (
          <Link className="sidebar-logout" onClick={logout} to={"/"}>
            <span>
              <span>
                <img src="/assets/img/open-account-logout.svg" alt="" />
              </span>
              Logout
            </span>
          </Link>
        )}
        {(!window.user || !window.user.id) && (
          <>
            {/* <Link className="sidebar-logout" to={"/"}> */}
            <Link className="sidebar-logout" to={"/login"}>
              <span>
                <img src="/assets/img/open-account-logout.svg" alt="" />
              </span>
              Login
            </Link>
          </>
        )}
        <div>
          <br />
          <br />
          <br />
          <Link
            className=""
            style={{ position: "absolute", left: "25px" }}
            to={"/pg/terms_of_use"}
          >
            Terms of Use
          </Link>
        </div>
      </div>

      <div className="navbar two-action no-hairline">
        <div className="navbar-inner d-flex align-items-center">
          <div className="left">
            <span onClick={openMenu} className="link icon-only">
              <i class="fas fa-2x	fa-th menuIcon">
                {/* <i className="custom-hamburger"> */}
                <span>
                  <b></b>
                </span>
              </i>
            </span>
          </div>

          <div className="sliding custom-title">
            <center>
              <b>
                <span className="headTextCls">
                  {props.title ? props.title : "Easyjoy"}
                </span>
              </b>{" "}
              <br></br>
              <span>Certified clean washrooms near you</span>
            </center>
          </div>

          {/* <div className="sliding custom-title">
            {logo ? <img src={logo.logo} alt="logo" /> : ""} {' '}
            {props.title ? props.title : ""}
          </div> */}
          {/* <div className="right d-flex">
            <a href="#" className="link icon-only">
              <i className="material-icons">notifications</i>
            </a>
            <Link onClick={toggleDropdown} className="link">
              <i className="material-icons">more_vert</i>
            </Link>
            <div
              className={
                showMenuDropdown
                  ? "dropdown-menu dropdown-menu-right header_drop_icon show"
                  : "dropdown-menu dropdown-menu-right header_drop_icon"
              }
            >
              {window.user && window.user.id ? (
                <>
                  <Link
                    to={`/user-profile/${window.user && window.user.id}`}
                    className="dropdown-item"
                  >
                    My Profile
                  </Link>
                  <Link to={"profile-settings.html"} className="dropdown-item">
                    Settings
                  </Link>
                  <Link onClick={logout} className="dropdown-item">
                    Log Out
                  </Link>
                </>
              ) : (
                <Link className="dropdown-item" to={"/login"}>
                  Login
                </Link>
              )}
            </div>
          </div>
         */}
        </div>
        {/* <div className="navbar-inner d-flex align-items-center">

          <div className="sliding custom-title">
            <div style={{float:"left", width:'50%'}}>
              <select>
                <option value="2000"> With in 2KM</option>
                <option value="5000"> With in 5KM</option>
                <option value="20000"> With in 20KM</option>
              </select>
            </div>
            <div style={{float:"left", width:'50%'}}>
              
            </div>
          </div> 
          </div>*/}
      </div>
    </>
  );
}
