import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
// window.API_URL="http://localhost:3026";
window.API_URL = "http://localhost:3002";
window.fronturl = "http://localhost:3001";

window.user = JSON.parse(localStorage.getItem("user")) ? JSON.parse(localStorage.getItem("user")) : null;
ReactDOM.render(
  <>
    <App />
  </>,
  document.getElementById('root')
);

// React.StrictMode If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
