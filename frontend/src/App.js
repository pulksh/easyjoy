import "./App.css";
import React from "react";
import Home from "./pages/Home/home";
import SearchDoctor from "./pages/Search/SearchDoctor";
import HeadeMenus from "./shared/HeaderMenus/HeaderMenus";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Review from "./pages/Review/Review";
import Login from "./pages/Login/Login";
import Registration from "./pages/Registration/Registration";
import UserProfile from "./pages/UserProfile/UserProfile";
import ChangePassword from "./pages/ChangePassword/ChangePassword";
import ForgotPassword from "./pages/ForgotPassword/ForgotPassword";
import PrivacyPolicy from "./pages/PrivacyPolicy/PrivacyPolicy";
import Pg from "./pages/pg/pg";
import Activate from "./pages/pg/activate";

function App() {
  return (
    <>
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          {/* <Route exact path="/home">
            <Home />
          </Route> */}

          <Route exact path="/login">
            <Login />
          </Route>
          <Route
            path="/search-location/:distance/:lat/:lng"
            component={SearchDoctor}
          />
          <Route exact path="/header-menus">
            <HeadeMenus />
          </Route>
          <Route exact path="/registration" component={Registration} />
          <Route exact path="/user-profile/:id" component={UserProfile} />
          <Route exact path="/forgot-password" component={ForgotPassword} />
          <Route exact path="/reset/:token" component={ChangePassword} />
          <Route exact path="/review/:id/:locname" component={Review} />
          <Route exact path="/privacy-policy" component={PrivacyPolicy} />
          <Route path="/pg/:id" component={Pg} />
          <Route path="/activate/:token" component={Activate} />
        </Switch>
      </Router>
      <div class="icon-bar">
        <a
          href="http://www.facebook.com/EasyJoy.in"
          target="_blank"
          className="facebook mb-2"
        >
          <img src="/assets/img/facebook.svg" alt="Facebook" />
        </a>
        <a
          href="http://www.instagram.com/EasyJoy.in"
          target="_blank"
          className="instagram mb-2"
        >
          <img src="/assets/img/instagram.svg" alt="Facebook" />
        </a>
        <a
          href="http://www.twitter.com/easyjoy_in"
          target="_blank"
          className="twitter mb-2"
        >
          <img src="/assets/img/twitter.svg" alt="Twitter" />
        </a>
        <a
          href="https://www.linkedin.com/in/easyjoy-in-8a3896227/"
          target="_blank"
          className="linkedin"
        >
          <img src="/assets/img/linkedin.svg" alt="LinkedIn" />
        </a>
        <a
          href="https://api.whatsapp.com/send/?phone=+919113261810&text&app_absent=0"
          target="_blank"
          className="whatsapp"
          style={{ paddingTop: "5px" }}
        >
          <img src="/assets/img/whatsapp.png" alt="WhatsApp" />
        </a>
      </div>
    </>
  );
}

export default App;
