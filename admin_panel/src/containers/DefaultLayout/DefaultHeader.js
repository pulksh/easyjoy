import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import {
  UncontrolledDropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
  NavItem
} from "reactstrap";
import PropTypes from "prop-types";
import SettingService from "../../services/settingService";
import { AppNavbarBrand, AppSidebarToggler } from "@coreui/react";
import logo from "../../assets/img/brand/sygnet.svg";
import sygnet from "../../assets/img/brand/sygnet.svg";

const propTypes = {
  children: PropTypes.node
};

const defaultProps = {};

class DefaultHeader extends Component {
  constructor(props) {
    super(props);
    this.state = { settings: [], logo: "" };
    this.settingServ = new SettingService();
  }

  componentDidMount()
  {
    this.getSetting()
    
  }
  getSetting()
  {
    let obj={}
    this.settingServ.listService(obj,0,100).then(
      response => {
        console.log(response,'settings');
        response && response.rows.map(sett=>{
          if(sett.name=="logo")
          {
            this.setState({ logo: sett.value });
          }
          if(sett.name=="site_title")
          {
            document.title = sett.value
          }
          if(sett.name=="favicon_icon")
          {
            document.getElementById("favicon").href=sett.value
          }
        })
      this.setState({ settings: response });
      },
      error => {
        alert("Opps! Something went wrong not able to fetch data.");
      }
    );
  }
  render() {
    // eslint-disable-next-line
    console.log(this.state.logo,'logos')
    const { children, ...attributes } = this.props;
    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: this.state.logo, width: 120, height: 60, alt: "CoreUI Logo" }}
          minimized={{ src: this.state.logo, width: 30, height: 30, alt: "CoreUI Logo" }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <NavLink to="/dashboard" className="nav-link">
              Dashboard
            </NavLink>
          </NavItem>
        </Nav>
        <Nav className="ml-auto" navbar>
          <UncontrolledDropdown nav direction="down">
            <DropdownToggle nav>
              {/* <img src={'../../assets/img/avatars/6.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com" /> */}
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem>
                <i className="fa fa-user"></i> Profile
              </DropdownItem>
              <DropdownItem>
                <i className="fa fa-wrench"></i> Settings
              </DropdownItem>
              <DropdownItem onClick={(e) => this.props.onLogout(e)}>
                <i className="fa fa-lock"></i> Logout
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
