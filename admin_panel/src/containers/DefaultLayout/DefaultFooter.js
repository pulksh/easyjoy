import React, { Component } from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultFooter extends Component {
  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      // Copyright © 2020 Publishing Clinic. All rights reserved
      <React.Fragment>
        <span><a href="http://ritzyware.com/" target = "_blank">Copyright</a> &copy;2020 Easyjoy. All rights reserved</span>
        {/* <span className="ml-auto"><a href="http://ritzyware.com/" target = "_blank">Ritzyware PVT LTD</a></span> */}
      </React.Fragment>
    );
  }
}

DefaultFooter.propTypes = propTypes;
DefaultFooter.defaultProps = defaultProps;

export default DefaultFooter;
