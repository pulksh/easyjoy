import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Link } from "react-router-dom";

function BackButton() {
  return (
    <Row>
      <Col>
        <div>
          <p
            style={{ textDecoration: "underline", cursor: "pointer" }}
            onClick={() => {
              window.history.back();
            }}
          >
            <span> &lt; Back</span>
          </p>
        </div>
      </Col>
    </Row>
  );
}

export default BackButton;
