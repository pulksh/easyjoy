export default {
  items: [
    {
      name: "Dashboard",
      url: "/dashboard",
      icon: "icon-speedometer",
    },
    {
      name: "Location List",
      url: "/ownerlist",
      icon: "icon-speedometer",
    },
    {
      name: "Notifications",
      url: "/notification-list",
      icon: "icon-star",
    },
    {
      name: "Agreement",
      url: "/agreement-list",
      icon: "icon-star",
    },
    // {
    //   name: "Check-in-out List",
    //   url: "/checkinoutowner",
    //   icon: "icon-speedometer",
    // },
    {
      name: "Logout",
      url: "/logout",
      icon: "icon-star",
    },
  ],
};
