import React from 'react';
import Table from 'react-bootstrap/Table';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Pageniation } from "../../components/Pageniation";
import { Link } from "react-router-dom";
import Pages from "../../services/pageService";
import SearchBar from '../../components/searchbar';

export default  class PageList extends React.Component{

    constructor(props) {
        super(props);
        this.state = {pageList:[],totalCount:0};
        this.search = {start:0,perPage:10,searchTxt:"",searchField:""};
        this.pagesServ = new Pages();
    }

    componentDidMount(){
        this.getPageList();
    }

    searchPageList(searchFilters){
        if(searchFilters.fieldname === "Name"){
            searchFilters.fieldname = "name";
        }
        if(searchFilters.fieldname === "Title"){
            searchFilters.fieldname = "title";
        }
        if (searchFilters.fieldname === "") {
            alert("select the fieldname to search");
          } else {   
            this.search.searchTxt =  searchFilters.term;
            this.search.searchField = searchFilters.fieldname;
            this.search.start = 0;
            this.getPageList();
          }
    }
   
    getPageList(){
            let pages = {};
            switch (this.search.searchField) {
              case "Title":
                this.search.searchField = "title";
                break;
            }
            pages[this.search.searchField] = this.search.searchTxt;
        
            this.pagesServ.listService(pages, this.search.start, this.search.perPage).then(
              response => {
                this.setState({ pageList: response.rows, totalCount: response.count });
              },
              error => {
                this.setState({ pageList: [], totalcount: 0 });
              }
            );
    }

    handlePaging(currentPage) {
        this.search.start = currentPage * this.search.perPage;
        this.getPageList();
      }
    handleDelete(id,e){
        if(window.confirm("Are you sure you want to delete this record?")){
            this.pagesServ.deleteService(id).then((response)=>{
                this.getPageList();
            })
        }
    }

    render() {
        let active = Math.ceil(this.search.start / this.search.perPage);
        let data = [];
        for(let i = 0;i<this.state.pageList.length;i++){
            data.push(<tr key = {this.state.pageList[i]._id+""+i}>
                        <td>{this.search.start + i + 1}</td>
                        <td><Link to={{pathname:"/staticPage/edit/"+this.state.pageList[i].id}}>{this.state.pageList[i]['name']}</Link></td>
                        <td dangerouslySetInnerHTML={{__html: this.state.pageList[i]['title'] }}></td>
                        {/* <Button size="sm" variant="danger" onClick ={this.handleDelete.bind(this,this.state.pageList[i]._id)} >Delete</Button> */}
                    </tr>);
        }
        return (
          <div className="address addresslist">
              
                <Row>
                    <Col sm={6}>
                        <h3>Page List</h3>
                    </Col>
                    <Col sm={5}>
                        <SearchBar categories={['Name','Title']} parentFunction={this.searchPageList.bind(this)} />
                    </Col>
                    <Col sm={1}>
                        <div></div>
                    </Col>
                </Row>
                <Row>
                <Col sm={9}>
                        <Pageniation
                            totalRecord={this.state.totalCount}
                            currentPage={active}
                            perPage={this.search.perPage}
                            pageHandler={this.handlePaging.bind(this)}
                        />
                    </Col>
                    <Col sm={3} className="text-right">
                        <Link to={{pathname:"/staticPage/add"}} className="btn btn-primary">Add Page</Link>
                    </Col>
                </Row>
                <Row>
                    <Col sm={12}>
                        <Table striped bordered hover size="sm">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Title</th>
                                </tr>
                            </thead>
                            {data.length !== 0 ?
                            <tbody>{data}</tbody> : <tbody style = {{textAlign : "center"}}><td colSpan = "3"><strong>No Record Found</strong></td></tbody>}
                        </Table>
                    </Col>
                </Row>
               
               
          </div>
        );
    }
}