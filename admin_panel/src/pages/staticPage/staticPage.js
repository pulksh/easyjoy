import React from "react";
import Container from "react-bootstrap/Container";
import SaticPage from "../../services/pageService";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Redirect } from "react-router-dom";
import Form from "react-bootstrap/Form";
import { Formik, ErrorMessage } from "formik";
import * as Yup from "yup";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Editor } from "@tinymce/tinymce-react";

export default class StaticPage extends React.Component {
  constructor(props) {
    super(props);
    this.pages_id = props.match.params.id;
    this.state = { pages: null, redirect: false,content: ''};
    this.state.pages = {
        name:"",
        title:"",
        meta_keyword:"",
        meta_title:"",
        meta_desc:"",
        content:""
    };

    this.handleEditorChange = this.handleEditorChange.bind(this);
    this.pagesServ = new SaticPage();
    if (this.pages_id) {
      this.pagesServ.getService(this.pages_id).then(
        response => {
            let staticPage = response;
          this.setState({ pages: staticPage,content:staticPage.content});
        },
        error => {
          alert("Opps! Something went wrong not able to fetch Pages details.");
        }
      );
    }
    this.schema = Yup.object({
      title: Yup.string().required("Title is a required field"), 
      name: Yup.string().required("Page Name is a required field")
    });
  }

  
  handleEditorChange(content) {
    this.setState({ content: content });
  }

  submitPagesForm(values, actions) {
    actions.setSubmitting(false);

    let pgs = values;
    this.setState({
        pages: pgs
    });

    if (this.state.content) {
        this.state.pages.content = this.state.content;
      }

    if (this.pages_id) {
        this.pagesServ.editService(this.state.pages).then(
        response => {
          setTimeout(() => {
            this.setState({ redirect: true });
          }, 2000);
          this.setState({ errorMsg: "" });
          toast.success(response);
        },
        error => {
          this.setState({ redirect: false });
        }
      );
    } else {
      this.pagesServ.addService(this.state.pages).then(
        response => {
          this.setState({ redirect: true });
        },
        error => {
          this.setState({ redirect: false });
        }
      );
    }
  }

  render() {
    if (this.state.redirect === true) {
      return <Redirect to='/staticPagelist' />;
    }
    return (
      <Formik
        validationSchema={this.schema}
        initialValues={this.state.pages}
        enableReinitialize={true}
        onSubmit={this.submitPagesForm.bind(this)}
        render={({
          values,
          errors,
          status,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
          isSubmitting
        }) => (
          <div className='address addresslist'>
            <Container>
              <Form onSubmit={handleSubmit}>
                <Row>
                  <Col sm={12}>
                    <h3>Page Details</h3>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Page Name*</Form.Label>
                      <Form.Control
                        type='text'
                        value={values.name}
                        name='name'
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.name && !errors.name}
                      />
                      <ErrorMessage name='name'>
                        {msg => <div className='err_below'>{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Title*</Form.Label>
                      <Form.Control
                        type='text'
                        value={values.title}
                        name='title'
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.title && !errors.title}
                      />
                      <ErrorMessage name='title'>
                        {msg => <div className='err_below'>{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                <Col sm={12} md={6}>
                    <Form.Group>
                        <Form.Label>Meta Keyword</Form.Label>
                        <Form.Control
                        type="text"
                        value={values.meta_keyword}
                        name="meta_keyword"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.meta_keyword && !errors.meta_keyword}
                        />
                        <ErrorMessage name="meta_keyword">
                        {msg => <div className="err_below">{msg}</div>}
                        </ErrorMessage>
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={6}>
                    <Form.Group>
                        <Form.Label>Meta Title</Form.Label>
                        <Form.Control
                        type="text"
                        value={values.meta_title}
                        name="meta_title"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.meta_title && !errors.meta_title}
                        />
                        <ErrorMessage name="meta_title">
                        {msg => <div className="err_below">{msg}</div>}
                        </ErrorMessage>
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                <Col sm={12} md={6}>
                    <Form.Group>
                        <Form.Label>Meta Description</Form.Label>
                        <Form.Control
                        type="text"
                        value={values.meta_desc}
                        name="meta_desc"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.meta_desc && !errors.meta_desc}
                        />
                        <ErrorMessage name="meta_desc">
                        {msg => <div className="err_below">{msg}</div>}
                        </ErrorMessage>
                    </Form.Group>
                  </Col>
                </Row>
                <Row> 
                <Col sm={12} md={6}>
                    <Form.Group>
                        <Form.Label>Content*</Form.Label>
                        <Editor 
                        apiKey={window.tinyAPIKEY}
                        init={{
                                    width: 1100,
                                    plugins: [
                                    'advlist autolink lists link image charmap print preview anchor',
                                    'searchreplace visualblocks code fullscreen',
                                    'insertdatetime media table paste code help wordcount'
                                    ],
                                    toolbar:
                                    'undo redo | formatselect | bold italic backcolor | \
                                    alignleft aligncenter alignright alignjustify | \
                                    bullist numlist outdent indent | removeformat | help'
                                }}
                        value={this.state.content}
                        onEditorChange ={this.handleEditorChange} />
                        <ErrorMessage name='content'>
                        {msg => <div className='err_below'>{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                  </Row>
                <Row>
                  <Col sm={12} md={4}></Col>
                  <Col sm={12} md={4}>
                    <button
                      className='btn btn-lg btn-primary btn-block setbgcolor'
                      type='submit'>
                      Save
                    </button>
                  </Col>
                </Row>
              </Form>
              <ToastContainer />
            </Container>
          </div>
        )}
      />
    );
  }
}
