import React from "react";
import Table from "react-bootstrap/Table";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Pageniation } from "../../components/Pageniation";
import { Link } from "react-router-dom";
import MenuService from "../../services/menuService";
import SearchBar from "../../components/searchbar";
import Button from "react-bootstrap/Button";

export default class Menu extends React.Component {
  constructor(props) {
    super(props);
    this.state = { menuList: [], totalCount: 0 };
    this.search = { start: 0, perPage: 10, searchTxt: "", searchField: "" };
    this.menuServ = new MenuService();
  }

  componentDidMount() {
    this.getMenuList();
  }

  searchMenuList(searchFilters) {
    this.search.searchTxt = searchFilters.term;
    this.search.searchField = searchFilters.fieldname;
    this.search.start = 0;
    if (!this.search.searchField) {
      alert(" select the fieldname to search");
    } else {
      this.getMenuList();
    }
  }

  getMenuList() {
    let menu = {};
    switch (this.search.searchField) {
      case "Name":
        this.search.searchField = "name";
        break;
    }

    menu[this.search.searchField] = this.search.searchTxt;
    this.menuServ
      .listService(menu, this.search.start, this.search.perPage)
      .then(
        response => {
          this.setState({
            menuList: response.rows,
            totalCount: response.count
          });
        },


        error => {
          this.setState({ menuList: [], totalcount: 0 });
        }
      );
  }

  handlePaging(currentPage) {
    this.search.start = currentPage * this.search.perPage;
    this.getMenuList();
  }
  handleDelete(id, e) {
    if (window.confirm("Are you sure you want to delete this record?")) {
      this.menuServ.deleteService(id).then(response => {
        this.getMenuList();
      });
    }
  }

  render() {
    let active = Math.ceil(this.search.start / this.search.perPage);
    let data = [];
    for (let i = 0; i < this.state.menuList.length; i++) {
      data.push(
        <tr key={this.state.menuList[i].id}>
          <td>{this.search.start + i + 1}</td>
          <td>
            <Link
              to={{
                pathname:
                  "/menu/edit/" + this.state.menuList[i].id
              }}
            >
              {this.state.menuList[i]["name"]}
            </Link>
          </td>
          <td>
            <Link
              to={{
                pathname:
                  "/menu/edit/" + this.state.menuList[i].id
              }}
            >
              {this.state.menuList[i]["link"]}
            </Link>
          </td>
          <td>
            <Link
              to={{
                pathname:
                  "/menu/edit/" + this.state.menuList[i].id
              }}
            >
              {this.state.menuList[i]["location"]}
            </Link>
          </td>
          <td>
            <Row>
              <Col sm={2}>
                <Button
                  size="sm"
                  variant="danger"
                  onClick={this.handleDelete.bind(this, this.state.menuList[i].id)}>
                  <i className="far fa-trash-alt"></i>
                </Button>
              </Col>
              <Col sm={4}>
                <Link
                  to={{
                    pathname:
                      "/menulinklist/" +
                      this.state.menuList[i].id
                  }}
                >
                  <Button
                  size="sm"
                  variant="primary"
                >
                  Items List
                </Button>
                </Link>
              </Col>
            </Row>
          </td>
        </tr>
      );
    }
    return (
      <div className="address addresslist">
        <Row>
          <Col sm={6}>
            <h3>Menu List</h3>
          </Col>
          <Col sm={5}>
            <SearchBar
              categories={["Name"]}
              parentFunction={this.searchMenuList.bind(this)}
            />
          </Col>
          <Col sm={1}>
            <div></div>
          </Col>
        </Row>
        <Row>
        <Col sm={9}>
            <Pageniation
                totalRecord={this.state.totalCount}
                currentPage={active}
                perPage={this.search.perPage}
                pageHandler={this.handlePaging.bind(this)}
            />
          </Col>
          <Col sm={3} className="text-right">
            <Link
              to={{
                pathname: "/menu/add"
              }}
              className="btn btn-primary"
            >
              Add Menu 
            </Link>
          </Col>
        </Row>
        <Row>
          <Col sm={12}>
            <Table striped bordered hover size="sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Link</th>
                  <th>Location</th>
                  <th>Action</th>
                </tr>
              </thead>
              {data.length !== 0 ?
              <tbody>{data}</tbody> : <tbody style = {{textAlign : "center"}}><td colSpan = "5"><strong>No Record Found</strong></td></tbody>}
            </Table>
          </Col>
        </Row>
      </div>
    );
  }
}
