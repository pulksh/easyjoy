import React from "react";
import Table from "react-bootstrap/Table";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Pageniation } from "../../components/Pageniation";
import { Link } from "react-router-dom";
import MenuLinkService from "../../services/menuLinkService";
import SearchBar from "../../components/searchbar";
import Button from "react-bootstrap/Button";

export default class MenuLinkServiceList extends React.Component {
  constructor(props) {
    super(props);
    this.menuId = this.props.match.params.id;
    this.state = { menulinkList: [], totalCount: 0 };
    this.search = { start: 0, perPage: 10, searchTxt: "", searchField: "" };
    this.menulinkServ = new MenuLinkService();
  }

  componentDidMount() {
    this.getMenuLinkList();
  }

  searchMenuLinkList(searchFilters) {
    this.search.searchTxt = searchFilters.term;
    this.search.searchField = searchFilters.fieldname;
    this.search.start = 0;
    if (!this.search.searchField) {
      alert(" select the fieldname to search");
    } else {
      this.getMenuLinkList();
    }
  }

  getMenuLinkList() {
    let menulink = {};
    switch (this.search.searchField) {
      case "Name":
        this.search.searchField = "name";
        break;
    }
    menulink[this.search.searchField] = this.search.searchTxt;
    menulink.menu_id = this.menuId;
    this.menulinkServ
      .listService(menulink, this.search.start, this.search.perPage)
      .then(
        response => {
          this.setState({
            menulinkList: response.rows,
            totalCount: response.count
          });
        },
        error => {
          this.setState({ menulinkList: [], totalcount: 0 });
        }
      );
  }

  handlePaging(currentPage) {
    this.search.start = currentPage * this.search.perPage;
    this.getMenuLinkList();
  }
  handleDelete(id, e) {
    if (window.confirm("Are you sure you want to delete this record?")) {
        this.menulinkServ.deleteService(id).then(response => {
        this.getMenuLinkList();
      });
    }
  }
  render() {
    let active = Math.ceil(this.search.start / this.search.perPage);
    let data = [];
    for (let i = 0; i < this.state.menulinkList.length; i++) {
      data.push(
        <tr key={this.state.menulinkList[i]._id}>
          <td>{this.search.start + i + 1}</td>
          <td>
            <Link
              to={{
                pathname:
                  "/menulink/edit/" +
                  this.menuId +
                  "/" +
                  this.state.menulinkList[i].id
              }}
            >
              {this.state.menulinkList[i]["item_name"]}
            </Link>
          </td>
          <td>{this.state.menulinkList[i]["link"]}</td>
         {/* <td>{this.state.menulinkList[i]["order_by"]}</td>  */}
          <td>
            <Row>
              <Col sm={2}>
                <Button
                  size="sm"
                  variant="danger"
                  onClick={this.handleDelete.bind(
                    this,
                    this.state.menulinkList[i].id
                  )}
                >
                  <i className="far fa-trash-alt"></i>
                </Button>
              </Col>
            </Row>
          </td>
        </tr>
      );
    }
    return (
      <div className="address addresslist">
        <Row>
          <Col sm={6}>
            <h3>Menu Item List</h3>
          </Col>
          <Col sm={5}>
            <SearchBar
              categories={["Name"]}
              parentFunction={this.searchMenuLinkList.bind(this)}
            />
          </Col>
          <Col sm={1}>
            <div></div>
          </Col>
        </Row>
        <Row>
        <Col sm={9}>
            <Pageniation
                totalRecord={this.state.totalCount}
                currentPage={active}
                perPage={this.search.perPage}
                pageHandler={this.handlePaging.bind(this)}
            />
          </Col>
          <Col sm={3} className="text-right">
            <Link
              to={{
                pathname: "/menulink/add/" + this.menuId
              }}
              className="btn btn-primary"
            >
              Add Menu Link
            </Link>
          </Col>
        </Row>
        <Row>
          <Col sm={12}>
            <Table striped bordered hover size="sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Link</th>
                  {/* <th>Sequence No.</th> */}
                  <th> Action</th>
                </tr>
              </thead>
              {data.length !== 0 ?
              <tbody>{data}</tbody> : <tbody style = {{textAlign : "center"}}><td colSpan = "5"><strong>No Record Found</strong></td></tbody>}
            </Table>
          </Col>
        </Row>
      </div>
    );
  }
}
