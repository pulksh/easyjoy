import React from "react";
import Container from "react-bootstrap/Container";
import MenuService from "../../services/menuService";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Redirect } from "react-router-dom";
import Form from "react-bootstrap/Form";
import { Formik, ErrorMessage } from "formik";
import * as Yup from "yup";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default class Menus extends React.Component {
  constructor(props) {
    super(props);
    this.menu_id = props.match.params.id;
    this.state = { menu: null, redirect: false,menulink:false };
    this.state.menu = {
      name: "",
      link:'',
      location:''
    };

    this.menuServ = new MenuService();
    if (this.menu_id) {
        this.menuServ.getService(this.menu_id).then(

        response => {
          this.setState({ menu: response });
          console.log(response,"response");
        },
        error => {
          alert(
            "Opps! Something went wrong not able to fetch Menu details."
          );
        }
      );
    }
    this.schema = Yup.object({
      name: Yup.string().required("Name is a required field"),
      link: Yup.string().required("link is a required field"),
      location: Yup.string()
    });
  }

  submitMenuForm(values, actions) {
    actions.setSubmitting(false);

    let tx = values;
    this.setState({
        menu: tx
    });

    if (this.menu_id) {
        this.menuServ.editService(this.state.menu).then(
        response => {
          setTimeout(() => {
            this.setState({ redirect: true });
          }, 1000);
          this.setState({ errorMsg: "" });
          toast.success(response);
        },
        error => {
          this.setState({ redirect: false });
        }
      );
    }else{
      this.setState({ menulink: true });
      this.menuServ.addService(this.state.menu).then(
        response => {
            this.setState({ redirect: true });
        },
        error => {
          this.setState({ redirect: false });
        }
      );
    }
  }

  render() {
    if (this.state.redirect === true) {
      return <Redirect to="/menulist" />;
    }

    // if (this.state.menulink === true) {
    //   return <Redirect to="/menulink" />;
    // }

    return (
      <Formik
        validationSchema={this.schema}
        initialValues={this.state.menu}
        enableReinitialize={true}
        onSubmit={this.submitMenuForm.bind(this)}
        render={({
          values,
          errors,
          status,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
          isSubmitting
        }) => (
          <div className="address addresslist">
            <Container>
              <Form onSubmit={handleSubmit}>
                <Row>
                  <Col sm={12}>
                    <h3>Menu Details</h3>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Name*</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.name}
                        name="name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.name && !errors.name}
                      />
                      <ErrorMessage name="name">
                        {msg => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Link*</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.link}
                        name="link"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.link && !errors.link}
                      />
                      <ErrorMessage name="link">
                        {msg => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                </Row>

                <Row>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Location*</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.location}
                        name="location"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.location && !errors.location}
                      />
                      <ErrorMessage name="location">
                        {msg => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                
                </Row>

                <Row>
                  <Col sm={12} md={4}></Col>
                  <Col sm={12} md={4}>
                    <button
                      className="btn btn-lg btn-primary btn-block setbgcolor"
                      type="submit"
                    >
                      Save
                    </button>
                  </Col>
                </Row>
              </Form>
            </Container>
            <ToastContainer />
          </div>
        )}
      />
    );
  }
}