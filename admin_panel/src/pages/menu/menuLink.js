import React from "react";
import Container from "react-bootstrap/Container";
import MenuLinkService from "../../services/menuLinkService";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Redirect } from "react-router-dom";
import Form from "react-bootstrap/Form";
import { Formik, ErrorMessage } from "formik";
import * as Yup from "yup";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const axios = require("axios");

export default class MenuLink extends React.Component {
  constructor(props) {
    super(props);
    this.menu_id = props.match.params.menuId;
    this.menulink_id = props.match.params.menulinkId;
    console.log(this.menulink_id,"jyoti");
    this.state = {
        menulink: {
          id: this.menu_id,
          item_name: "",
          // sequence_no:"",
          link: "",
      },
      
      redirect: false
    };
    
    this.menulinkServ = new MenuLinkService();
    if (this.menulink_id) {
        this.menulinkServ.getService(this.menulink_id).then(
        response => {
          console.log(response,"response");
          this.setState({ menulink: response });
        },
        error => {
          // alert("Opps! Something went wrong not able to fetch data.");
        }
      );
    }
    this.schema = Yup.object({
      item_name: Yup.string().required("Name is a required field"),
      link: Yup.string().required("Link is a required field"),
      //sequence_no: Yup.number().required()
    });
  }

  async submitForm(values, actions) {
    console.log('1111')
    actions.setSubmitting(false);
      console.log(values,'values')
    let tx = values;
    this.setState({
        menulink: tx
    });

    if (this.menulink_id) {
      console.log(this.menulink_id,"manulink_id");
        this.menulinkServ.editService(this.state.menulink).then(
        response => {
          console.log(response,"response")
          setTimeout(() => {
            this.setState({ redirect: true });
          }, 2000);
          this.setState({ errorMsg: "" });
          toast.success(response.result);
        },
        error => {
          this.setState({ redirect: false });
        }
      );
    } else {
        this.menulinkServ
        .addService(this.state.menulink)
        .then(response => {
          this.setState({ redirect: true });
        })
        .catch(err => {
          this.setState({ redirect: false });
        });
    }
  }

  render() {
    if (this.state.redirect === true) {
      return <Redirect to={`/menulinklist/${this.menu_id}`} />;
    }

    return (
      <Formik
        validationSchema={this.schema}
        initialValues={this.state.menulink}
        enableReinitialize={true}
        onSubmit={this.submitForm.bind(this)}
        render={({
          values,
          errors,
          status,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
          isSubmitting,
          setFieldValue
        }) => (
          <div className="address addresslist">
            <Container>
              <Form onSubmit={handleSubmit}>
                <Row>
                  <Col></Col>
                </Row>
                <Row>
                  <Col sm={12}>
                    <h3>Menu Link Details</h3>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Name*</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.item_name}
                        name="item_name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.item_name && !errors.item_name}
                      />
                       <ErrorMessage name="item_name">
                        {msg => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Link*</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.link}
                        name="link"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.link && !errors.link}
                      />
                      <ErrorMessage name="link">
                        {msg => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                </Row>
                {/* <Row>
                <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Sequence No.*</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.sequence_no}
                        name="sequence_no"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.sequence_no && !errors.sequence_no}
                      />
                      <ErrorMessage name="sequence_no">
                          {msg => <div className="err_below">{"Sequence Number must be a number"}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                </Row> */}
                <Row>
                  <Col sm={12} md={4}></Col>
                  <Col sm={12} md={4}>
                    <button
                      className="btn btn-lg btn-primary btn-block setbgcolor"
                      type="submit"
                    >
                      Save
                    </button>
                  </Col>
                </Row>
              </Form>
            </Container>
            <ToastContainer />
          </div>
        )}
      />
    );
  }
}
