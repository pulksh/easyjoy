import { Col, Row, Container, Table, Spinner } from "reactstrap";
import React, { Component } from "react";
import UserService from "../../services/userService";

export default class Agreement extends Component {
  constructor(props) {
    super(props);

    this.state = {
        user: [],
    };
    this.userServ = new UserService();
  }

  componentDidMount() {
    this.userList();
  }

  userList() {
    this.userServ.getUser(window.user && window.user.id).then(
      (response) => {
        this.setState({ user: response });
      },
      (error) => {
        alert("Opps! Something went wrong not able to fetch User details.");
      }
    );
  }

  render() {
    return (
      <div className="container">
        <Row>
          <Col sm={12} md={6}>
          {this.state && this.state.user && this.state.user.agreement? <a href={this.state.user.agreement} target="_blank">Agreement</a> : ""}
          </Col>
        </Row>
      </div>
    );
  }
}
