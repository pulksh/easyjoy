import React from "react";
import Container from "react-bootstrap/Container";
import LocationService from "../../services/locationService";
import { Editor } from "@tinymce/tinymce-react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Redirect } from "react-router-dom";
import Form from "react-bootstrap/Form";
import { Formik, ErrorMessage } from "formik";
import * as Yup from "yup";
import Card from "react-bootstrap/Card";
import CardGroup from "react-bootstrap/CardGroup";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import UserService from "../../services/userService";
const axios = require("axios");
export default class Location extends React.Component {
  constructor(props) {
    super(props);
    this.location_id = props.match.params.id;
    this.state = {
      location: null,
      userList: null,
      redirect: false,
      content: "",
      locationAdditionalImageList: [],
    };
    this.state.location = {
      name: "",
      rating: "",
      description: "",
      latitude: "",
      longitude: "",
      morning_ot: "",
      morning_ct: "",
      evening_ot: "",
      evening_ct: "",
      close_day: 0,
      url: "",
      show_home_page: false,
      is_active: false,
      featured_image: "",
      additional_image: "",

      meta_title: "",
      meta_keyword: "",
      meta_desc: ""
    };

    this.locationServ = new LocationService();
    this.userServ = new UserService();
    if (this.location_id) {
      this.locationServ.getService(this.location_id).then(
        (response) => {
          this.setState({ location: response, content: response.description });
        },
        (error) => {
          alert(
            "Opps! Something went wrong not able to fetch Locationdetails."
          );
        }
      );
    }
    this.schema = Yup.object({
      name: Yup.string().required("Name a required field"),
      rating: Yup.number().required("Rating a required field"),
      // description: Yup.string().required("Description a required field"),
      latitude: Yup.string().required("Latitude a required field"),
      longitude: Yup.string().required("Longitude a required field"),
      featured_image: Yup.string().required(
        "Featured image is a required field"
      ),
    });
  }
  componentDidMount() {
    this.getAdditionalImageList();
    this.userList();
  }
  getAdditionalImageList() {
    this.locationServ.getLocationAdditionalImage(this.location_id).then(
      (response) => {
        this.setState({ locationAdditionalImageList: response });
      },
      (error) => {
        this.setState({ locationAdditionalImageList: [], totalcount: 0 });
      }
    );
  }

  userList() {
    let user = {};
    this.userServ.listUser(user, 0, 10000).then(
      (response) => {
        if (!response) {
          return (response = []);
        }
        this.setState({
          userList: response.rows,
          totalCount: response.count,
          loading: false,
        });
      },
      (error) => {
        this.setState({ userList: [], totalcount: 0 });
      }
    );
  }

  async submitLocationForm(values, actions) {
    actions.setSubmitting(false);
    let tx = values;
    this.setState({
      location: tx,
    });
    const formData = new FormData();
    if (this.state.location.additional_image) {
      for (let prop of Object.keys(this.state.location.additional_image)) {
        formData.append(
          "additional_image",
          this.state.location.additional_image[prop]
        );
      }
    }
    this.state.location.description = this.state.content;
    for (let prop in this.state.location) {
      formData.append(prop, this.state.location[prop]);
    }
    const token = window.user ? window.user.token : "no-token";
    const config = {
      headers: {
        "content-type": "multipart/form-data",
        Authorization: "Bearer " + token,
      },
    };
    if (this.location_id) {
      try {
        const response = await axios.put(
          window.apiurl + "/location",
          formData,
          config
        );
        setTimeout(() => {
          this.setState({ redirect: true });
        }, 2000);
        this.setState({ errorMsg: "" });
        toast.success(response.data);
      } catch (err) {
        toast.error(err.response.data.errors.toString());
        this.setState({ errorMsg: err.response.data, redirect: false });
      }
    } else {
      axios
        .post(window.apiurl + "/location", formData, config)
        .then((res) => {
          this.setState({ redirect: true });
        })
        .catch((err) => {
          toast.error(err);
          this.setState({ errorMsg: err.response.data, redirect: false });
        });
    }
  }

  handleDelete(id, e) {
    if (window.confirm("Are you sure you want to delete this Image?")) {
      this.locationServ.deleteLocationAdditionalImage(id).then((response) => {
        this.getAdditionalImageList();
      });
    }
  }

  handleEditorChange(content) {
    this.setState({ content: content });
  }

  render() {
    let userOption = [];
    for (let i = 0; i < this.state.userList?.length; i++) {
      if (this.state.userList[i].role == "3") {
        userOption.push(
          <option
            key={this.state.userList[i].id}
            value={this.state.userList[i].id}
          >
            {this.state.userList[i].first_name}
          </option>
        );
      }
    }

    if (this.state.redirect === true) {
      return <Redirect to="/locationlist" />;
    }

    let addimgVal = [];
    for (let i = 0; i < this.state.locationAdditionalImageList.length; i++) {
      addimgVal.push(
        <React.Fragment>
          <td>
            <CardGroup style={{ paddingLeft: "10px", marginBottom: "20px" }}>
              <Card>
                <Card.Body style={{ display: "block", blockSize: "125px" }}>
                  <h5
                    style={{
                      paddingLeft: "91px",
                      cursor: "pointer",
                      fontSize: "10px",
                    }}
                    size="sm"
                    onClick={this.handleDelete.bind(
                      this,
                      this.state.locationAdditionalImageList[i].id
                    )}
                  >
                    X
                  </h5>
                  <img
                    src={
                      this.state?.locationAdditionalImageList[i]
                        ?.additional_image
                    }
                    width={"100px"}
                    height={"70px"}
                  />
                </Card.Body>
              </Card>
            </CardGroup>
          </td>
        </React.Fragment>
      );
    }
    return (
      <Formik
        validationSchema={this.schema}
        initialValues={this.state.location}
        enableReinitialize={true}
        onSubmit={this.submitLocationForm.bind(this)}
        render={({
          values,
          errors,
          status,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
          isSubmitting,
          setFieldValue,
        }) => (
          <div className="address addresslist">
            <Container>
              <Form onSubmit={handleSubmit}>
                <Row>
                  <Col sm={12}>
                    <h3>Location Details</h3>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Name*</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.name}
                        name="name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.name && !errors.name}
                      />
                      <ErrorMessage name="name">
                        {(msg) => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Rating*</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.rating}
                        name="rating"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.rating && !errors.rating}
                      />
                      <ErrorMessage name="rating">
                        {(msg) => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Latitude*</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.latitude}
                        name="latitude"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.latitude && !errors.latitude}
                      />
                      <ErrorMessage name="latitude">
                        {(msg) => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Longitude*</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.longitude}
                        name="longitude"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.longitude && !errors.longitude}
                      />
                      <ErrorMessage name="longitude">
                        {(msg) => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Morning Opening Time</Form.Label>
                      <Form.Control
                        type="time"
                        value={values.morning_ot}
                        name="morning_ot"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.morning_ot && !errors.morning_ot}
                      />
                      <ErrorMessage name="morning_ot">
                        {(msg) => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Morning Close Time</Form.Label>
                      <Form.Control
                        type="time"
                        value={values.morning_ct}
                        name="morning_ct"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.morning_ct && !errors.morning_ct}
                      />
                      <ErrorMessage name="morning_ct">
                        {(msg) => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Evening Opening Time</Form.Label>
                      <Form.Control
                        type="time"
                        value={values.evening_ot}
                        name="evening_ot"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.evening_ot && !errors.evening_ot}
                      />
                      <ErrorMessage name="evening_ot">
                        {(msg) => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Evening Close Time</Form.Label>
                      <Form.Control
                        type="time"
                        value={values.evening_ct}
                        name="evening_ct"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.evening_ct && !errors.evening_ct}
                      />
                      <ErrorMessage name="evening_ct">
                        {(msg) => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Form.Label>Close Day</Form.Label>
                    <Form.Control
                      as="select"
                      name="close_day"
                      value={values.close_day}
                      onChange={handleChange}
                      isValid={touched.close_day && !errors.close_day}
                    >
                      <option value="0">No closing day</option>
                      <option value="1">Monday</option>
                      <option value="2">Tuesday</option>
                      <option value="3">Wednesday</option>
                      <option value="4">Thursday</option>
                      <option value="5">Friday</option>
                      <option value="6">Saturday</option>
                      <option value="7">Sunday</option>
                    </Form.Control>
                  </Col>
                  <Col></Col>
                </Row>
                <Row>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Description*</Form.Label>
                      <Editor
                        apiKey={window.tinyAPIKEY}
                        init={{
                          width: 1100,
                          plugins: [
                            "advlist autolink lists link image charmap print preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime media table paste code help wordcount",
                          ],
                          toolbar:
                            "undo redo | formatselect | bold italic backcolor | \
                                      alignleft aligncenter alignright alignjustify | \
                                      bullist numlist outdent indent | removeformat | help",
                        }}
                        value={this.state.content}
                        onEditorChange={this.handleEditorChange.bind(this)}
                      />
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Location URL</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.url}
                        name="url"
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Select Owner*</Form.Label>
                      <Form.Control
                        as="select"
                        name="user_id"
                        value={values.user_id}
                        onChange={(e) => {
                          handleChange(e);
                          this.setState({ user_id: e.target.value });
                        }}
                        isValid={touched.user_id && !errors.user_id}
                      >
                        <option></option>
                        {userOption}
                      </Form.Control>
                      <ErrorMessage name="location_id">
                        {(msg) => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Meta Keyword</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.meta_keyword}
                        name="meta_keyword"
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Meta Title</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.meta_title}
                        name="meta_title"
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Meta Description</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.meta_desc}
                        name="meta_desc"
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={6}>
                    <div>
                      <strong>Display on homepage?</strong>
                    </div>
                    <Form.Group controlId="form21">
                      <Form.Check
                        type="checkbox"
                        label="Yes"
                        checked={values.show_home_page}
                        value={values.show_home_page}
                        onChange={handleChange}
                        name="show_home_page"
                        onBlur={handleBlur}
                        isValid={
                          touched.show_home_page && !errors.show_home_page
                        }
                      />
                      <ErrorMessage name="show_home_page">
                        {(msg) => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={6}>
                    <div>
                      <strong>Is Active</strong>
                    </div>
                    <Form.Group controlId="form21">
                      <Form.Check
                        type="checkbox"
                        label="Yes"
                        checked={values.is_active}
                        value={values.is_active}
                        onChange={handleChange}
                        name="is_active"
                      />
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={12}>
                    <Form.Group>
                      <p>Featured Image Upload*</p>
                      <input
                        type="file"
                        name="featured_image"
                        onChange={(event) => {
                          setFieldValue(
                            "featured_image",
                            event.currentTarget.files[0]
                          );
                        }}
                      />
                      <ErrorMessage name="featured_image">
                        {(msg) => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                    {this.location_id ? (
                      <img
                        src={values.featured_image}
                        width={"100px"}
                        height={"auto"}
                      />
                    ) : (
                      " "
                    )}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={12}>
                    <Form.Group>
                      <p>Additional Image Upload</p>
                      <input
                        type="file"
                        name="additional_image"
                        onChange={(event) => {
                          setFieldValue(
                            "additional_image",
                            event.currentTarget.files
                          );
                        }}
                        multiple="multiple"
                      />
                    </Form.Group>
                    {addimgVal}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4}></Col>
                  <Col sm={12} md={4}>
                    <button
                      className="btn btn-lg btn-primary btn-block setbgcolor"
                      type="submit"
                    >
                      Save
                    </button>
                  </Col>
                </Row>
              </Form>
            </Container>
            <ToastContainer />
          </div>
        )}
      />
    );
  }
}
