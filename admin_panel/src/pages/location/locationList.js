import React from "react";
import Table from "react-bootstrap/Table";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Pagination from "react-bootstrap/Pagination";
import { Link } from "react-router-dom";
import Location from "../../services/locationService";
import SearchBar from "../../components/searchbar";
import Button from "react-bootstrap/Button";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Helmet } from 'react-helmet';
export default class LocationList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { locationList: [], totalCount: 0 };
    this.search = { start: 0, perPage: 10, searchTxt: "", searchField: "" };
    this.locationServ = new Location();
  }

  componentDidMount() {
    this.getLocationList();
  }

  searchLocationList(searchFilters) {
    this.search.searchTxt = searchFilters.term;
    this.search.searchField = searchFilters.fieldname;
    this.search.start = 0;
    this.getLocationList();
  }

  getLocationList() {
    let location = {};
    switch (this.search.searchField) {
      case "name":
        this.search.searchField = "name";
        break;
      case "rating":
        this.search.searchField = "rating";
        break;
      case "description":
        this.search.searchField = "description";
        break;
    }

    location[this.search.searchField] = this.search.searchTxt;
    this.locationServ
      .listService(location, this.search.start, this.search.perPage)
      .then(
        (response) => {
          this.setState({
            locationList: response.rows,
            totalCount: response.count,
          });
        },
        (error) => {
          this.setState({ locationList: [], totalcount: 0 });
        }
      );
  }

  handlePaging(e) {
    if (e.target.text) {
      this.search.start =
        parseInt(e.target.text) * this.search.perPage - this.search.perPage;
      this.getLocationList();
    }
  }
  handleDelete(id, e) {
    if (window.confirm("Are you sure you want to delete this record?")) {
      this.locationServ.deleteService(id).then((response) => {
        if(response.error){
          toast.error(response.error);
        } else {
          toast.success("Record deleted successfully");
          this.getLocationList();
        }
      });              
    }
  }

  render() {

    // let location = this.state.locationList;
    // <Helmet>
    //   {/* <title>{page.name}</title> */}
    //   <meta name="keywords" content={location.meta_keywords} />
    //   <meta name="description" content={location.meta_description} />
    // </Helmet>; 

    let active = Math.ceil((this.search.start + 1) / this.search.perPage);
    let pages = Math.ceil(this.state.totalCount / this.search.perPage);
    let items = [];
    for (let number = 1; number <= pages; number++) {
      items.push(
        <Pagination.Item
          key={number}
          onClick={this.handlePaging.bind(this)}
          active={number === active}
        >
          {number}
        </Pagination.Item>
      );
    }
    let data = [];
    for (let i = 0; i < this.state.locationList.length; i++) {
      data.push(
        <tr key={this.state.locationList[i].id}>
          <td>{this.search.start + i + 1}</td>
          <td>
            <Link
              to={{
                pathname: "/location/edit/" + this.state.locationList[i].id,
              }}
            >
              {this.state.locationList[i]["name"]}
            </Link>
          </td>
          <td>{this.state.locationList[i]["rating"]?this.state.locationList[i]["rating"]:""}</td>
          <td>{this.state.locationList[i]["show_home_page"]?"Yes":"No"}</td>
          <td>{this.state.locationList[i]["is_active"]?"Yes":"No" }</td>
          <td>{this.state.locationList[i]["featured_image"] && <img src={this.state.locationList[i]["featured_image"]} width={"80px"} height={"auto"} alt="Location thumbnail" />}</td>
          <td>
            <Button
              size="sm"
              variant="danger"
              onClick={this.handleDelete.bind(
                this,
                this.state.locationList[i].id
              )}
            >
              <i className="far fa-trash-alt"></i>
            </Button>
          </td>
        </tr>
      );
    }
    return (
      <div className="address addresslist">
        <Row>
          <Col sm={6}>
            <h3>Location List</h3>
          </Col>
          {/* <Col sm={5}>
            <SearchBar
              categories={["name", "rating","description","latitude","longitude"]}
              parentFunction={this.searchLocationList.bind(this)}
            />
          </Col> */}
          <Col sm={1}>
            <div></div>
          </Col>
        </Row>
        <Row>
          <Col sm={9}>
            <Pagination size="md">{items}</Pagination>
          </Col>
          <Col sm={3} className="text-right">
            <Link
              to={{ pathname: "/location/add" }}
              className="btn btn-primary"
            >
              Add Location
            </Link>
          </Col>
        </Row>
        <Row>
          <Col sm={12}>
            <Table striped bordered hover size="sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Ratings</th>
                  <th>Show on Home</th>
                  <th>Is Active</th>
                  <th>Featured Image</th>
                  <th>Actions</th>
                </tr>
              </thead>
              {data.length !== 0 ? (
                <tbody>{data}</tbody>
              ) : (
                <tbody style={{ textAlign: "center" }}>
                  <td colSpan="4">
                    <strong>No Record Found</strong>
                  </td>
                </tbody>
              )}
            </Table>
          </Col>
        </Row>
        <ToastContainer />
      </div>
    );
  }
}
