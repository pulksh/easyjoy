import React from "react";
import Form from "react-bootstrap/Form";
import { Formik, ErrorMessage } from "formik";
import { Container, Nav, Navbar } from "react-bootstrap";
// import Table from "react-bootstrap/Table";
import { Col, Row, Table, Spinner } from "reactstrap";
import { Link } from "react-router-dom";
import UserService from "../../services/userService";
import SearchBar from "../../components/searchbar";
import Button from "react-bootstrap/Button";
import { Pageniation } from "../../components/Pageniation";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
export default class userList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userList: [],
      totalCount: 0,
      loading: false,
    };
    this.search = {
      start: 0,
      perPage: 10,
      searchTxt: "",
      searchField: "",
    };
    this.userServ = new UserService();
  }

  componentDidMount() {
    this.getUserList();
  }

  searchUserList(searchFilters) {
    this.search.searchTxt = searchFilters.term;
    this.search.searchField = searchFilters.fieldname;
    this.search.start = 0;
    if (this.search.searchField) this.getUserList();
  }

  getUserList() {
    let user = {};
    switch (this.search.searchField) {
      case "title":
        this.search.searchField = "title";
        break;
      case "First Name":
        this.search.searchField = "first_name";
        break;
      case "Last Name":
        this.search.searchField = "last_name";
        break;
      case "Email":
        this.search.searchField = "email";
        break;
      case "Phone No":
        this.search.searchField = "phone_no";
        break;
    }
    user[this.search.searchField] = this.search.searchTxt;
    this.setState({ loading: true });
    this.userServ.listUser(user, this.search.start, this.search.perPage).then(
      (response) => {
        if (!response) {
          return (response = []);
        }
        this.setState({
          userList: response.rows,
          totalCount: response.count,
          loading: false,
        });
      },
      (error) => {
        this.setState({ userList: [], totalcount: 0 });
      }
    );
  }

  handlePaging(currentPage) {
    this.search.start = currentPage * this.search.perPage;
    this.getUserList();
  }

  handleDelete(id, e) {
    if (window.confirm("Are you sure you want to delete this record?")) {
      this.userServ.deleteUser(id).then((response) => {
        if(response.error){
          toast.error(response.error);
        } else {
          toast.success("Record deleted successfully");
          this.getUserList();
        }
      });
    }
  }

  sendMail = async (ownerId) => {
    let user = await this.userServ.sendMailToAdmin(ownerId);
    if(user.result === true){
      toast.success(user.message);
    }
    else{
      toast.error("Send mail failed.");
    }
  }
  render() {
    let active = Math.ceil(this.search.start / this.search.perPage);
    let data = [];
    for (let i = 0; i < this.state.userList.length; i++) {
      data.push(
        <tr key={this.state.userList[i].id}>
          <td>{this.search.start + i + 1}</td>
          <td> {this.state.userList[i]["title"]}</td>
          <td>
            <Link to={{ pathname: "/user/edit/" + this.state.userList[i].id }}>
              {this.state.userList[i]["first_name"] ? this.state.userList[i]["first_name"] : ""}
            </Link>
          </td>
          <td>{this.state.userList[i]["email"] ? this.state.userList[i]["email"] : ""}</td>
          <td>{this.state.userList[i]["phone"] ? this.state.userList[i]["phone"] : ""}</td>

          <td>{this.state.userList[i]["role"] == 1 ? "Admin" : this.state.userList[i]["role"] == 2 ? "User" : "Owner"}</td>
          {/* <td>{this.state.userList[i]["role"] == 3 ? <Button size="sm" variant="warning" onClick={this.sendMail.bind(this,this.state.userList[i].id)}> Send Mail </Button>: ""}</td> */}
          <td>
            <Row>
              <Col sm={2}>
                <Button
                  size="sm"
                  variant="danger"
                  onClick={this.handleDelete.bind(
                    this,
                    this.state.userList[i].id
                  )}
                >
                  <i className="far fa-trash-alt"></i>
                </Button>
              </Col>
            </Row>
          </td>
        </tr>
      );
    }
    return (
      <div className="address addresslist">
        <Row>
          <Col sm={6}>
            <h3>User List</h3>
          </Col>
          {/* <Col sm={5}>
            <SearchBar
              categories={["First Name", "Email"]}
              parentFunction={this.searchUserList.bind(this)}
            />
          </Col> */}
          <Col sm={1}>
            <div></div>
          </Col>
        </Row>
        {this.state.loading ? (
          <div style={{ textAlign: "center" }}>
            {" "}
            <Spinner style={{ width: "3rem", height: "3rem" }} />
          </div>
        ) : (
          <React.Fragment>
            <Row>
              <Col sm={9}>
                <Pageniation
                  totalRecord={this.state.totalCount}
                  currentPage={active}
                  perPage={this.search.perPage}
                  pageHandler={this.handlePaging.bind(this)}
                />
              </Col>
              <Col sm={3} className="text-right">
                <Link
                  to={{ pathname: "/user/add" }}
                  className="btn btn-primary"
                >
                  Add User
                </Link>
              </Col>
            </Row>
            <Row>
              <Col sm={12}>
                <Table striped bordered hover size="sm">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Title</th>
                      <th>User Name</th>
                      <th>Email</th>
                      <th>Phone</th>
                      <th>Role</th>
                      {/* <th>Activate Owner</th> */}
                      <th>Actions</th>
                    </tr>
                  </thead>
                  {data.length !== 0 ? (
                    <tbody>{data}</tbody>
                  ) : (
                    <tbody style={{ textAlign: "center" }}>
                      <td colSpan="5">
                        <strong>No Record Found</strong>
                      </td>
                    </tbody>
                  )}
                </Table>
              </Col>
            </Row>
          </React.Fragment>
        )}
        <ToastContainer />
      </div>
    );
  }
}
