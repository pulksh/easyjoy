import React from "react";
import { Container, Nav, Navbar, Button, Toast } from "react-bootstrap";
import UserService from "../../services/userService";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Redirect } from "react-router-dom";
import Form from "react-bootstrap/Form";
import { Formik, ErrorMessage } from "formik";
import { Link } from "react-router-dom";
import * as Yup from "yup";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const axios = require("axios");
export default class User extends React.Component {
  constructor(props) {
    super(props);
    this.user_id = props.match.params.id;
    this.state = {
      user: null,
      redirect: false,
      errorMsg: "",
    };

    this.state.user = {
      title: "Mr",
      first_name: "",
      last_name: "",
      email: "",
      phone: "",
      role: "",
      password: "",
      is_active: true,
      agreement: '',
    };
    this.userServ = new UserService();
    if (this.user_id) {
      this.userServ.getUser(this.user_id).then(
        (response) => {
          this.setState({ user: response });
        },
        (error) => {
          alert("Opps! Something went wrong not able to fetch User details.");
        }
      );
    }
    const phoneNumberRegex = RegExp(
      /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/
    );

    this.schema = Yup.object({
      title: Yup.string(),
      first_name: Yup.string()
        .required("First Name is a required field")
        .min(2, 'Too Short!')
        .max(100, "Too Long!"),
      email: Yup.string()
        .required("Email is a required field").nullable()
        .email("Invalid Email"),

      phone: Yup.string()
        .matches(phoneNumberRegex, "Invalid phone number")
        .required("Phone Number is a required field").nullable(),
      // role: Yup.number().required("Role is a required field"),
      password: Yup.string(),
      // is_active: Yup.bool().required("Is Active is a required field"),
    });
  }
  async submitUserForm(values, actions) {
    actions.setSubmitting(false);
    if (values.role == null || values.role == "") {
      values.role = 2;
    }

    let tx = values;
    this.setState({
      user: tx,
    });

    const formData = new FormData();
    for (let prop in this.state.user) {
      formData.append(prop, this.state.user[prop]);
    }
    
    const token = window.user ? window.user.token : "no-token";
    const config = {
      headers: {
        "content-type": "multipart/form-data",
        Authorization: "Bearer " + token,
      },
    };

    if (this.user_id) {
      
      // this.userServ.editService(this.state?.user).then(
      //   (response) => {
      //     if(response.message){
      //       setTimeout(() => {
      //         toast.success(response.message);
      //         this.setState({ redirect: true });
      //       }, 2000);
      //       this.setState({ errorMsg: "" });
      //     } else {
      //       toast.error(response.error);
      //       this.setState({ redirect: false });
      //     }
          
      //   },
      //   (error) => {
      //     toast.error(error);
      //     console.log(error,'errorserrors')
      //     this.setState({ redirect: false });
      //   }
      // );
    
      try {
        const response = await axios.put(
          window.apiurl + "/user",
          formData,
          config
        );
        setTimeout(() => {
          this.setState({ redirect: true });
        }, 2000);
        this.setState({ errorMsg: "" });
        toast.success(response.message);
      } catch (err) {
        toast.error(err.response.data.errors.toString());
        this.setState({ errorMsg: err.response.data, redirect: false });
      }
    } else {
      // this.userServ
      //   .addService(this.state?.user)
      //   .then((response) => {
      //     if(response.message){
      //       setTimeout(() => {
      //         toast.success(response.message);
      //         this.setState({ redirect: true });
      //       }, 2000);
      //       this.setState({ errorMsg: "" });
      //     } else {
      //       toast.error(response.error);
      //       this.setState({ redirect: false });
      //     }
      //   })
      //   .catch((err) => {
      //     let error = JSON.parse(err.errors)
      //     // toast.error(err.errors.toString());
      //     console.log(error,'errorserrors errerr')
      //     this.setState({ redirect: false });
      //   });

      axios
        .post(window.apiurl + "/user", formData, config)
        .then((res) => {
          toast.success(res.message);
          this.setState({ redirect: true });
        })
        .catch((err) => {
          toast.error(err);
          this.setState({ errorMsg: err.response.data, redirect: false });
        });
    }
  }

  render() {
    if (this.state.redirect === true) {
      return <Redirect to="/userlist" />;
    }
    return (
      <Formik
        validationSchema={this.schema}
        initialValues={this.state.user}
        enableReinitialize={true}
        onSubmit={this.submitUserForm.bind(this)}
        render={({
          values,
          errors,
          status,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
          isSubmitting,
          setFieldValue,
        }) => (
          <div className="address addresslist">
            <Container>
              <Form onSubmit={handleSubmit}>
                <Row>
                  <Col sm={3}>
                    <h3>User Details</h3>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Form.Label>Title*</Form.Label>
                    <Form.Control
                      as="select"
                      name="title"
                      value={values.title}
                      onChange={handleChange}
                      isValid={touched.title && !errors.title}
                    >
                      <option value="Dr">Dr</option>
                      <option value="Mr">Mr</option>
                      <option value="Mrs">Mrs</option>
                      <option value="Miss">Miss</option>
                      <option value="Ms">Ms</option>
                    </Form.Control>
                  </Col>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Email*</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.email}
                        name="email"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.email && !errors.email}
                      />
                      <ErrorMessage name="email">
                        {(msg) => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                      {this.state.errorMsg === `Email already registerd` ? (
                        <div className="errormsg">{this.state.errorMsg}</div>
                      ) : (
                        " "
                      )}
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>User Name*</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.first_name}
                        name="first_name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.first_name && !errors.first_name}
                      />
                      <ErrorMessage name="first_name">
                        {(msg) => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                  {/* <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Last Name</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.last_name}
                        name="last_name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.last_name && !errors.last_name}
                      />
                      <ErrorMessage name="last_name">
                        {(msg) => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col> */}
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Phone*</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.phone}
                        name="phone"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.phone && !errors.phone}
                      />
                      <ErrorMessage name="phone">
                        {(msg) => (
                          <div className="err_below">
                            {"enter a valid Phone Number"}
                          </div>
                        )}
                      </ErrorMessage>
                      {this.state.errorMsg ===
                      `Mobile Number already registerd` ? (
                        <div className="errormsg">{this.state.errorMsg}</div>
                      ) : (
                        " "
                      )}
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Role</Form.Label>
                      <Form.Control
                        as="select"
                        name="role"
                        value={values.role}
                        onChange={handleChange}
                        isValid={touched.role && !errors.role}
                      >
                        <option value="2">User</option>
                        <option value="1">Admin</option>
                        <option value="3">Owner</option>
                      </Form.Control>
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Password*</Form.Label>
                      <Form.Control
                        type="password"
                        name="password"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.password && !errors.password}
                      />
                    </Form.Group>
                    <ErrorMessage name="password">
                      {(msg) => <div className="err_below">{msg}</div>}
                    </ErrorMessage>
                    {`"password" is not allowed to be empty` ===
                    this.state.errorMsg ? (
                      <div className="errormsg">
                        {"Password is a required field"}
                      </div>
                    ) : (
                      " "
                    )}
                  </Col>
                </Row>
                <Row>
                <Col sm={12} md={6}>
                  <Form.Group>
                    <Form.Label>Is Active</Form.Label>
                    <Form.Control
                      as="select"
                      name="is_active"
                      value={values.is_active}
                      onChange={handleChange}
                      isValid={touched.is_active && !errors.is_active}
                    >
                      <option value="true">Active</option>
                      <option value="false">In Active</option>
                    </Form.Control>
                  </Form.Group>
                  <ErrorMessage name="is_active">
                    {(msg) => <div className="err_below">{msg}</div>}
                  </ErrorMessage>
                  </Col>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <p> File</p>
                      <input
                        type="file"
                        name="agreement"
                        onChange={(event) => {
                          setFieldValue(
                            "agreement",
                            event.currentTarget.files[0]
                          );
                        }}
                      />
                      {this.user_id && values.agreement ? <a href={values.agreement} target="_blank">Agreement</a> : ""}
                      {/* <ErrorMessage name="agreement">
                        {(msg) => <div className="err_below">{msg}</div>}
                      </ErrorMessage> */}
                    </Form.Group>
                    </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4}></Col>
                  <Col sm={12} md={4}>
                    <button
                      className="btn btn-lg btn-primary btn-block setbgcolor"
                      type="submit"
                    >
                      Save
                    </button>
                  </Col>
                </Row>
              </Form>
              <ToastContainer />
            </Container>
          </div>
        )}
      />
    );
  }
}
