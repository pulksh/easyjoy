import React, { Component } from "react";
import { Col, Row, Table, Spinner } from "reactstrap";
import { Link } from "react-router-dom";
import CheckinoutService from "../../services/checkinoutService";
import SearchBar from "../../components/searchbar";
import Button from "react-bootstrap/Button";
import { Pageniation } from "../../components/Pageniation";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import moment from "moment";
import { CSVLink, CSVDownload } from "react-csv";

export default class CheckinoutListOwner extends Component {
  constructor(props) {
    super(props);

    this.state = {
      code: "",
      totalCount: 0,
      checkinoutList: [],
      csvResultList: [],
    };
    this.search = {
      start: 0,
      perPage: 10,
      searchTxt: "",
      searchField: "",
    };
    this.checkinoutServ = new CheckinoutService();
  }

  componentDidMount() {
    this.getCheckinoutList();
    this.getCsvData();
  }
  searchUserList(searchFilters) {
    this.search.searchTxt = searchFilters.term;
    this.search.searchField = searchFilters.fieldname;
    this.search.start = 0;
    if (this.search.searchField) this.getCheckinoutList();
  }
  getCsvData() {
    let checkinout = { "": "" };
    this.checkinoutServ.getCsvData(checkinout, 0, 100000).then(
      (response) => {
        console.log(response);
        this.setState({ csvResultList: response.data });
      },
      (error) => {
        this.setState({ csvResultList: [] });
      }
    );
  }

  getCheckinoutList() {
    // let checkinout = { "": "" };
    let checkinout = {};
    // checkinout[this.search.searchField] = this.search.searchTxt;
    this.checkinoutServ.listService(checkinout, 0, 1000).then(
      (response) => {
        this.setState({
          checkinoutList: response.rows,
          totalCount: response.count,
        });
      },
      (error) => {
        this.setState({ checkinoutList: [], totalcount: 0 });
      }
    );
  }

  handleChange = (event) => {
    this.setState({ code: event.target.value });
  };

  handleVerifyCode = (e) => {
    e.preventDefault();
    try {
      let code = document.getElementById("verifycode").value;
      const vcode = this.validateform(code);
      if (vcode == false) {
        return;
      } else {
        const vryCode = this.state?.code;
        let obj = {
          code: vryCode,
        };
        const result = this.checkinoutServ.getVerifyCode(obj).then(
          (response) => {
            if(response.error){
              toast.warn(response.error);
            }
            toast.success(response.message);
            this.getCheckinoutList();
          },
          (error) => {
            toast.error("Oops! Code not varified.Try again");
            this.setState({ code: "", totalcount: 0 });
          }
        );
      }
    } catch (err) {
      toast.error("Code not varified");
      this.setState({ errorMsg: err.response, redirect: false });
    }
  };

  validateform = (verifyCode) => {
    this.setState({ errMsg: "" });
    if (verifyCode == null || verifyCode == "") {
      const errMsg = "Please enter verify code";
      toast.error(errMsg);
      return false;
    }
  };
  handlePaging(currentPage) {
    this.search.start = currentPage * this.search.perPage;
    this.getCheckinoutList();
  }

  render() {
    let active = Math.ceil(this.search.start / this.search.perPage);
    let data = [];
    for (let i = 0; i < this.state.checkinoutList.length; i++) {
      data.push(
        <tr key={this.state.checkinoutList[i].id}>
          <td>{i + 1}</td>
          <td>
            {this.state.checkinoutList[i]["user"]?.first_name}
          </td>
          <td>{this.state.checkinoutList[i]["user"]?.email}</td>
          <td>{this.state.checkinoutList[i]["location"]?.name}</td>
          <td>
            {moment(this.state.checkinoutList[i].checkin_time).format(
              "YYYY-MM-DD hh:mm A"
            )}
          </td>
          <td>
            {this.state.checkinoutList[i].checkout_time
              ? moment(this.state.checkinoutList[i].checkout_time).format(
                  "YYYY-MM-DD hh:mm A"
                )
              : ""}
          </td>
          <td>{this.state.checkinoutList[i].verified ? "YES" : "NO"}</td>
          <td>
            {this.state.checkinoutList[i].verification_time
              ? moment(this.state.checkinoutList[i].verification_time).format(
                  "YYYY-MM-DD hh:mm A"
                )
              : ""}
          </td>
        </tr>
      );
    }
    return (
      <div className="address addresslist">
        <Row>
          <Col sm={6}>
            <h3>Check-in-out Details</h3>
          </Col>
          
        </Row>
        <Row>
          <Col sm={12} md={4}>
            <Pageniation
              totalRecord={this.state.totalCount}
              currentPage={active}
              perPage={this.search.perPage}
              pageHandler={this.handlePaging.bind(this)}
            />
          </Col>
          <Col sm={12} md={6} className="text-right">
            <form class="form-inline">
              <div class="form-group mx-sm-3 mb-2">
                <input
                  type="text"
                  class="form-control"
                  name="verifyCode"
                  id="verifycode"
                  value={this.state?.code}
                  onChange={this.handleChange}
                  placeholder="Please enter your code"
                />
              </div>
              <button
                type="submit"
                class="btn btn-primary mb-2"
                onClick={this.handleVerifyCode}
              >
                Verify Code
              </button>
            </form>
          </Col>
        </Row>

        <Row>
          {/* <Col sm={5}>
                <SearchBar
                  categories={["First Name", "Email"]}
                  parentFunction={this.searchUserList.bind(this)}
                />
              </Col> */}
          {/* <Col sm={4} className="text-right">
          <CSVLink className="btn btn-primary"  data={this.state.csvResultList} ><i class="fa fa-file-excel-o"></i> &nbsp;Download Csv</CSVLink>
          </Col> */}
        </Row>
        {this.state.loading ? (
          <div style={{ textAlign: "center" }}>
            {" "}
            <Spinner style={{ width: "3rem", height: "3rem" }} />
          </div>
        ) : (
          <React.Fragment>
            <Row></Row>
            <Row>
              <Col sm={12}>
                <Table striped bordered hover size="sm">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>User Name</th>
                      <th>Email</th>
                      <th>Location</th>
                      <th>Checkin Time</th>
                      <th>Checkout Time</th>
                      <th>Is verified</th>
                      <th>Verification Time</th>
                    </tr>
                  </thead>
                  {data.length !== 0 ? (
                    <tbody>{data}</tbody>
                  ) : (
                    <tbody style={{ textAlign: "center" }}>
                      <td colSpan="5">
                        <strong>No Record Found</strong>
                      </td>
                    </tbody>
                  )}
                </Table>
              </Col>
            </Row>
          </React.Fragment>
        )}
        <ToastContainer />
      </div>
    );
  }
}
