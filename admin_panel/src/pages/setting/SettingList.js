import React from "react";
import Table from "react-bootstrap/Table";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Pageniation } from "../../components/Pageniation";
import { Link } from "react-router-dom";
import Setting from "../../services/settingService";
import SearchBar from "../../components/searchbar";
import Button from "react-bootstrap/Button";

export default class SettingList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { settingList: [], totalCount: 0 };
    this.search = { start: 0, perPage: 10, searchTxt: "", searchField: "" };
    this.settingServ = new Setting();
  }

  componentDidMount() {
    this.getSettingList();
  }

  searchSettingList(searchFilters) {
    this.search.searchTxt = searchFilters.term;
    this.search.searchField = searchFilters.fieldname;
    this.search.start = 0;
    if (!this.search.searchField) {
      alert(" select the fieldname to search");
    } else {
      this.getSettingList();
    }
  }

  getSettingList() {
    let setting = {};
    switch (this.search.searchField) {
      case " Name":
        this.search.searchField = "name";
        break;
      case "Value":
        this.search.searchField = "value";
        break;
    }
    setting[this.search.searchField] = this.search.searchTxt;
    this.settingServ
      .listService(setting, this.search.start, this.search.perPage)
      .then(
        response => {
          this.setState({
            settingList: response.rows,
            totalCount: response.count
          });
        },
        error => {
          this.setState({ settingList: [], totalcount: 0 });
        }
      );
  }

  handlePaging(currentPage) {
    this.search.start = currentPage * this.search.perPage;
    this.getSettingList();
  }
  handleDelete(id, e) {
    if (window.confirm("Are you sure you want to delete this record?")) {
      this.settingServ.deleteService(id).then(response => {
        this.getSettingList();
      });
    }
  }

  render() {
    let active = Math.ceil(this.search.start / this.search.perPage);
    let data = [];
    for (let i = 0; i < this.state.settingList.length; i++) {
      data.push(
        <tr key={this.state.settingList[i].id}>
          <td>{this.search.start + i + 1}</td>
          <td>
            <Link
              to={{
                pathname: "/setting/edit/" + this.state.settingList[i].id
              }}
            >
              {this.state.settingList[i]["name"]}
            </Link>
          </td>
          <td dangerouslySetInnerHTML={{ __html: this.state.settingList[i]["value"]}}></td>
        </tr>
      );
    }
    return (
      <div className="address addresslist">
        <Row>
          <Col sm={6}>
            <h3>Setting List</h3>
          </Col>
          {/* <Col sm={5}>
            <SearchBar
              categories={["Name", "Value"]}
              parentFunction={this.searchSettingList.bind(this)}
            />
          </Col> */}
          <Col sm={1}>
            <div></div>
          </Col>
        </Row>
        <Row>
        <Col sm={9}>
            <Pageniation
                totalRecord={this.state.totalCount}
                currentPage={active}
                perPage={this.search.perPage}
                pageHandler={this.handlePaging.bind(this)}
            />
          </Col>
        </Row>
        <Row>
          <Col sm={12}>
            <Table striped bordered hover size="sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Value</th>
                </tr>
              </thead>
              {data.length !== 0 ?
                <tbody>{data}</tbody> : <tbody style = {{textAlign : "center"}}><td colSpan = "3"><strong>No Record Found</strong></td></tbody>}
            </Table>
          </Col>
        </Row>
      </div>
    );
  }
}
