import React from "react";
import Container from "react-bootstrap/Container";
import SettingService from "../../services/settingService";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Redirect } from "react-router-dom";
import Form from "react-bootstrap/Form";
import { Formik, ErrorMessage } from "formik";
import * as Yup from "yup";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Editor } from "@tinymce/tinymce-react";
const axios = require("axios");

export default class Setting extends React.Component {
  constructor(props) {
    super(props);
    this.setting_id = props.match.params.id;
    this.state = { setting: null, redirect: false,flag:false,content: ''};
    this.state.setting = {
      name: "",
      value: "",
      content_type : "",
      is_editable:""
    };
    this.handleEditorChange = this.handleEditorChange.bind(this);
    this.settingServ = new SettingService();
    if (this.setting_id) {
      this.settingServ.getService(this.setting_id).then(
        response => {
          if (!response) {
            return (response = []);
          }
          this.setState({ setting: response,content:response.value});
        },
        error => {
          alert(
            "Opps! Something went wrong not able to fetch Setting  details."
          );
        }
      );
    }
    this.schema = Yup.object({
      name: Yup.string()
        .required()
        .max(100, "Too Long"),
      // value: Yup.string().required()
    });
  }

  handleEditorChange(content) {
    this.setState({ content: content });
  }

  async submitSettingForm(values, actions) {
    actions.setSubmitting(false);

    let tx = values;
    this.setState({
      setting: tx
    });
    if(this.state.setting.content_type === 2){  
     this.state.setting.value = this.state.content;
    }
    const formData = new FormData();
    for (let key in this.state.setting) {
      formData.append(key, this.state.setting[key]);
    }
    const token = window.user ? window.user.token : "no-token";
    const config = {
      headers: {
        "content-type": "multipart/form-data",
        Authorization: "Bearer " + token
      }
    };

    if (this.setting_id) {
      try {
        const response = await axios.put(
          window.apiurl + "/setting",
          formData,
          config
        );
        this.setState({ redirect: true,errorMsg: "" });
        toast.success(response);
      } catch (err) {
         this.setState({ redirect: false });
      }
    } else {
      axios
        .post(window.apiurl + "/setting", formData, config)
        .then(res => {
          this.setState({ redirect: true });
        })
        .catch(err => {
          // toast.error(err);
          this.setState({ redirect: false });
        });
    }
  }

  render() {
    if (this.state.redirect === true) {
      return <Redirect to="/settinglist" />;
    }
    return (
      <Formik
        validationSchema={this.schema}
        initialValues={this.state.setting}
        enableReinitialize={true}
        onSubmit={this.submitSettingForm.bind(this)}
        render={({
          values,
          errors,
          status,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
          isSubmitting,
          setFieldValue
        }) => (
          <div className="address addresslist">
            <Container>
              <Form onSubmit={handleSubmit}>
                <Row>
                </Row>
                <Row>
                  <Col sm={12}>
                    <h3>Setting Details</h3>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={6}>
                    {this.setting_id && values.is_editable === true ?
                       <Form.Group>
                       <Form.Label>Name*</Form.Label>
                       <Form.Control
                         type="text"
                         value={values.name}
                         name="name"
                         onChange={handleChange}
                         onBlur={handleBlur}
                         isValid={touched.name && !errors.name}
                       />
                       <ErrorMessage name="name">
                         {msg => <div className="err_below">{msg}</div>}
                       </ErrorMessage>
                     </Form.Group> :
                     <Form.Group>
                     <Form.Label>Name*</Form.Label>
                     <Form.Control
                       type="text"
                       value={values.name}
                       name="name"
                       onChange={handleChange}
                       onBlur={handleBlur}
                       isValid={touched.name && !errors.name}
                       disabled = {true}
                     />
                     <ErrorMessage name="name">
                       {msg => <div className="err_below">{msg}</div>}
                     </ErrorMessage>
                   </Form.Group>
                  }
                  </Col>
                </Row>
                
                {this.setting_id && values.is_editable === true ?
                <Row>
                  {this.state.setting.content_type === 1 ?
                  <Col sm={12} md={6}>
                  <Form.Group>
                    <p>Logo*</p>
                    <input
                      type="file"
                      name="value"
                      onChange={event => {
                        setFieldValue(
                          "value",
                          event.currentTarget.files[0]
                        );
                      }}
                    />
                    <img src={values.value} width={"100px"} height={"auto"} />
                  </Form.Group>
                </Col> : this.state.setting.content_type === 2 ?
                  <Col sm={12} md={6}>
                  <Form.Group>
                      <Form.Label>Content*</Form.Label>
                      <Editor 
                      apiKey={window.tinyAPIKEY}
                      init={{
                                  width: 1100,
                                  plugins: [
                                  'advlist autolink lists link image charmap print preview anchor',
                                  'searchreplace visualblocks code fullscreen',
                                  'insertdatetime media table paste code help wordcount'
                                  ],
                                  toolbar:
                                  'undo redo | formatselect | bold italic backcolor | \
                                  alignleft aligncenter alignright alignjustify | \
                                  bullist numlist outdent indent | removeformat | help'
                              }}
                      value={this.state.content}
                      onEditorChange ={this.handleEditorChange} />
                  </Form.Group>
                 </Col>
                 :
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Value*</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.value}
                        name="value"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.value && !errors.value}
                      />
                      <ErrorMessage name="value">
                        {msg => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                  }
                </Row> :
                     <Row>
                     {this.state.setting.content_type === 1 ?
                     <Col sm={12} md={6}>
                     <Form.Group>
                       <p>Logo*</p>
                       {/* <input
                         type="file"
                         name="value"
                         onChange={event => {
                           setFieldValue(
                             "value",
                             event.currentTarget.files[0]
                           );
                         }}
                       /> */}
                       <img src={values.value} width={"100px"} height={"auto"} />
                     </Form.Group>
                   </Col> : this.state.setting.content_type === 2 ?
                     <Col sm={12} md={6}>
                     <Form.Group>
                         <Form.Label>Content*</Form.Label>
                         <Editor 
                         disabled={true}
                         apiKey={window.tinyAPIKEY}
                         init={{ 
                                  width: 1100,
                                  plugins: [
                                  'advlist autolink lists link image charmap print preview anchor',
                                  'searchreplace visualblocks code fullscreen',
                                  'insertdatetime media table paste code help wordcount'
                                  ],
                                  toolbar:
                                  'undo redo | formatselect | bold italic backcolor | \
                                  alignleft aligncenter alignright alignjustify | \
                                  bullist numlist outdent indent | removeformat | help',
                                  readonly: true
                                 }}
                         value={this.state.content}
                         onEditorChange ={this.handleEditorChange} />
                     </Form.Group>
                    </Col>
                    :
                     <Col sm={12} md={6}>
                       <Form.Group>
                         <Form.Label>Value*</Form.Label>
                         <Form.Control
                           type="text"
                           value={values.value}
                           name="value"
                           disabled = {true}
                           onChange={handleChange}
                           onBlur={handleBlur}
                           isValid={touched.value && !errors.value}
                         />
                         <ErrorMessage name="value">
                           {msg => <div className="err_below">{msg}</div>}
                         </ErrorMessage>
                       </Form.Group>
                     </Col>
                     }
                   </Row>
                  }
                <Row>
                  <Col sm={12} md={4}></Col>
                  <Col sm={12} md={4}>
                    <button
                      className="btn btn-lg btn-primary btn-block setbgcolor"
                      type="submit"
                    >
                      Save
                    </button>
                  </Col>
                </Row>
              </Form>
              <ToastContainer />
            </Container>
          </div>
        )}
      />
    );
  }
}
