import React, { Component } from "react";
import { Col, Row, Table, Spinner } from "reactstrap";
import { Link } from "react-router-dom";
import CheckinoutService from "../../services/checkinoutService";
import SearchBar from "../../components/searchbar";
import Button from "react-bootstrap/Button";
import { Pageniation } from "../../components/Pageniation";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import moment from "moment";
import { CSVLink, CSVDownload } from "react-csv";

export default class CheckinoutList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      checkinoutList: [],
      csvResultList: [],
      totalCount: 0,
    };
    this.search = {
      start: 0,
      perPage: 10,
      searchTxt: "",
      searchField: "",
    };
    this.checkinoutServ = new CheckinoutService();
  }

  componentDidMount() {
    this.getCheckinoutList();
    this.getCsvData();
  }

  getCsvData() {
    let checkinout = { "": "" };

    this.checkinoutServ.getCsvData(checkinout, 0, 100000).then(
      (response) => {
        console.log(response);
        this.setState({ csvResultList: response.data });
      },
      (error) => {
        this.setState({ csvResultList: [] });
      }
    );
  }

  getCheckinoutList() {
    // let checkinout = { "": "" };
    let checkinout = {};
    switch (this.search.searchField) {
      case "User name":
        this.search.searchField = "user_id";
        break;
    }
    checkinout[this.search.searchField] = this.search.searchTxt;
    this.checkinoutServ.listService(checkinout, 0, 1000).then(
      (response) => {
        this.setState({
          checkinoutList: response.rows,
          totalCount: response.count,
        });
      },
      (error) => {
        this.setState({ checkinoutList: [], totalcount: 0 });
      }
    );
  }

  handlePaging(currentPage) {
    this.search.start = currentPage * this.search.perPage;
    this.getCheckinoutList();
  }
  render() {
    let active = Math.ceil(this.search.start / this.search.perPage);
    let data = [];
    for (let i = 0; i < this.state.checkinoutList.length; i++) {
      data.push(
        <tr key={this.state.checkinoutList[i].id}>
          <td>{i + 1}</td>
          <td>{this.state.checkinoutList[i]["user"]?.first_name}</td>
          <td>{this.state.checkinoutList[i]["user"]?.email}</td>
          <td>{this.state.checkinoutList[i]["location"]?.name}</td>
          <td>
            {moment(this.state.checkinoutList[i].checkin_time).format(
              "YYYY-MM-DD hh:mm A"
            )}
          </td>
          <td>
            {this.state.checkinoutList[i].checkout_time
              ? moment(this.state.checkinoutList[i].checkout_time).format(
                  "YYYY-MM-DD hh:mm A"
                )
              : ""}
          </td>
          <td>
            {this.state.checkinoutList[i].verification_time
              ? moment(this.state.checkinoutList[i].verification_time).format(
                  "YYYY-MM-DD hh:mm A"
                )
              : ""}
          </td>
        </tr>
      );
    }
    return (
      <div className="address addresslist">
        <Row>
          <Col sm={6}>
            <h3>Check-in-out Details</h3>
          </Col>
        </Row>
        <Row>
          <Col sm={9}>
            <Pageniation
              totalRecord={this.state.totalCount}
              currentPage={active}
              perPage={this.search.perPage}
              pageHandler={this.handlePaging.bind(this)}
            />
          </Col>
          <Col sm={3} className="text-right">
            <CSVLink
              className="btn btn-primary"
              fileName={"Checkinout List.csv"}
              data={this.state.csvResultList}
            >
              <i class="fa fa-file-excel-o"></i> &nbsp;Download Csv
            </CSVLink>
          </Col>
        </Row>
        {this.state.loading ? (
          <div style={{ textAlign: "center" }}>
            {" "}
            <Spinner style={{ width: "3rem", height: "3rem" }} />
          </div>
        ) : (
          <React.Fragment>
            <Row></Row>
            <Row>
              <Col sm={12}>
                <Table striped bordered hover size="sm">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>User Name</th>
                      <th>Email</th>
                      <th>Location</th>
                      <th>Checkin Time</th>
                      <th>Checkout Time</th>
                      <th>Verification Time</th>
                    </tr>
                  </thead>
                  {data.length !== 0 ? (
                    <tbody>{data}</tbody>
                  ) : (
                    <tbody style={{ textAlign: "center" }}>
                      <td colSpan="5">
                        <strong>No Record Found</strong>
                      </td>
                    </tbody>
                  )}
                </Table>
              </Col>
            </Row>
          </React.Fragment>
        )}
        <ToastContainer />
      </div>
    );
  }
}
