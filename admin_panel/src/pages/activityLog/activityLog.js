import React from "react";
import Table from "react-bootstrap/Table";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Pageniation } from "../../components/Pageniation";
import moment from "moment";
import activityLogService from "../../services/activityLogService";
import Button from "react-bootstrap/Button";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { CSVLink, CSVDownload } from "react-csv";

export default class ActivityLog extends React.Component {
  constructor(props) {
    super(props);
    this.state = { activityList: [], totalCount: 0, csvResultList: [] };
    this.search = { start: 0, perPage: 10, searchTxt: "", searchField: "" };
    this.activityLogServ = new activityLogService();
  }

  componentDidMount() {
    this.getActivityLogList();
    this.getCsvData();
  }

  searchActivityLogList(searchFilters) {
    this.search.searchTxt = searchFilters.term;
    this.search.searchField = searchFilters.fieldname;
    this.search.start = 0;
    this.getActivityLogList();
  }

  getActivityLogList() {
    let activity = {};
    switch (this.search.searchField) {
      case "name":
        this.search.searchField = "name";
        break;
      case "rating":
        this.search.searchField = "rating";
        break;
      case "description":
        this.search.searchField = "description";
        break;
    }

    activity[this.search.searchField] = this.search.searchTxt;
    this.activityLogServ
      .listService(activity, this.search.start, this.search.perPage)
      .then(
        (response) => {
          this.setState({
            activityList: response.rows,
            totalCount: response.count,
          });
        },
        (error) => {
          this.setState({ activityList: [], totalcount: 0 });
        }
      );
  }

 
  handlePaging(currentPage) {
    this.search.start = currentPage * this.search.perPage;
    this.getActivityLogList();
  }

  getCsvData() {
    let reviews = { "": "" };
    this.activityLogServ.getCsvData(reviews, 0, 100000).then(
      (response) => {
        console.log(response);
        this.setState({ csvResultList: response.data });
      },
      (error) => {
        this.setState({ csvResultList: [] });
      }
    );
  }

  render() {
    let active = Math.ceil(this.search.start / this.search.perPage);
    let data = [];
    for (let i = 0; i < this.state.activityList.length; i++) {
      data.push(
        <tr key={this.state.activityList[i].id}>
          <td>{this.search.start + i + 1}</td>
          <td>{this.state.activityList[i]["user"].first_name}</td>
          <td>
            {this.state.activityList[i].login_time
              ? moment(this.state.activityList[i].login_time).format(
                  "YYYY-MM-DD hh:mm A"
                )
              : ""}
          </td>
        </tr>
      );
    }
    return (
      <div className="address addresslist">
        <Row>
          <Col sm={6}>
            <h3>Activity Log List</h3>
          </Col>
        </Row>
        <Row>
          <Col sm={9}>
            <Pageniation
              totalRecord={this.state.totalCount}
              currentPage={active}
              perPage={this.search.perPage}
              pageHandler={this.handlePaging.bind(this)}
            />
          </Col>
          <Col sm={2} className="text-right">
            <CSVLink
              className="btn btn-primary"
              fileName={"Review List.csv"}
              data={this.state.csvResultList}
            >
              <i className="fa fa-file-excel-o"></i> &nbsp;Download CSV
            </CSVLink>
          </Col>
        </Row>
        <Row>
          <Col sm={12}>
            <Table striped bordered hover size="sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>User Name</th>
                  <th>Login Time</th>
                </tr>
              </thead>
              {data.length !== 0 ? (
                <tbody>{data}</tbody>
              ) : (
                <tbody style={{ textAlign: "center" }}>
                  <td colSpan="4">
                    <strong>No Record Found</strong>
                  </td>
                </tbody>
              )}
            </Table>
          </Col>
        </Row>
        <ToastContainer />
      </div>
    );
  }
}
