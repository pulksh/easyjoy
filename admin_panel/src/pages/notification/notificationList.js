import React, { Component } from "react";
import Table from "react-bootstrap/Table";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Pagination from "react-bootstrap/Pagination";
import { Link } from "react-router-dom";
import NotificationService from "../../services/notificationService";
import SearchBar from "../../components/searchbar";
import Button from "react-bootstrap/Button";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default class NotificationList extends Component {
  constructor(props) {
    super(props);

    this.state = { notificationList: [], totalCount: 0 };
    this.search = { start: 0, perPage: 25, searchTxt: "", searchField: "" };

    this.notificationServ = new NotificationService();
  }

  componentDidMount() {
    this.getNitificationList();
  }

  searchLocationList(searchFilters) {
    this.search.searchTxt = searchFilters.term;
    this.search.searchField = searchFilters.fieldname;
    this.search.start = 0;
    this.getNitificationList();
  }

  getNitificationList() {
    // setInterval(async () => {
      let notification = {};
      this.notificationServ
        .listNotification(notification, this.search.start, this.search.perPage)
        .then(
          (response) => {
            this.setState({
              notificationList: response.rows,
              totalCount: response.count,
            });
          },
          (error) => {
            this.setState({ locationList: [], totalcount: 0 });
          }
        );
    // }, 5000);
  }

  handlePaging(e) {
    if (e.target.text) {
      this.search.start =
        parseInt(e.target.text) * this.search.perPage - this.search.perPage;
      this.getNitificationList();
    }
  }

  handleDelete(id, e) {
    if (window.confirm("Are you sure you want to delete this record?")) {
      this.notificationServ.deleteNotification(id).then((response) => {
        if (response.error) {
          toast.error(response.error);
        } else {
          toast.success("Record deleted successfully");
          this.getNitificationList();
        }
      });
    }
  }

  render() {
    let active = Math.ceil((this.search.start + 1) / this.search.perPage);
    let pages = Math.ceil(this.state.totalCount / this.search.perPage);
    let items = [];
    for (let number = 1; number <= pages; number++) {
      items.push(
        <Pagination.Item
          key={number}
          onClick={this.handlePaging.bind(this)}
          active={number === active}
        >
          {number}
        </Pagination.Item>
      );
    }
    let data = [];
    for (let i = 0; i < this.state.notificationList.length; i++) {
      data.push(
        <tr key={this.state.notificationList[i].id}>
          <td>{this.search.start + i + 1}</td>
          <td>{this.state.notificationList[i]["user"]?.first_name}</td>
          <td>{this.state.notificationList[i]["location"]?.name}</td>
          <td>{this.state.notificationList[i].message}</td>
          {/* <td>{this.state.notificationList[i].status}</td> */}
          {/* <td>
            <Button
              size="sm"
              variant="danger"
              onClick={this.handleDelete.bind(
                this,
                this.state.notificationList[i].id
              )}
            >
              <i className="far fa-trash-alt"></i>
            </Button>
          </td> */}
        </tr>
      );
    }
    return (
      <div className="address addresslist">
        <Row>
          <Col sm={6}>
            <h3>Notification List</h3>
          </Col>
          {/* <Col sm={5}>
                <SearchBar
                  categories={["name", "rating","description","latitude","longitude"]}
                  parentFunction={this.searchLocationList.bind(this)}
                />
              </Col> */}
          <Col sm={1}>
            <div></div>
          </Col>
        </Row>
        <Row>
          <Col sm={9}>
            <Pagination size="md">{items}</Pagination>
          </Col>
        </Row>
        <Row>
          <Col sm={12}>
            <Table striped bordered hover size="sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>User Name</th>
                  <th>Location</th>
                  <th>Message</th>
                  {/* <th>Status</th> */}
                  {/* <th>Actions</th> */}
                </tr>
              </thead>
              {data.length !== 0 ? (
                <tbody>{data}</tbody>
              ) : (
                <tbody style={{ textAlign: "center" }}>
                  <td colSpan="4">
                    <strong>No Record Found</strong>
                  </td>
                </tbody>
              )}
            </Table>
          </Col>
        </Row>
        <ToastContainer />
      </div>
    );
  }
}
