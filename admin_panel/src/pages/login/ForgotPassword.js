import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Button,
  Card,
  CardBody,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from "reactstrap";
import userservice from "../../services/userService";
import { Redirect } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      loading: false,
      redirect: false,
    };
    this.userv = new userservice();
  }

  submitForgotPassword(e) {
    e.preventDefault();
    this.setState({ loading: true });
    let val = {
      email: this.state.email,
      flag: "admin_fogot_password",
    };
    this.userv
      .forget_password(val)
      .then((result) => {
        if(result.message){
          toast.success(result.message);
          setTimeout(() => {
            this.setState({ loading: false, redirect: true });
          }, 5000);
        } else {
          toast.error(result.error)
        }
      })
      .catch((e) => {
        toast.error(e);
        this.setState({ loading: false, redirect: false });
      });
  }

  handleUsernameChange(event) {
    this.setState({ email: event.target.value });
  }

  render() {
    if (this.state.redirect === true) {
      return <Redirect to="/login" />;
    } else {
      return (
        <div className="app flex-row align-items-center">
          <Container>
            <Row className="justify-content-center">
              <Col md="9" lg="7" xl="6">
                <Card className="mx-4">
                  <CardBody className="p-4">
                    <Form onSubmit={this.submitForgotPassword.bind(this)}>
                      <h1>Forgot Password</h1>
                      <p className="text-muted">
                        Enter your email address and check your email and click
                        on provided link to reset password
                      </p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>@</InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="text"
                          placeholder="Email"
                          autoComplete="email"
                          onChange={this.handleUsernameChange.bind(this)}
                        />
                      </InputGroup>
                      <Button color="primary" block>
                        Submit
                      </Button>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </Container>
          <ToastContainer />
        </div>
      );
    }
  }
}

export default ForgotPassword;
