import React, { Component } from "react";
import { Formik, ErrorMessage, useField } from "formik";
import * as Yup from "yup";
import {
  Button,
  Card,
  CardBody,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from "reactstrap";
import userservice from "../../services/userService";
import { Link, Redirect } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.token = props.match.params.token;
    this.state = {
      password: "",
      confirm_password: "",
      loading: false,
      redirect: false,
    };
    this.userv = new userservice();
    this.schema = Yup.object({
      password: Yup.string().required("Password is a required field"),
      confirm_password: Yup.string()
        .oneOf([Yup.ref("password"), null], "Password must match")
        .required("Confirm Password is a required field"),
    });
  }

  submitResetPassword(values, e) {
    this.setState({ loading: true });
    this.userv
      .resetPassword(values.password, values.confirm_password, this.token)
      .then((result) => {
        if(result.message){
          toast.success(result.message);
          setTimeout(()=>{
            this.setState({ loading: false, redirect: true });
          }, 4000)
        } else {
          toast.error(result.error)
        }
      })
      .catch((e) => {
        toast.error(e);
        this.setState({ loading: false, redirect: false });
      });
  }

  render() {
    if (this.state.redirect === true) {
      return <Redirect to="/login" />;
    } else {
      return (
        <Formik
          validationSchema={this.schema}
          initialValues={this.state}
          enableReinitialize={true}
          onSubmit={this.submitResetPassword.bind(this)}
        >
          {({
            values,
            errors,
            status,
            touched,
            handleBlur,
            handleChange,
            handleSubmit,
            isSubmitting,
          }) => (
            <div className="app flex-row align-items-center">
              <Container>
                <Row className="justify-content-center">
                  <Col md="9" lg="7" xl="6">
                    <Card className="mx-4">
                      <CardBody className="p-4">
                        <Form onSubmit={handleSubmit}>
                          <h1>Reset Password</h1>
                          <p className="text-muted">
                            Enter new password and confirm password
                          </p>
                          <div className="form-group">
                            <InputGroup>
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="icon-lock"></i>
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                type="password"
                                name="password"
                                placeholder="Password"
                                autoComplete="new-password"
                                onChange={handleChange}
                                onBlur={handleBlur}
                              />
                            </InputGroup>
                            <ErrorMessage name="password">
                              {(msg) => <div className="err_below">{msg}</div>}
                            </ErrorMessage>
                          </div>
                          <div className="form-group">
                            <InputGroup>
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="icon-lock"></i>
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                type="password"
                                name="confirm_password"
                                placeholder="Confirm password"
                                autoComplete="new-password"
                                onChange={handleChange}
                                onBlur={handleBlur}
                              />
                            </InputGroup>
                            <ErrorMessage name="confirm_password">
                              {(msg) => <div className="err_below">{msg}</div>}
                            </ErrorMessage>
                          </div>
                          <Button color="primary" block>
                            Submit
                          </Button>
                          <Link to="/login">Back to Login</Link>
                        </Form>
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </Container>
              <ToastContainer />
            </div>
          )}
        </Formik>
      );
    }
  }
}

export default ResetPassword;
