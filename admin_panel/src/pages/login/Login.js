import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Alert } from "reactstrap";
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row
} from "reactstrap";
import UserService from "../../services/userService";
import { Redirect } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mobile: "",
      otp: "",
      otpSend: false,
      loading: false,
      redirect: false
    };
    // this.date = new Date.year()
    this.userv = new UserService();
    this.props.handlerLoginParent(false);
  }

  generateOtp() {
    this.userv.otpGenerate(this.state.mobile).then(response => {
      console.log(response);
      if (response.send == true) {
        this.setState({ otpSend: true });
        alert(response.randomOtp);
      } else {
        // alert otp failed
      }
    });
  }

  submitLogin(e) {
    e.preventDefault();
    this.setState({ loading: true, redirect:false });
    this.userv
      .login(this.state.mobile, this.state.otp, 'backend')
      .then(result => {
        if(!result.error)
        {
          setTimeout(() => {
            this.setState({ loading: false, redirect: true });
          }, 2000);
          this.props.handlerLoginParent(true);
        }
        else{
          console.log(result.error,'result.error')
          toast.error(result.error)
          this.props.handlerLoginParent(false);
          this.setState({ loading: false, redirect: false });
        }
      })
      .catch(e => {
        toast.error(e.error);
        this.props.handlerLoginParent(false);
        this.setState({ loading: false, redirect: false });
      });
  }

  handleUsernameChange(event) {
    this.setState({ mobile: event.target.value });
  }

  handlePasswordChange(event) {
    this.setState({ otp: event.target.value });
  }
  render() {
    if (this.state.redirect === true || window.user) {
         return <Redirect to="/dashboard" />;
    } else {
      return (
        <div className="app flex-row align-items-center">
          <Container>
            <Row className="justify-content-center">
              <Col md="8">
                <CardGroup>
                  <Card className="p-4">
                    <CardBody>
                      <Form onSubmit={this.submitLogin.bind(this)}>
                        <h1>Login</h1>
                        <p className="text-muted">Sign In to your account</p>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-phone"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            placeholder="Email"
                            onChange={this.handleUsernameChange.bind(this)}
                          />
                        </InputGroup>
                        <InputGroup className="mb-4">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-lock"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="password"
                            placeholder="password"
                            onChange={this.handlePasswordChange.bind(this)}
                          />
                        </InputGroup>
                        <Row>
                          <Col xs="2"></Col>
                          <Col xs="4">
                            <Button color="primary" className="px-4">
                              Login
                            </Button>
                          </Col>
                          <Col xs="6" style = {{marginTop : "7px"}}>
                            <Link to="/forgotpassword" className="px-6">
                              Forgot password?
                            </Link>
                          </Col>
                        </Row>
                      </Form>
                    </CardBody>
                  </Card>
                  <Card
                    className="text-white bg-primary py-5 d-md-down-none"
                    style={{ width: "44%" }}
                  >
                    <CardBody className="text-center">
                      <div>
                        <h2>Please Login</h2>
                        <p>
                          You are allowed to login only if you are a registered
                          user as a Admin.
                        </p>
                      </div>
                    </CardBody>
                  </Card>
                </CardGroup>
              </Col>
            </Row>
          </Container>
          <ToastContainer />
        </div>
      );
    }
  }
}

export default Login;
