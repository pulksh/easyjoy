import React from "react";
import Table from "react-bootstrap/Table";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Pagination from "react-bootstrap/Pagination";
import { Link } from "react-router-dom";
import Review from "../../services/reviewService";
import SearchBar from "../../components/searchbar";
import Button from "react-bootstrap/Button";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { CSVLink, CSVDownload } from "react-csv";
export default class ReviewList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { reviewList: [], totalCount: 0, csvResultList: [] };
    this.search = { start: 0, perPage: 10, searchTxt: "", searchField: "" };
    this.reviewServ = new Review();
  }

  componentDidMount() {
    this.getReviewList();
    this.getCsvData();
  }

  getCsvData() {
    let reviews = { "": "" };
    this.reviewServ.getCsvData(reviews, 0, 100000).then(
      (response) => {
        console.log(response);
        this.setState({ csvResultList: response.data });
      },
      (error) => {
        this.setState({ csvResultList: [] });
      }
    );
  }

  searchReviewList(searchFilters) {
    this.search.searchTxt = searchFilters.term;
    this.search.searchField = searchFilters.fieldname;
    this.search.start = 0;
    this.getReviewList();
  }

  getReviewList() {
    let review = {};
    switch (this.search.searchField) {
      case "User Name":
        this.search.searchField = "user_id";
        break;
      case "location":
        this.search.searchField = "location_id";
        break;
      case "review":
        this.search.searchField = "review";
        break;
    }

    review[this.search.searchField] = this.search.searchTxt;
    this.reviewServ
      .listService(review, this.search.start, this.search.perPage)
      .then(
        (response) => {
          this.setState({
            reviewList: response.rows,
            totalCount: response.count,
          });
        },
        (error) => {
          this.setState({ reviewList: [], totalcount: 0 });
        }
      );
  }

  handlePaging(e) {
    if (e.target.text) {
      this.search.start =
        parseInt(e.target.text) * this.search.perPage - this.search.perPage;
      this.getReviewList();
    }
  }
  handleDelete(id, e) {
    if (window.confirm("Are you sure you want to delete this record?")) {
      this.reviewServ.deleteService(id).then((response) => {
        if (response.error) {
          toast.error(response.error);
        } else {
          toast.success("Record deleted successfully");
          this.getReviewList();
        }
      });
    }
  }

  render() {
    let active = Math.ceil((this.search.start + 1) / this.search.perPage);
    let pages = Math.ceil(this.state.totalCount / this.search.perPage);
    let items = [];
    for (let number = 1; number <= pages; number++) {
      items.push(
        <Pagination.Item
          key={number}
          onClick={this.handlePaging.bind(this)}
          active={number === active}
        >
          {number}
        </Pagination.Item>
      );
    }
    let data = [];
    for (let i = 0; i < this.state.reviewList.length; i++) {
      data.push(
        <tr key={this.state.reviewList[i].id}>
          <td>{this.search.start + i + 1}</td>
          <td>
            <Link
              to={{ pathname: "/review/edit/" + this.state.reviewList[i].id }}
            >
              {this.state.reviewList[i]["review"]}
            </Link>
          </td>
          <td>{this.state.reviewList[i]["user"]?.first_name}</td>
          <td>{this.state.reviewList[i]["location"]?.name}</td>

          <td>
            <Button
              size="sm"
              variant="danger"
              onClick={this.handleDelete.bind(
                this,
                this.state.reviewList[i].id
              )}
            >
              <i className="far fa-trash-alt"></i>
            </Button>
          </td>
        </tr>
      );
    }
    return (
      <div className="address addresslist">
        <Row>
          <Col sm={6}>
            <h3>Review List</h3>
          </Col>
          {/* <Col sm={5}>
            <SearchBar
              categories={["User Name", "location","review"]}
              parentFunction={this.searchReviewList.bind(this)}
            />
          </Col> */}
        </Row>
        <Row>
          <Col sm={8}>
            <Pagination size="md">{items}</Pagination>
          </Col>
          <Col sm={2} className="text-right">
            <CSVLink
              className="btn btn-primary"
              fileName={"Review List.csv"}
              data={this.state.csvResultList}
            >
              <i className="fa fa-file-excel-o"></i> &nbsp;Download CSV
            </CSVLink>
          </Col>
          <Col sm={2} className="text-right">
            <Link to={{ pathname: "/review/add" }} className="btn btn-primary">
              Add Review
            </Link>
          </Col>
        </Row>
        <Row>
          <Col sm={12}>
            <Table striped bordered hover size="sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Review</th>
                  <th>User Name</th>
                  <th>Location</th>
                </tr>
              </thead>
              {data.length !== 0 ? (
                <tbody>{data}</tbody>
              ) : (
                <tbody style={{ textAlign: "center" }}>
                  <td colSpan="4">
                    <strong>No Record Found</strong>
                  </td>
                </tbody>
              )}
            </Table>
          </Col>
        </Row>
        <ToastContainer />
      </div>
    );
  }
}
