import React from "react";
import Container from "react-bootstrap/Container";
import ReviewService from "../../services/reviewService";
import LocationService from "../../services/locationService";
import UserService from "../../services/userService";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Redirect } from "react-router-dom";
import Form from "react-bootstrap/Form";
import { Formik, ErrorMessage } from "formik";
import * as Yup from "yup";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default class Review extends React.Component {
  constructor(props) {
    super(props);
    this.review_id = props.match.params.id;
    this.state = {
      review: null,
      redirect: false,
      location: null,
      userList: null,
    };
    this.state.review = {
      user_id: "",
      location_id: "",
      review: "",
      rating: "",
      review_order_hygiene: "",
      review_clean_pot_seat: "",
      review_handwash: "",
      review_dryseat: "",
    };

    this.reviewServ = new ReviewService();
    this.locationServ = new LocationService();
    this.userServ = new UserService();

    if (this.review_id) {
      this.reviewServ.getService(this.review_id).then(
        (response) => {
          this.setState({ review: response });
        },
        (error) => {
          alert("Opps! Something went wrong not able to fetch Review details.");
        }
      );
    }

    this.schema = Yup.object({
      user_id: Yup.string().required("User required field"),
      location_id: Yup.string().required("Location required field"),
      review: Yup.string().required("Review a required field"),
      rating: Yup.string().required("Rating a required field"),
    });
  }

  componentDidMount() {
    this.locationList();
    this.userList();
  }

  userList() {
    let user = {};
    this.userServ.listUser(user, 0, 10000).then(
      (response) => {
        if (!response) {
          return (response = []);
        }
        this.setState({
          userList: response.rows,
          totalCount: response.count,
          loading: false,
        });
      },
      (error) => {
        this.setState({ userList: [], totalcount: 0 });
      }
    );
  }

  locationList() {
    let obj = {};
    this.locationServ.listService(obj, 0, 10000).then(
      (response) => {
        this.setState({ location: response.rows });
      },
      (error) => {
        alert("Opps! Something went wrong not able to fetch Locationdetails.");
      }
    );
  }

  submitReviewForm(values, actions) {
    actions.setSubmitting(false);
    let tx = values;
    this.setState({
      review: tx,
    });

    if (this.review_id) {
      this.reviewServ.editService(this.state.review).then(
        (response) => {
          setTimeout(() => {
            this.setState({ redirect: true });
          }, 2000);
          this.setState({ errorMsg: "" });
          toast.success(response);
        },
        (error) => {
          this.setState({ redirect: false });
        }
      );
    } else {
      this.reviewServ
        .addService(this.state.review)
        .then((response) => {
          this.setState({ redirect: true });
        })
        .catch((err) => {
          this.setState({ redirect: false });
        });
    }
  }

  render() {
    let userOption = [];
    for (let i = 0; i < this.state.userList?.length; i++) {
      if (this.state.userList[i].role == "2") {
        userOption.push(
          <option
            key={this.state.userList[i].id}
            value={this.state.userList[i].id}
          >
            {this.state.userList[i].first_name}
          </option>
        );
      }
    }

    let locationOption = [];
    for (let i = 0; i < this.state.location?.length; i++) {
      locationOption.push(
        <option
          key={this.state.location[i].id}
          value={this.state.location[i].id}
        >
          {this.state.location[i].name}
        </option>
      );
    }

    if (this.state.redirect === true) {
      return <Redirect to="/reviewlist" />;
    }
    return (
      <Formik
        validationSchema={this.schema}
        initialValues={this.state.review}
        enableReinitialize={true}
        onSubmit={this.submitReviewForm.bind(this)}
        render={({
          values,
          errors,
          status,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
          isSubmitting,
        }) => (
          <div className="address addresslist">
            <Container>
              <Form onSubmit={handleSubmit}>
                <Row>
                  <Col sm={12}>
                    <h3>Review Details</h3>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>User Name*</Form.Label>
                      <Form.Control
                        as="select"
                        name="user_id"
                        value={values.user_id}
                        onChange={(e) => {
                          handleChange(e);
                          this.setState({ user_id: e.target.value });
                        }}
                        isValid={touched.user_id && !errors.user_id}
                      >
                        <option></option>
                        {userOption}
                      </Form.Control>
                      <ErrorMessage name="user_id">
                        {(msg) => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Location Name*</Form.Label>
                      <Form.Control
                        as="select"
                        name="location_id"
                        value={values.location_id}
                        onChange={(e) => {
                          handleChange(e);
                          this.setState({ location_id: e.target.value });
                        }}
                        isValid={touched.location_id && !errors.location_id}
                      >
                        <option></option>
                        {locationOption}
                      </Form.Control>
                      <ErrorMessage name="location_id">
                        {(msg) => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Review*</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.review}
                        name="review"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.review && !errors.review}
                      />
                      <ErrorMessage name="review">
                        {(msg) => <div className="err_below">{msg}</div>}
                      </ErrorMessage>
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Rating</Form.Label>
                      <Form.Control
                        as="select"
                        name="rating"
                        value={values.rating}
                        onChange={handleChange}
                        isValid={touched.rating && !errors.rating}
                      >
                        <option></option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </Form.Control>
                      {/* <ErrorMessage name="rating">
                        {(msg) => <div className="err_below">{msg}</div>}
                      </ErrorMessage> */}
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Odour hygiene</Form.Label>
                      <Form.Control
                        as="select"
                        name="review_order_hygiene"
                        value={values.review_order_hygiene}
                        onChange={handleChange}
                        isValid={
                          touched.review_order_hygiene &&
                          !errors.review_order_hygiene
                        }
                      >
                        <option></option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </Form.Control>
                      {/* <ErrorMessage name="rating">
                        {(msg) => <div className="err_below">{msg}</div>}
                      </ErrorMessage> */}
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Clean Pot/ Seat</Form.Label>
                      <Form.Control
                        as="select"
                        name="review_clean_pot_seat"
                        value={values.review_clean_pot_seat}
                        onChange={handleChange}
                        isValid={
                          touched.review_clean_pot_seat &&
                          !errors.review_clean_pot_seat
                        }
                      >
                        <option></option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </Form.Control>
                      {/* <ErrorMessage name="rating">
                        {(msg) => <div className="err_below">{msg}</div>}
                      </ErrorMessage> */}
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Handwash & Water</Form.Label>
                      <Form.Control
                        as="select"
                        name="review_handwash"
                        value={values.review_handwash}
                        onChange={handleChange}
                        isValid={
                          touched.review_handwash && !errors.review_handwash
                        }
                      >
                        <option></option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </Form.Control>
                      {/* <ErrorMessage name="rating">
                        {(msg) => <div className="err_below">{msg}</div>}
                      </ErrorMessage> */}
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Dry Seat/ Floor</Form.Label>
                      <Form.Control
                        as="select"
                        name="review_dryseat"
                        value={values.review_dryseat}
                        onChange={handleChange}
                        isValid={
                          touched.review_dryseat && !errors.review_dryseat
                        }
                      >
                        <option></option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </Form.Control>
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4}></Col>
                  <Col sm={12} md={4}>
                    <button
                      className="btn btn-lg btn-primary btn-block setbgcolor"
                      type="submit"
                    >
                      Save
                    </button>
                  </Col>
                </Row>
              </Form>
            </Container>
            <ToastContainer />
          </div>
        )}
      />
    );
  }
}
