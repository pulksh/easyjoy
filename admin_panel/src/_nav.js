export default {
  items: [
    {
      name: "Dashboard",
      url: "/dashboard",
      icon: "icon-speedometer",
    },
    {
      title: true,
      name: "Navigation",
      wrapper: {
        // optional wrapper object
        element: "", // required valid HTML5 element tag
        attributes: {}, // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: "", // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: "Users",
      url: "/userlist",
      icon: "icon-star",
    },
    {
      name: "Location",
      url: "/locationlist",
      icon: "icon-star",
    },
    {
      name: "Review",
      url: "/reviewlist",
      icon: "icon-star",
    },
    // {
    //   name: "Check-in-out List",
    //   url: "/checkinoutlist",
    //   icon: "icon-star",
    // },
    {
      name: "Notifications",
      url: "/notification-list",
      icon: "icon-star",
    },
    {
      name: "Settings",
      url: "/settinglist",
      icon: "icon-star",
    },
    {
      name: "Static Pages",
      url: "/staticPagelist",
      icon: "icon-star"
    },
    {
      name: "Activity Log",
      url: "/activitylog",
      icon: "icon-star"
    },
    {
      name: "Logout",
      url: "/logout",
      icon: "icon-star",
    },
  ],
};
