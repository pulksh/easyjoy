import util from "../utils/util";

export default class UserService {
  login(email, password, type) {
    let body = { email, password, type };
    return util
      .sendApiRequest("/user/login", "POST", true, body)
      .then((response) => {
        console.log(response, "response");

        if (!response.error) {
          localStorage.setItem(
            "user",
            JSON.stringify({ token: response.token, ...response.result })
          );
          window.user = { token: response.token, ...response.result };
          return window.user;
        } else {
          return response;
        }
      })
      .catch((e) => {
        throw e;
      });
  }

  logout() {
    localStorage.removeItem("user");
    window.user = null;
  }

  getUser(id, withCourse) {
    return util
      .sendApiRequest("/user/" + id + (withCourse ? "/1" : ""), "GET", true)
      .then(
        (response) => {
          return response;
        },
        (error) => {
          throw new Error(error);
        }
      );
  }

  editService(service) {
    return util.sendApiRequest("/user", "PUT", true, service).then(
      (response) => {
        return response;
      },
      (error) => {
        throw new Error(error);
      }
    );
  }

  addService(service) {
    console.log(service, "service");
    return util.sendApiRequest("/user", "POST", true, service).then(
      (response) => {
        return response;
      },
      (error) => {
        throw new Error(error);
      }
    );
  }

  listUser(data, start, perpage) {
    const user = Object.keys(data).reduce((object, key) => {
      if (data[key] !== "") {
        object[key] = data[key];
      }
      return object;
    }, {});
    return util
      .sendApiRequest("/user/list/" + start + "/" + perpage, "POST", true, user)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        throw err;
      });
  }
  ownerlist(data, start, perpage) {
    const user = Object.keys(data).reduce((object, key) => {
      if (data[key] !== "") {
        object[key] = data[key];
      }
      return object;
    }, {});
    return util
      .sendApiRequest(
        "/user/ownerlist/" + start + "/" + perpage,
        "POST",
        true,
        user
      )
      .then((response) => {
        return response;
      })
      .catch((err) => {
        throw err;
      });
  }
  deleteUser(_id) {
    return util.sendApiRequest("/user/" + _id, "DELETE", true).then(
      (response) => {
        return response;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  forget_password(email) {
    return util
      .sendApiRequest("/user/forgotPassword", "POST", true, email)
      .then(
        (response) => {
          return response;
        },
        (error) => {
          return error;
        }
      );
  }
  resetPassword(password, confirmPassword, token) {
    let setPassword = {
      newPassword: password,
      verifyPassword: confirmPassword,
    };
    return util
      .sendApiRequest("/user/reset/" + token, "POST", true, setPassword, token)
      .then((response) => {
        return response;
      })
      .catch((e) => {
        throw e;
      });
  }
// Send mail to admin 
  sendMailToAdmin(ownerId) {
    return util
      .sendApiRequest("/user/sendmail/" + ownerId, "POST", true)
      .then((response) => {
        return response;
      })
      .catch((e) => {
        throw e;
      });
  }
}
