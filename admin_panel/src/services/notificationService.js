import util from "../utils/util";

export default class NotificationService {
  getNotification(id) {
    return util.sendApiRequest("/notification/" + id, "GET", true).then(
      response => {
        return response;
      },
      error => {
        throw new Error(error);
      }
    );
  }

  editNotification(service) {
    return util.sendApiRequest("/notification", "PUT", true, service).then(
      response => {
        return response;
      },
      error => {
        throw new Error(error);
      }
    );
  }

  listNotification(data, start, length) {
    const review = Object.keys(data).reduce((object, key) => {
      if (data[key] !== "") {
        object[key] = data[key];
      }
      return object;
    }, {});

    return util
      .sendApiRequest("/notification/list/" + start + "/" + length, "POST", true, review)
      .then(
        response => {
          return response;
        },
        error => {
          throw new Error(error);
        }
      );
  }


  deleteNotification(id) {
    return util.sendApiRequest("/notification/" + id, "DELETE", true).then(
      response => {
        return response;
      },
      error => {
        throw new Error(error);
      }
    );
  }
}
