import util from "../utils/util";

export default class activityLogList {
  listService(data, start, length) {
    const activity = Object.keys(data).reduce((object, key) => {
      if (data[key] !== "") {
        object[key] = data[key];
      }
      return object;
    }, {});

    return util
      .sendApiRequest(
        "/activitylog/list/" + start + "/" + length,
        "POST",
        true,
        activity
      )
      .then(
        (response) => {
          return response;
        },
        (error) => {
          throw new Error(error);
        }
      );
  }

   // Get csvdata
   getCsvData(searchObj) {
    const body = Object.keys(searchObj).reduce((object, key) => {
      if (searchObj[key] !== "") {
        object[key] = searchObj[key];
      }
      return object;
    }, {});
    return util.sendApiRequest("/activitylog/csvData/", "POST", true, body).then(
      (response) => {
        return response;
      },
      (error) => {
        throw new Error(error);
      }
    );
  }
}
