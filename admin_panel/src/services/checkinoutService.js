import util from "../utils/util";

export default class Checkinout {
  getService(id) {
    return util.sendApiRequest("/checkinout/" + id, "GET", true).then(
      (response) => {
        return response;
      },
      (error) => {
        throw new Error(error);
      }
    );
  }

  listService(data, start, length) {
    const checkinout = Object.keys(data).reduce((object, key) => {
      if (data[key] !== "") {
        object[key] = data[key];
      }
      return object;
    }, {});

    return util
      .sendApiRequest(
        "/checkinout/list/" + start + "/" + length,
        "POST",
        true,
        checkinout
      )
      .then(
        (response) => {
          return response;
        },
        (error) => {
          throw new Error(error);
        }
      );
  }

  // Get csvdata
  getCsvData(searchObj) {
    const body = Object.keys(searchObj).reduce((object, key) => {
      if (searchObj[key] !== "") {
        object[key] = searchObj[key];
      }
      return object;
    }, {});
    return util.sendApiRequest("/checkinout/csvData/", "POST", true, body).then(
      (response) => {
        return response;
      },
      (error) => {
        throw new Error(error);
      }
    );
  }

  async addCheckinout(data) {
    try {
      return await util.sendApiRequest("/checkinout/", "POST", true, data);
    } catch (err) {
      throw err;
    }
  }

  async editCheckinout(data) {
    try {
      return await util.sendApiRequest("/checkinout/", "PUT", true, data);
    } catch (err) {
      throw err;
    }
  }

  //Verify code
  async getVerifyCode(data) {
    try {
      return await util.sendApiRequest("/verifycode/", "POST", true, data);
    } catch (err) {
      throw err;
    }
  }
}
