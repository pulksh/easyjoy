import util from "../utils/util";

export default class ReviewService {
  getService(id) {
    return util.sendApiRequest("/review/" + id, "GET", true).then(
      response => {
        return response;
      },
      error => {
        throw new Error(error);
      }
    );
  }

  editService(service) {
    return util.sendApiRequest("/review", "PUT", true, service).then(
      response => {
        return response;
      },
      error => {
        throw new Error(error);
      }
    );
  }

  listService(data, start, length) {
    const review = Object.keys(data).reduce((object, key) => {
      if (data[key] !== "") {
        object[key] = data[key];
      }
      return object;
    }, {});

    return util
      .sendApiRequest("/review/list/" + start + "/" + length, "POST", true, review)
      .then(
        response => {
          return response;
        },
        error => {
          throw new Error(error);
        }
      );
  }

  addService(service) {
    return util.sendApiRequest("/review", "POST", true, service).then(
      response => {
        return response;
      },
      error => {
        throw new Error(error);
      }
    );
  }
  deleteService(id) {
    return util.sendApiRequest("/review/" + id, "DELETE", true).then(
      response => {
        return response;
      },
      error => {
        throw new Error(error);
      }
    );
  }
  // Get csvdata
  getCsvData(searchObj) {
    const body = Object.keys(searchObj).reduce((object, key) => {
      if (searchObj[key] !== "") {
        object[key] = searchObj[key];
      }
      return object;
    }, {});
    return util.sendApiRequest("/review/csvData/", "POST", true, body).then(
      (response) => {
        return response;
      },
      (error) => {
        throw new Error(error);
      }
    );
  }
}
