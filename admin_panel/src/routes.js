import React from 'react';
const Logout = React.lazy(() => import('./components/Logout'));
const UserList = React.lazy(() => import('./pages/user/userList'));
const User = React.lazy(() => import('./pages/user/user'));
const Location = React.lazy(() => import('./pages/location/location'));
const LocationList = React.lazy(() => import('./pages/location/locationList'));
const Review = React.lazy(() => import('./pages/review/review'));
const ReviewList = React.lazy(() => import('./pages/review/reviewList'));
const SettingList = React.lazy(() => import('./pages/setting/SettingList'));
const Setting = React.lazy(() => import('./pages/setting/Setting'));
const CheckinoutList = React.lazy(() => import('./pages/checkinout/checkinoutList'));
const OwnerList = React.lazy(() => import('./pages/Owners/List'));
const CheckinoutListOwner = React.lazy(() => import('./pages/Owners/checkinoutListOwner'))
const StaticPage  = React.lazy(() => import('./pages/staticPage/staticPage'));
const StaticPagelist = React.lazy(() => import('./pages/staticPage/staticPageList') )
const NotificationList = React.lazy(() => import('./pages/notification/notificationList') )
const AgreementList = React.lazy(() => import('./pages/agreement/agreement') )
const ActivityLogList = React.lazy(() => import('./pages/activityLog/activityLog') )

const routes = [
 // { path: '/', exact: true, name: 'Home', component: Home },
  { path: "/", exact: true, name: "Home" },
  { path: '/logout', name:'Logout', component: Logout },

  { path: '/user/add', name:'Add User', component: User },
  { path: '/user/edit/:id', name:'Edit User', component: User },
  { path: '/userlist', name:'User List', component: UserList },
  { path: '/location/add', name:'Add Location', component: Location },
  { path: '/location/edit/:id', name:'Edit Location', component: Location },
  { path: '/locationlist', name:'Location List', component:  LocationList },
  { path: '/review/add', name:'Add Review', component: Review },
  { path: '/review/edit/:id', name:'Edit Review', component: Review },
  { path: '/reviewlist', name:'Review List', component:  ReviewList },
  { path: '/settinglist', name:'Setting List', component: SettingList },
  { path: '/setting/add', name:'Add Setting', component: Setting },
  { path: '/setting/edit/:id', name:'Edit Setting', component: Setting },
  { path: '/checkinoutlist', name:'Checkinout List', component: CheckinoutList },

  { path: '/ownerlist', name:'Owner List', component: OwnerList },
  { path: '/checkinoutowner', name:'Checkinout List', component: CheckinoutListOwner },
  { path: '/staticpage/add', name:'Add Static Page', component: StaticPage },
  { path: '/staticpage/edit/:id', name:'Edit Static Page', component: StaticPage },
  { path: '/staticpagelist', name:'Static Page List', component: StaticPagelist },
  { path: '/notification-list', name: 'Notification List', component: NotificationList },
  { path: '/agreement-list', name: 'Agreement List', component: AgreementList },
  { path: '/Activitylog', name: 'ActivityLog List', component: ActivityLogList}
  
];

export default routes;
